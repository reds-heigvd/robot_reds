# Notes réunion AROBSA - Agribot 

### 24 Aout 2021

#### <u>Participants : EMI/ SMS/ ACT</u> 

- Opto coupleurs impossibles à utiliser -> ferrites SMD à la place. Ne sont placées que sur la carte, peut-être les placer aussi à l'entrée de l'alimentation de la batterie. Mettre une ferrite sur + et - ? 
  Les ferrites juste autour de SCL et SDA n'étaient pas le gros des soucis, mettre la ferrites sur le GND est le plus important. 
- Vérifier pour l'alim le filtrage + condensateur (alim 5V de la carte).
- Annoncer la démo à Mathilde (pour la mettre dans la liste des démos)
- Présentation du robot sur 2 pages A4 -> PDF.
- Peut-être faire un poster (imprimé en grand) ? à réfléchir
- Prévoir de classer le robot en A07. Demander à Steve une caisse de rangement.



```
./autogen.sh && ./configure CFLAGS='-g -O2' --sysconfdir=/etc --localstatedir=/var --libdir=/usr/lib64 --prefix=/usr
```
