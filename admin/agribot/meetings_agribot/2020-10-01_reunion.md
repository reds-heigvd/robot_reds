## Travail réalisé : 

- Structure mise en place
- Robot envoyé côté mécanique 



## Travail à faire : 

Suivre le montage du robot.

**<u>Modifier la structure du projet :</u>**

<u>RACINE</u>

- ​	robot_reds
  - dev
    - src
    - robot.launch
    - build
    - devel
  - admin
  - doc
  - publi



<u>DANS SOURCE :</u> 

- global_launcher

- robot

  - agribot
    - description
    - source

- ros_pkg

  - navigation

  - sensors

  - hardware

  - misc

    

- reds_pkg

  - navigation
  - sensors
  - hardware
  - misc
