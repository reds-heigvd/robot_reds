# Notes réunion AROBSA - Agribot 

### 20 Avril 2021

#### <u>Participants : EMI/ SMS/ ACT</u> 

- Définir et documenter la résolution (améliorée par le réducteur)

- Faire des tests pour voir si perte d'info ou pas

- Mesure des signaux du codeur.

- Tester à fond la communication avec programme/ligne de commande.

  

- Les dessins mécaniques sont fait. Il faut découper les plaques. En mécanique, peut-être une matière noire.

- Matière pomme plus flexible.

- Voir avec l'atelier mécanique pour récupérer les bons matériaux.
