----------------------------------------------------------------------------------------

HEIG-VD, Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
REDS Institute, Reconfigurable Embedded Digital Systems

----------------------------------------------------------------------------------------------------------

------------

file : README.md
Author : A.Gabriel Catel Torres
Date : 25.04.2021

Context : plateforme multi-robot du REDS

--------------------

Description : structure du dépôt du projet générique pour les robots développés au sein du REDS.

_____________



### Informations importantes : 

Plus de précisions concernant les différentes parties sont présentes dans les dossier "software" et "hardware".



## Structure du dépôt



```
​```
robot_reds
│   README.md
│
└───admin                  // Informations administratives
|   |
│   └───agribot
|   |    |   
|   |    └───meetings      // PV des réunions pour Agribot
|   | 
│   └───helpie
│   
└───dev
|   |
│   └───hardware            // Dev hardware pour les différents robots
|   |    |   
|   |    └───agribot        // Contient les informations de la CPLD d'agribot
|   |
│   └───software            // Regroupe le code pour les différents robots
|   |    |   
|   |    └───external_ws    // Worskpace catkin pour le lancement des démos (machine host)
|   |    |   
|   |    └───gui            // GUI pour le touchscreen
|   |    |   
|   |    └───robot_ws       // Workspace catkin contenant le code pour les robots
|   |    |   
|   |    | README.md
|   |    | launch_demo.md

│   
└───doc                     // Documentation de tiers
|   |
│   └───agribot             // Doc des différents composants HW d'agribot
|   |
│   └───helpie              // Doc des différents composants HW d'agribot
│   
└───publi                   // Documentation rédigée 
|   |
│   └───agribot
|   |
│   └───helpie
|   |
│   └───tutoriels           // Tutoriels permettant d'apréhender l'environnemente et ROS

​```
```