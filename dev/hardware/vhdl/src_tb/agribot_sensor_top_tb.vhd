-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : agribot_sensor_top_tb.vhd
-- Description  : quick testbench to test agribot sensor design
--
-- Author       : Sebastien Masle
-- Date         : 26.03.2021
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       SMS    26.03.2021         Creation
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library std;
    use std.textio.all;

library work;
    use work.function_pkg.all;
    use work.i2c_slave_pkg.all;

entity agribot_sensor_top_tb is
end agribot_sensor_top_tb ;

architecture testbench of agribot_sensor_top_tb is
  
    component agribot_sensor_top
        port(
            clock_25MHz_i  : in  std_logic;
            nReset_i       : in  std_logic;
            --i2c signals
            sda_io         : inout std_logic; 
            scl_i          : in    std_logic;
            -- incremental encoders signals (F stands for Front - B stands for Back
            --                               R stands for Right - L stands for Left)
            encoder_FR_a_i : in  std_logic;  -- FR encoder - A signal
            encoder_FR_b_i : in  std_logic;  -- FR encoder - B signal
            encoder_FL_a_i : in  std_logic;  -- FL encoder - A signal
            encoder_FL_b_i : in  std_logic;  -- FL encoder - B signal
            encoder_BR_a_i : in  std_logic;  -- BR encoder - A signal
            encoder_BR_b_i : in  std_logic;  -- BR encoder - B signal
            encoder_BL_a_i : in  std_logic;  -- BL encoder - A signal
            encoder_BL_b_i : in  std_logic); -- BL encoder - B signal
    end component;

    constant PERIODE_CLK_SYS  : time := 100 ns;
    constant NB_POS_PER_REVOL : natural := 96;

    signal sim_end_s  : boolean := false;
    signal clk_sti    : std_logic;
    signal reset_sti  : std_logic;

    signal data_read_s : std_logic_vector(31 downto 0) := (others => '0');

    -- Open Collector
    signal en_OC_s    : std_Logic;
    signal sda_in_s   : std_Logic;
    signal sda_out_s  : std_Logic;

    signal i2c_sda_s  : std_logic;
    signal i2c_scl_s  : std_logic;

    -- encoder signals
    signal encoder_FR_a_sti : std_logic;
    signal encoder_FR_b_sti : std_logic;
    signal encoder_FL_a_sti : std_logic;
    signal encoder_FL_b_sti : std_logic;
    signal encoder_BR_a_sti : std_logic;
    signal encoder_BR_b_sti : std_logic;
    signal encoder_BL_a_sti : std_logic;
    signal encoder_BL_b_sti : std_logic;

begin

    uut: agribot_sensor_top
    port map(clock_25MHz_i  => clk_sti,
             nReset_i       => not reset_sti,
             --i2c signals
             sda_io         => i2c_sda_s,
             scl_i          => i2c_scl_s,
             -- incremental encoders signals (F stands for Front - B stands for Back
             --                               R stands for Right - L stands for Left)
             encoder_FR_a_i => encoder_FR_a_sti,
             encoder_FR_b_i => encoder_FR_b_sti,
             encoder_FL_a_i => encoder_FL_a_sti,
             encoder_FL_b_i => encoder_FL_b_sti,
             encoder_BR_a_i => encoder_BR_a_sti,
             encoder_BR_b_i => encoder_BR_b_sti,
             encoder_BL_a_i => encoder_BL_a_sti,
             encoder_BL_b_i => encoder_BL_b_sti
    );

    -- Open Collector
    sda_in_s  <= to_X01(i2c_sda_s);  -- transformer état 'H' en '1' pour la simulation
    i2c_sda_s <= '0' when (en_OC_s = '1' and sda_out_s = '0') else
                 'Z';
    
    --Model of pull-up
    i2c_sda_s  <= 'H';
    
    ------------------------------------------------------------------
    --clock generation
    ------------------------------------------------------------------
    process
    begin
        while not sim_end_s loop
            clk_sti <= '1', '0' after PERIODE_CLK_SYS/2;
            wait for PERIODE_CLK_SYS;
        end loop;
        wait;
    end process;

    -- main process
    process
        variable l_v        :line;
        variable i2c_data_v : std_logic_vector(31 downto 0);

        procedure cycle(nb_cycle : integer := 1) is
        begin
            for i in 1 to nb_cycle loop
                wait until falling_edge(clk_sti);
                wait for 2 ns;
            end loop;
        end cycle;
        
        procedure reset_seq is
        begin
            reset_sti <= '1';
            i2c_scl_s <= 'H';
            en_OC_s   <= '0';
            sda_out_s <= '1';
            cycle(2);
            reset_sti <= '0';
            -- wait for the PLL to be locked
            cycle(10);
        end reset_seq;
    begin

        -- Reset system at the beginning
        reset_seq; -- reset procedure

        ----------------------------------------
        -- configure time for speed calculation
        ----------------------------------------
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        write(l_v,now);write(l_v,string'(": [I2C] configure interval for speed calculation"));writeline(output,l_v);
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        -- start condition
        en_OC_s   <= '1';
        sda_out_s <= '0';

        cycle(5);

        i2c_scl_s <= '0';

        -- send slave address
        for I in I2C_SLAVE_ADDRESS'range loop
            cycle(2);
            sda_out_s <= I2C_SLAVE_ADDRESS(I);
            cycle(3);
            i2c_scl_s <= '1';
            cycle(5);
            i2c_scl_s <= '0';
        end loop;

        -- set access direction (write)
        cycle(2);
        sda_out_s <= '0';
        cycle(3);
        i2c_scl_s <= '1';
        cycle(5);
        i2c_scl_s <= '0';
        -- free sda line for slave ack
        en_OC_s <= '0';
        cycle(5);
        i2c_scl_s <= '1';
        if sda_in_s = '1' then -- NACK slave address
            report ">> I2C Master: no slave at address " & vector_to_string(I2C_SLAVE_ADDRESS) & " on bus, error" severity FAILURE;
        else -- ACK slave address
            report ">> I2C Master: slave responded at address " & vector_to_string(I2C_SLAVE_ADDRESS) & " on bus, this is a good start" severity NOTE;
            cycle(5);
            i2c_scl_s <= '0';
            en_OC_s   <= '1';
            sda_out_s <= '1'; -- do not drive SDA
            -- send register address
            for I in REGISTER_BANK_ADDR(12).addr'range loop
                cycle(2);
                sda_out_s <= REGISTER_BANK_ADDR(12).addr(I);
                cycle(3);
                i2c_scl_s <= '1';
                cycle(5);
                i2c_scl_s <= '0';
            end loop;
            -- free sda line for slave ack
            en_OC_s <= '0';
            cycle(5);
            i2c_scl_s <= '1';
             -- check and and verify that it corresponds with the test case
            if sda_in_s = '1' then -- NACK register address
                report ">> I2C Master: no register at address " & vector_to_string(REGISTER_BANK_ADDR(12).addr) & " on bus, error" severity FAILURE;
            else  -- ACK register address
                report ">> I2C Master: register present at address " & vector_to_string(REGISTER_BANK_ADDR(12).addr) & " on bus, this is fairly good" severity NOTE;
                cycle(5);
                i2c_scl_s <= '0';
                -- write interval value
                en_OC_s   <= '1';
                sda_out_s <= '1'; -- do not drive SDA
                i2c_data_v := x"00000002";
                for I in REGISTER_SIZE - 1 downto 0 loop
                    for J in 7 downto 0 loop -- can only send one byte at a time
                        cycle(2);
                        sda_out_s <= i2c_data_v(I*8 + J);
                        cycle(3);
                        i2c_scl_s <= '1';
                        cycle(5);
                        i2c_scl_s <= '0';
                    end loop;-- free sda line for slave ack
                    en_OC_s <= '0';
                    cycle(5);
                    i2c_scl_s <= '1';
                    if sda_in_s = '1' then -- NACK 
                        report ">> I2C Master: received a NACK while writing, this is not correct" severity FAILURE;
                        exit;
                    else
                        report ">> I2C Master: received a ACK while writing, this is expected behavior" severity NOTE;
                    end if;
                    cycle(5);
                    i2c_scl_s <= '0';
                    cycle(2);
                    en_OC_s   <= '1';
                end loop;
            end if;
        end if;
        cycle(2);
        en_OC_s   <= '1';
        sda_out_s <= '0';
        cycle(3);
        i2c_scl_s <= '1';

        -- stop condition
        cycle(2);
        sda_out_s <= '1';

        cycle(20);

        ----------------------------------------
        -- memorize encoders positions
        ----------------------------------------
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        write(l_v,now);write(l_v,string'(": [I2C] memorize encoders positions"));writeline(output,l_v);
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        -- start condition
        en_OC_s   <= '1';
        sda_out_s <= '0';

        cycle(5);

        i2c_scl_s <= '0';

        -- send slave address
        for I in I2C_SLAVE_ADDRESS'range loop
            cycle(2);
            sda_out_s <= I2C_SLAVE_ADDRESS(I);
            cycle(3);
            i2c_scl_s <= '1';
            cycle(5);
            i2c_scl_s <= '0';
        end loop;

        -- set access direction (write)
        cycle(2);
        sda_out_s <= '0';
        cycle(3);
        i2c_scl_s <= '1';
        cycle(5);
        i2c_scl_s <= '0';
        -- free sda line for slave ack
        en_OC_s <= '0';
        cycle(5);
        i2c_scl_s <= '1';
        if sda_in_s = '1' then -- NACK slave address
            report ">> I2C Master: no slave at address " & vector_to_string(I2C_SLAVE_ADDRESS) & " on bus, error" severity FAILURE;
        else -- ACK slave address
            report ">> I2C Master: slave responded at address " & vector_to_string(I2C_SLAVE_ADDRESS) & " on bus, this is a good start" severity NOTE;
            cycle(5);
            i2c_scl_s <= '0';
            en_OC_s   <= '1';
            sda_out_s <= '1'; -- do not drive SDA
            -- send register address
            for I in REGISTER_BANK_ADDR(2).addr'range loop
                cycle(2);
                sda_out_s <= REGISTER_BANK_ADDR(2).addr(I);
                cycle(3);
                i2c_scl_s <= '1';
                cycle(5);
                i2c_scl_s <= '0';
            end loop;
            -- free sda line for slave ack
            en_OC_s <= '0';
            cycle(5);
            i2c_scl_s <= '1';
             -- check and and verify that it corresponds with the test case
            if sda_in_s = '1' then -- NACK register address
                report ">> I2C Master: no register at address " & vector_to_string(REGISTER_BANK_ADDR(2).addr) & " on bus, error" severity FAILURE;
            else  -- ACK register address
                report ">> I2C Master: register present at address " & vector_to_string(REGISTER_BANK_ADDR(2).addr) & " on bus, this is fairly good" severity NOTE;
                cycle(5);
                i2c_scl_s <= '0';
                -- write interval value
                en_OC_s   <= '1';
                sda_out_s <= '1'; -- do not drive SDA
                i2c_data_v := x"00000F00";
                for I in REGISTER_SIZE - 1 downto 0 loop
                    for J in 7 downto 0 loop -- can only send one byte at a time
                        cycle(2);
                        sda_out_s <= i2c_data_v(I*8 + J);
                        cycle(3);
                        i2c_scl_s <= '1';
                        cycle(5);
                        i2c_scl_s <= '0';
                    end loop;-- free sda line for slave ack
                    en_OC_s <= '0';
                    cycle(5);
                    i2c_scl_s <= '1';
                    if sda_in_s = '1' then -- NACK 
                        report ">> I2C Master: received a NACK while writing, this is not correct" severity FAILURE;
                        exit;
                    else
                        report ">> I2C Master: received a ACK while writing, this is expected behavior" severity NOTE;
                    end if;
                    cycle(5);
                    i2c_scl_s <= '0';
                    cycle(2);
                    en_OC_s   <= '1';
                end loop;
            end if;
        end if;
        cycle(2);
        en_OC_s   <= '1';
        sda_out_s <= '0';
        cycle(3);
        i2c_scl_s <= '1';

        -- stop condition
        cycle(2);
        sda_out_s <= '1';

        cycle(20);

        ----------------------------------------
        -- memorize encoders speeds
        ----------------------------------------
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        write(l_v,now);write(l_v,string'(": [I2C] memorize encoders speeds"));writeline(output,l_v);
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        -- start condition
        en_OC_s   <= '1';
        sda_out_s <= '0';

        cycle(5);

        i2c_scl_s <= '0';

        -- send slave address
        for I in I2C_SLAVE_ADDRESS'range loop
            cycle(2);
            sda_out_s <= I2C_SLAVE_ADDRESS(I);
            cycle(3);
            i2c_scl_s <= '1';
            cycle(5);
            i2c_scl_s <= '0';
        end loop;

        -- set access direction (write)
        cycle(2);
        sda_out_s <= '0';
        cycle(3);
        i2c_scl_s <= '1';
        cycle(5);
        i2c_scl_s <= '0';
        -- free sda line for slave ack
        en_OC_s <= '0';
        cycle(5);
        i2c_scl_s <= '1';
        if sda_in_s = '1' then -- NACK slave address
            report ">> I2C Master: no slave at address " & vector_to_string(I2C_SLAVE_ADDRESS) & " on bus, error" severity FAILURE;
        else -- ACK slave address
            report ">> I2C Master: slave responded at address " & vector_to_string(I2C_SLAVE_ADDRESS) & " on bus, this is a good start" severity NOTE;
            cycle(5);
            i2c_scl_s <= '0';
            en_OC_s   <= '1';
            sda_out_s <= '1'; -- do not drive SDA
            -- send register address
            for J in REGISTER_BANK_ADDR(2).addr'range loop
                cycle(2);
                sda_out_s <= REGISTER_BANK_ADDR(2).addr(J);
                cycle(3);
                i2c_scl_s <= '1';
                cycle(5);
                i2c_scl_s <= '0';
            end loop;
            -- free sda line for slave ack
            en_OC_s <= '0';
            cycle(5);
            i2c_scl_s <= '1';
             -- check and and verify that it corresponds with the test case
            if sda_in_s = '1' then -- NACK register address
                report ">> I2C Master: no register at address " & vector_to_string(REGISTER_BANK_ADDR(2).addr) & " on bus, error" severity FAILURE;
            else  -- ACK register address
                report ">> I2C Master: register present at address " & vector_to_string(REGISTER_BANK_ADDR(2).addr) & " on bus, this is fairly good" severity NOTE;
                cycle(5);
                i2c_scl_s <= '0';
                -- write interval value
                en_OC_s   <= '1';
                sda_out_s <= '1'; -- do not drive SDA
                i2c_data_v := x"0000F000";
                for J in REGISTER_SIZE - 1 downto 0 loop
                    for K in 7 downto 0 loop -- can only send one byte at a time
                        cycle(2);
                        sda_out_s <= i2c_data_v(J*8 + K);
                        cycle(3);
                        i2c_scl_s <= '1';
                        cycle(5);
                        i2c_scl_s <= '0';
                    end loop;-- free sda line for slave ack
                    en_OC_s <= '0';
                    cycle(5);
                    i2c_scl_s <= '1';
                    if sda_in_s = '1' then -- NACK 
                        report ">> I2C Master: received a NACK while writing, this is not correct" severity FAILURE;
                        exit;
                    else
                        report ">> I2C Master: received a ACK while writing, this is expected behavior" severity NOTE;
                    end if;
                    cycle(5);
                    i2c_scl_s <= '0';
                    cycle(2);
                    en_OC_s   <= '1';
                end loop;
            end if;
        end if;
        cycle(2);
        en_OC_s   <= '1';
        sda_out_s <= '0';
        cycle(3);
        i2c_scl_s <= '1';

        -- stop condition
        cycle(2);
        sda_out_s <= '1';

        cycle(20);

        ----------------------------------------
        -- read front right encoder position
        ----------------------------------------
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        write(l_v,now);write(l_v,string'(": [I2C] read front right encoder position"));writeline(output,l_v);
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        -- start condition
        en_OC_s   <= '1';
        sda_out_s <= '0';

        cycle(5);

        i2c_scl_s <= '0';

        -- send slave address
        for I in I2C_SLAVE_ADDRESS'range loop
            cycle(2);
            sda_out_s <= I2C_SLAVE_ADDRESS(I);
            cycle(3);
            i2c_scl_s <= '1';
            cycle(5);
            i2c_scl_s <= '0';
        end loop;

        -- set access direction (write)
        cycle(2);
        sda_out_s <= '0';
        cycle(3);
        i2c_scl_s <= '1';
        cycle(5);
        i2c_scl_s <= '0';
        -- free sda line for slave ack
        en_OC_s <= '0';
        cycle(5);
        i2c_scl_s <= '1';
        if sda_in_s = '1' then -- NACK slave address
            report ">> I2C Master: no slave at address " & vector_to_string(I2C_SLAVE_ADDRESS) & " on bus, error" severity FAILURE;
        else -- ACK slave address
            report ">> I2C Master: slave responded at address " & vector_to_string(I2C_SLAVE_ADDRESS) & " on bus, this is a good start" severity NOTE;
            cycle(5);
            i2c_scl_s <= '0';
            en_OC_s   <= '1';
            sda_out_s <= '1'; -- do not drive SDA
            -- send register address
            for J in REGISTER_BANK_ADDR(4).addr'range loop
                cycle(2);
                sda_out_s <= REGISTER_BANK_ADDR(4).addr(J);
                cycle(3);
                i2c_scl_s <= '1';
                cycle(5);
                i2c_scl_s <= '0';
            end loop;
            -- free sda line for slave ack
            en_OC_s <= '0';
            cycle(5);
            i2c_scl_s <= '1';
             -- check and and verify that it corresponds with the test case
            if sda_in_s = '1' then -- NACK register address
                report ">> I2C Master: no register at address " & vector_to_string(REGISTER_BANK_ADDR(4).addr) & " on bus, error" severity FAILURE;
            else  -- ACK register address
                report ">> I2C Master: register present at address " & vector_to_string(REGISTER_BANK_ADDR(4).addr) & " on bus, this is fairly good" severity NOTE;
                cycle(5);
                i2c_scl_s <= '0';
                
                -- read access
                -- send repeted start
                cycle(5);
                i2c_scl_s <= '1';
                cycle(2);
                en_OC_s   <= '1';
                sda_out_s <= '0';
                cycle(3);
                i2c_scl_s <= '0';

                -- send slave address
                for I in I2C_SLAVE_ADDRESS'range loop
                    cycle(2);
                    sda_out_s <= I2C_SLAVE_ADDRESS(I);
                    cycle(3);
                    i2c_scl_s <= '1';
                    cycle(5);
                    i2c_scl_s <= '0';
                end loop;
    
                -- set access direction
                cycle(2);
                sda_out_s <= '1';
                cycle(3);
                i2c_scl_s <= '1';
                cycle(5);
                i2c_scl_s <= '0';
                -- free sda line for slave ack
                en_OC_s <= '0';
                cycle(5);
                i2c_scl_s <= '1';
                if sda_in_s = '1' then -- NACK slave address
                    report ">> I2C Master: no slave at address " & vector_to_string(I2C_SLAVE_ADDRESS) & " on bus, error" severity FAILURE;
                else -- ACK slave address
                    report ">> I2C Master: slave responded at address " & vector_to_string(I2C_SLAVE_ADDRESS) & " on bus, this is a good start" severity NOTE;
                    cycle(5);
                    i2c_scl_s <= '0';
                    -- read data from slave
                    for I in REGISTER_SIZE - 1 downto 0 loop
                        en_OC_s <= '0';
                        for J in 7 downto 0 loop -- can only receive one byte at a time
                            cycle(5);
                            i2c_scl_s <= '1';
                            data_read_s(I * 8 + J) <= sda_in_s;
                            cycle(5);
                            i2c_scl_s <= '0';
                        end loop;
                        if I = 0 then -- send NACK - cannot receive any more bytes
                            cycle(2);
                            en_OC_s   <= '1';
                            sda_out_s <= '1';
                            cycle(3);
                            i2c_scl_s <= '1';
                            cycle(5);
                            i2c_scl_s <= '0';
                        else -- send ACK - can receive more bytes
                            cycle(2);
                            en_OC_s   <= '1';
                            sda_out_s <= '0';
                            cycle(3);
                            i2c_scl_s <= '1';
                            cycle(5);
                            i2c_scl_s <= '0';
                        end if;
                    end loop;
                end if;
            end if;
        end if;
        cycle(2);
        en_OC_s   <= '1';
        sda_out_s <= '0';
        cycle(3);
        i2c_scl_s <= '1';

        -- stop condition
        cycle(2);
        sda_out_s <= '1';

        cycle(100000);

        sim_end_s <= true;
        wait;

    end process;
  
    -- front right sensor signal generation
    FR_sensor : process
        variable l_v    :line;
        variable NumPos : integer:=0;
        variable DureeParPosition_v : time;

        procedure cycle(nb_cycle : integer := 1) is
        begin
            for i in 1 to nb_cycle loop
                wait until falling_edge(clk_sti);
                wait for 2 ns;
            end loop;
        end cycle;

    
        procedure GenSensorSignalFromPosition(
            position: in integer;
            signal channel_A,channel_B:out std_logic) is

            variable dist_from_zero_v : integer;
            variable ok: boolean;
        begin
            dist_from_zero_v := position mod NB_POS_PER_REVOL;

            --   if    (dist_from_zero_v mod 8 = 0) or (dist_from_zero_v mod 8 = 4) then
            if    (dist_from_zero_v mod 4 = 0) then
                channel_A<='0';
                channel_B<='0';
            --   elsif (dist_from_zero_v mod 8 = 1) or (dist_from_zero_v mod 8 = 5) then
            elsif (dist_from_zero_v mod 4 = 1) then
                channel_A<='1';
                channel_B<='0';
            elsif (dist_from_zero_v mod 4 = 2) then
                channel_A<='1';
                channel_B<='1';
            elsif (dist_from_zero_v mod 4 = 3) then
                channel_A<='0';
                channel_B<='1';
            end if;
        end procedure GenSensorSignalFromPosition;
    
        procedure turn(N : in integer; PositionDuration: in time ) is
            variable J : integer := N;
        begin
            while J /= 0 loop
                if J < 0 then
                    J := J+1;
                    NumPos := NumPos - 1;
                else
                    J := J-1;
                    NumPos := NumPos + 1;
                end if;
                GenSensorSignalFromPosition(NumPos, encoder_FR_a_sti, encoder_FR_b_sti);
                wait for PositionDuration;
            end loop;
        end procedure turn;
  
    begin

        GenSensorSignalFromPosition(NumPos, encoder_FR_a_sti, encoder_FR_b_sti);
        
        cycle(5);
        
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        write(l_v,now);write(l_v,string'(": generate signals for front right encoder while moving clockwise"));writeline(output,l_v);
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        DureeParPosition_v := 1 us;
        for i in 1 to 3 loop
            for i in 1 to NB_POS_PER_REVOL loop
                turn(1,DureeParPosition_v);
            end loop;
        end loop;
        wait for 0 ns;
        
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        write(l_v,now);write(l_v,string'(": generate signals for front right encoder while moving counterclockwise"));writeline(output,l_v);
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        
        for i in 1 to 3 loop
            for i in 1 to NB_POS_PER_REVOL loop
                turn(-1,DureeParPosition_v);
            end loop;
        end loop;
        wait for 0 ns;

        wait;
    end process;
  
    -- front left sensor signal generation
    FL_sensor : process
        variable l_v    :line;
        variable NumPos : integer:=0;
        variable DureeParPosition_v : time;

        procedure cycle(nb_cycle : integer := 1) is
        begin
            for i in 1 to nb_cycle loop
                wait until falling_edge(clk_sti);
                wait for 2 ns;
            end loop;
        end cycle;

    
        procedure GenSensorSignalFromPosition(
            position: in integer;
            signal channel_A,channel_B:out std_logic) is

            variable dist_from_zero_v : integer;
            variable ok: boolean;
        begin
            dist_from_zero_v := position mod NB_POS_PER_REVOL;

            if    (dist_from_zero_v mod 4 = 0) then
                channel_A<='0';
                channel_B<='0';
            elsif (dist_from_zero_v mod 4 = 1) then
                channel_A<='1';
                channel_B<='0';
            elsif (dist_from_zero_v mod 4 = 2) then
                channel_A<='1';
                channel_B<='1';
            elsif (dist_from_zero_v mod 4 = 3) then
                channel_A<='0';
                channel_B<='1';
            end if;
        end procedure GenSensorSignalFromPosition;
    
        procedure turn(N : in integer; PositionDuration: in time ) is
            variable J : integer := N;
        begin
            while J /= 0 loop
                if J < 0 then
                    J := J+1;
                    NumPos := NumPos - 1;
                else
                    J := J-1;
                    NumPos := NumPos + 1;
                end if;
                GenSensorSignalFromPosition(NumPos, encoder_FL_a_sti, encoder_FL_b_sti);
                wait for PositionDuration;
            end loop;
        end procedure turn;
  
    begin

        GenSensorSignalFromPosition(NumPos, encoder_FL_a_sti, encoder_FL_b_sti);
        
        cycle(5);
        
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        write(l_v,now);write(l_v,string'(": generate signals for front left encoder while moving clockwise"));writeline(output,l_v);
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        DureeParPosition_v := 900 ns;
        for i in 1 to 3 loop
            for i in 1 to NB_POS_PER_REVOL loop
                turn(1,DureeParPosition_v);
            end loop;
        end loop;
        wait for 0 ns;
        
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        write(l_v,now);write(l_v,string'(": generate signals for front left encoder while moving counterclockwise"));writeline(output,l_v);
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        
        for i in 1 to 3 loop
            for i in 1 to NB_POS_PER_REVOL loop
                turn(-1,DureeParPosition_v);
            end loop;
        end loop;
        wait for 0 ns;

        wait;
    end process;
  
    -- frbackont right sensor signal generation
    BR_sensor : process
        variable l_v    :line;
        variable NumPos : integer:=0;
        variable DureeParPosition_v : time;

        procedure cycle(nb_cycle : integer := 1) is
        begin
            for i in 1 to nb_cycle loop
                wait until falling_edge(clk_sti);
                wait for 2 ns;
            end loop;
        end cycle;

    
        procedure GenSensorSignalFromPosition(
            position: in integer;
            signal channel_A,channel_B:out std_logic) is

            variable dist_from_zero_v : integer;
            variable ok: boolean;
        begin
            dist_from_zero_v := position mod NB_POS_PER_REVOL;

            if    (dist_from_zero_v mod 4 = 0) then
                channel_A<='0';
                channel_B<='0';
            elsif (dist_from_zero_v mod 4 = 1) then
                channel_A<='1';
                channel_B<='0';
            elsif (dist_from_zero_v mod 4 = 2) then
                channel_A<='1';
                channel_B<='1';
            elsif (dist_from_zero_v mod 4 = 3) then
                channel_A<='0';
                channel_B<='1';
            end if;
        end procedure GenSensorSignalFromPosition;
    
        procedure turn(N : in integer; PositionDuration: in time ) is
            variable J : integer := N;
        begin
            while J /= 0 loop
                if J < 0 then
                    J := J+1;
                    NumPos := NumPos - 1;
                else
                    J := J-1;
                    NumPos := NumPos + 1;
                end if;
                GenSensorSignalFromPosition(NumPos, encoder_BR_a_sti, encoder_BR_b_sti);
                wait for PositionDuration;
            end loop;
        end procedure turn;
  
    begin

        GenSensorSignalFromPosition(NumPos, encoder_BR_a_sti, encoder_BR_b_sti);
        
        cycle(5);
        
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        write(l_v,now);write(l_v,string'(": generate signals for back right encoder while moving clockwise"));writeline(output,l_v);
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        DureeParPosition_v := 1200 ns;
        for i in 1 to 3 loop
            for i in 1 to NB_POS_PER_REVOL loop
                turn(1,DureeParPosition_v);
            end loop;
        end loop;
        wait for 0 ns;
        
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        write(l_v,now);write(l_v,string'(": generate signals for back right encoder while moving counterclockwise"));writeline(output,l_v);
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        
        for i in 1 to 3 loop
            for i in 1 to NB_POS_PER_REVOL loop
                turn(-1,DureeParPosition_v);
            end loop;
        end loop;
        wait for 0 ns;

        wait;
    end process;
    
    -- back left sensor signal generation
    BL_sensor : process
        variable l_v    :line;
        variable NumPos : integer:=0;
        variable DureeParPosition_v : time;

        procedure cycle(nb_cycle : integer := 1) is
        begin
            for i in 1 to nb_cycle loop
                wait until falling_edge(clk_sti);
                wait for 2 ns;
            end loop;
        end cycle;

    
        procedure GenSensorSignalFromPosition(
            position: in integer;
            signal channel_A,channel_B:out std_logic) is

            variable dist_from_zero_v : integer;
            variable ok: boolean;
        begin
            dist_from_zero_v := position mod NB_POS_PER_REVOL;

            if    (dist_from_zero_v mod 4 = 0) then
                channel_A<='0';
                channel_B<='0';
            elsif (dist_from_zero_v mod 4 = 1) then
                channel_A<='1';
                channel_B<='0';
            elsif (dist_from_zero_v mod 4 = 2) then
                channel_A<='1';
                channel_B<='1';
            elsif (dist_from_zero_v mod 4 = 3) then
                channel_A<='0';
                channel_B<='1';
            end if;
        end procedure GenSensorSignalFromPosition;
    
        procedure turn(N : in integer; PositionDuration: in time ) is
            variable J : integer := N;
        begin
            while J /= 0 loop
                if J < 0 then
                    J := J+1;
                    NumPos := NumPos - 1;
                else
                    J := J-1;
                    NumPos := NumPos + 1;
                end if;
                GenSensorSignalFromPosition(NumPos, encoder_BL_a_sti, encoder_BL_b_sti);
                wait for PositionDuration;
            end loop;
        end procedure turn;
  
    begin

        GenSensorSignalFromPosition(NumPos, encoder_BL_a_sti, encoder_BL_b_sti);
        
        cycle(5);
        
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        write(l_v,now);write(l_v,string'(": generate signals for back left encoder while moving clockwise"));writeline(output,l_v);
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        DureeParPosition_v := 800 ns;
        for i in 1 to 3 loop
            for i in 1 to NB_POS_PER_REVOL loop
                turn(1,DureeParPosition_v);
            end loop;
        end loop;
        wait for 0 ns;
        
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        write(l_v,now);write(l_v,string'(": generate signals for back left encoder while moving counterclockwise"));writeline(output,l_v);
        write(l_v,string'("************************************************************"));writeline(output,l_v);
        
        for i in 1 to 3 loop
            for i in 1 to NB_POS_PER_REVOL loop
                turn(-1,DureeParPosition_v);
            end loop;
        end loop;
        wait for 0 ns;

        wait;
    end process;

end testbench;
