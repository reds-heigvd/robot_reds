-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : timer.vhd
-- Description  : timer to generate a pulse every "value" clock cycle
--
-- Author       : Sebastien Masle
-- Date         : 25.03.2021
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       SMS    25.03.2021         Creation
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity timer is
    port(
        clock_i     : in  std_logic; --system clock
        reset_i     : in  std_logic; --asynchronous reset
        enable_i    : in  std_logic;
        init_i      : in  std_logic;
        load_val_i  : in  std_logic_vector(31 downto 0);
        timer_top_o : out std_logic
        );
end timer;

architecture struct of timer is

    signal countdown_s : unsigned(31 downto 0);

begin

    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            countdown_s <= (others => '0');
        elsif rising_edge(clock_i) then
            if init_i = '1' or countdown_s = 0 then
                countdown_s <= unsigned(load_val_i);
            elsif enable_i = '1' then
                countdown_s <= countdown_s - 1;
            end if;
        end if;
    end process;

    timer_top_o <= '1' when countdown_s = 0 else
                   '0';

end struct;