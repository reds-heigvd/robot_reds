--------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : acqu_pos.vhd 
-- Description  : Acquisition de la position de la table tournante
-- 
-- Auteur       : Etienne Messerli
-- Date         : 07.12.2015 
--
-- Utilise      : Labo csn, décembre 2015 
--  
--------------------------------------------------------------------------
-- Description : Acquisition de la position de la table tournante
--   -mesure de la position de la table, comptage incréments capteurs
--      traitement des signaux A et B de l'encodeur
--   -comptage du nombre d'erreur de l'encodeur
--   -MSS de gestion de l'encodeur de position (capt A-B)
--      sens_hr : actif si sens horaire
--      Inc_up, Inc_dn: impulsion pour comptage position
--      det_err: indique double changement simultane de A et B
--        
--| Modifications |-----------------------------------------------------------
-- Ver  Date        Qui   Description
-- 0.1  14.01.2015  EMI   version initiale "mgn_position.vhd"
-- 1.0  07.12.2015  EMI   Adaptation pour le labo de décembre 2015
--
-----------------------------------------------------------------------

library IEEE;
use IEEE.Std_Logic_1164.all;  -- Defini le type standard logic

entity acqu_pos is
  port (clock_i       : in  std_logic;  --Horloge du systeme
        reset_i       : in  std_logic;  --Remise a Zero asychrone
        init_pos_i    : in  std_logic;  --Initialisation a zero, sychrone, des compteurs (pos, idx, err)
        capt_a_i      : in  std_logic;  --Encodeur phase A
        capt_b_i      : in  std_logic;  --Encodeur phase B
        dir_cw_o      : out std_logic;  --Direction: '1' CW (horaire), '0' CCW (anti-horaire)
        position_o    : out std_logic_vector(31 downto 0); --position
        trigger_o     : out std_logic;  --Indication d'un changement de position de l'encodeur
        det_err_o     : out std_logic;  --Detection d'une erreur (double changement A et B)
        err_o         : out std_logic;  --Il y a eu une erreur de double changement A et B)
        nbr_err_o     : out std_logic_vector(4 downto 0)  --nombre d'erreur detectees
  );
end acqu_pos;

architecture struct of acqu_pos is 
   --| internal signal declarations |-------------------------------------
  signal IncUp_s, IncDn_s : std_logic;
  signal cnt_pos_UpnDn_s  : std_logic;
  signal cnt_pos_en_s     : std_logic;
  signal position_s       : std_logic_vector(position_o'range);
  signal inc_err_s        : std_logic;
  signal cnt_err_zero_s   : std_logic;
  
  signal capt_a_sync1_s, capt_a_sync2_s     : std_logic;
  signal capt_b_sync1_s, capt_b_sync2_s     : std_logic;
  

   --| component declaration |--------------------------------------------
  component det_rot_ab 
    port (reset_i    : in  Std_Logic;  --Remise a Zero asychrone
          clock_i    : in  std_logic;  --Horloge du systeme
          capt_a_i   : in  Std_Logic;  --Capteurs phase A
          capt_b_i   : in  Std_Logic;  --Capteurs phase B
          pos_up_o   : out std_logic;  --Impulsion pour incr. cpt position
          pos_dn_o   : out std_logic;  --Impulsion pour decr. cpt position
          sens_hr_o  : out std_logic;  --Indication sens: '1' horaire, '0' anti-horaire
          det_err_o  : out std_logic); --Detection d'une erreur (double changment de A et B)
   end component;
   for all : det_rot_ab use entity work.det_rot_ab;

   
   component cpt_UpDn_gen 
    generic(TAILLE   : natural := 16);
    port(clock_i     : in  std_logic ;
        reset_i     : in  std_logic ; --remise a zero asynchrone
        --les commandes suivantes sont TOUTES synchrones
        load_i      : in  std_logic ; --chargement
        init_zero_i : in  std_logic ; --remise a zero
        UpnDn_i     : in  std_logic ; --selection du mode up ou down
        en_i        : in  std_logic ; --autorisation comptage/decomptage
        --autres ports
        val_i       : in  std_logic_vector (TAILLE - 1 downto 0);
        cpt_o       : out std_logic_vector (TAILLE - 1 downto 0);
        eq_zero_o   : out std_logic 
        );
   end component;
   for all : cpt_UpDn_gen use entity work.cpt_UpDn_gen;
   
   component ff_d
     port(clk_i   : in     std_logic;
          reset_i : in     std_logic;
          d_i     : in     std_logic;
          q_o     : out    std_logic  );
   end component;
   for all : ff_d use entity work.ff_d;



begin
  --polarity adaptation
  
  --signal synchronisation with 2 flip-flop
  I_ff_a1: ff_d port map(
          clk_i   => clock_i,
          reset_i => reset_i,
          d_i     => capt_a_i,
          q_o     => capt_a_sync1_s  );

  I_ff_a2: ff_d port map(
          clk_i   => clock_i,
          reset_i => reset_i,
          d_i     => capt_a_sync1_s,
          q_o     => capt_a_sync2_s  );

  I_ff_b1: ff_d port map(
          clk_i   => clock_i,
          reset_i => reset_i,
          d_i     => capt_b_i,
          q_o     => capt_b_sync1_s  );

  I_ff_b2: ff_d port map(
          clk_i   => clock_i,
          reset_i => reset_i,
          d_i     => capt_b_sync1_s,
          q_o     => capt_b_sync2_s );


  -- Instance port mappings.
  I_det_rot : det_rot_ab port map (
       reset_i    => reset_i,
       clock_i    => clock_i,
       capt_a_i   => capt_a_sync2_s,
       capt_b_i   => capt_b_sync2_s,
       pos_up_o   => IncUp_s,
       pos_dn_o   => IncDn_s,
       sens_hr_o  => dir_cw_o,
       det_err_o  => inc_err_s 
  );

  --| counters |-------------------------------------------------------- 
  cnt_pos_en_s    <= IncUp_s or IncDn_s;
  cnt_pos_UpnDn_s <= IncUp_s;
   
  -- position counter
  I_cnt_pos : cpt_UpDn_gen 
    generic map(TAILLE => position_o'length)
    port map (
      clock_i      => clock_i,
      reset_i      => reset_i,
      load_i       => '0',
      init_zero_i  => init_pos_i,
      UpnDn_i      => cnt_pos_UpnDn_s,
      en_i         => cnt_pos_en_s,
      val_i        => (others => '0'),
      cpt_o        => position_s,
      eq_zero_o    => open
  );

  -- error counter  
  I_cnt_err : cpt_UpDn_gen 
    generic map(TAILLE => nbr_err_o'length)
    port map (
      clock_i      => clock_i,
      reset_i      => reset_i,
      load_i       => '0',
      init_zero_i  => init_pos_i,
      UpnDn_i      => '1', --always mode up
      en_i         => inc_err_s,
      val_i        => (others => '0'),
      cpt_o        => nbr_err_o,
      eq_zero_o    => cnt_err_zero_s
  );


  --| output assignment |-------------------------------------------------
  position_o <= position_s;
  trigger_o  <= cnt_pos_en_s;
  err_o      <= not cnt_err_zero_s;  --il y a des erreurs detectees
  det_err_o  <= inc_err_s; --pulse detection erreur
  --revol_o    <= x"AA"; --default value, not used momentarily

end struct;
