-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : det_rot_ab.vhd 
-- Description  : Detecteur de rotation AB
-- 
-- Auteur       : Etienne Messerli
-- Date         : 18.12.2012 
--
-- Utilise      : Labo IFS, novembre 2013  
--  
--| Modifications |-----------------------------------------------------------
-- Version   Auteur Date               Description
--
-----------------------------------------------------------------------
-- Description du fonctionnement : Detecteur de sens de rotation
--   Detecteur de sens de rotation avec soirtes:
--     sens_hr : actif si sens horaire
--     Inc_up, Inc_dn: impulsion pour comptage position
--        
--| Modifications |-----------------------------------------------------------
-- Ver  Date        Qui   Description
-- 1.0  09.01.2015  EMI   Adapter style ecriture indentificateurs
--                        Ajout de la gestion de l'indexe
-- 1.1  05.01.2017  EMI   Info: gestion cpt index pas fiable !
--                        Erreur détectée avec le tb de Rey
-- 1.2  27.11.2019  SMS   Suppression de la gestion de l'index
--
-----------------------------------------------------------------------

library IEEE;
use IEEE.Std_Logic_1164.all;  -- Defini le type standard logic

entity det_rot_ab is
  port (reset_i    : in  Std_Logic;  --Remise a Zero asychrone
        clock_i    : in  std_logic;  --Horloge du systeme
        capt_a_i   : in  Std_Logic;  --Capteurs phase A
        capt_b_i   : in  Std_Logic;  --Capteurs phase B
        pos_up_o   : out std_logic;  --Impulsion pour incr. cpt position
        pos_dn_o   : out std_logic;  --Impulsion pour decr. cpt position
        sens_hr_o  : out std_logic;  --Indication sens: '1' horaire, '0' anti-horaire
        det_err_o  : out std_logic); --Detection d'une erreur (double changment de A et B)
end det_rot_ab;

architecture M_Etats of det_rot_ab is 

  signal Etat_fut, Etat_pres : Std_Logic_Vector(6 downto 0);

  --Definition du codage a l'aide de constante
  -- signification des bits du code des etats:
  -- bit    6     5  4      3       2         1        0
  --       '-'  num_seq  det_err  sens_hr   pos_dn   pos_up
  --
  -- si bit 6 = '0' etats reset et erreur
  -- si bit 6 = '1' etat de fct normal
  constant E_reset   : Std_Logic_Vector(6 downto 0) := "0000000"; -- etat lors d'un reset 
  constant E_er_hr_p : Std_Logic_Vector(6 downto 0) := "0011000"; -- erreur double changement AB sens horaire, pulse Err
  constant E_er_ah_p : Std_Logic_Vector(6 downto 0) := "0101000"; -- erreur double changement AB sens anti-horaire, pulse Err

  constant E_hr_0p : Std_Logic_Vector(6 downto 0) := "1000101"; -- 1er etat avec AB = 00 sens horaire, pulse pos_up
  constant E_hr_0  : Std_Logic_Vector(6 downto 0) := "1000100"; -- etat sens horaire avec AB = 00 stable
  constant E_hr_1p : Std_Logic_Vector(6 downto 0) := "1010101"; -- 1er etat avec AB = 01 sens horaire, pulse pos_up
  constant E_hr_1  : Std_Logic_Vector(6 downto 0) := "1010100"; -- etat sens horaire avec AB = 01 stable
  constant E_hr_3p : Std_Logic_Vector(6 downto 0) := "1110101"; -- 1er etat avec AB = 11 sens horaire, pulse pos_up
  constant E_hr_3  : Std_Logic_Vector(6 downto 0) := "1110100"; -- etat sens horaire avec AB = 11 stable
  constant E_hr_2p : Std_Logic_Vector(6 downto 0) := "1100101"; -- 1er etat avec AB = 10 sens horaire, pulse pos_up
  constant E_hr_2  : Std_Logic_Vector(6 downto 0) := "1100100"; -- etat sens anti-horaire avec AB = 10 stable
  constant E_ah_0p : Std_Logic_Vector(6 downto 0) := "1000010"; -- 1er etat avec AB = 00 sens anti-horaire, pulse pos_dn
  constant E_ah_0  : Std_Logic_Vector(6 downto 0) := "1000000"; -- etat sens anti-horaire avec AB = 00 stable
  constant E_ah_1p : Std_Logic_Vector(6 downto 0) := "1010010"; -- 1er etat avec AB = 01 sens anti-horaire, pulse pos_dn
  constant E_ah_1  : Std_Logic_Vector(6 downto 0) := "1010000"; -- etat sens anti-horaire avec AB = 01 stable
  constant E_ah_3p : Std_Logic_Vector(6 downto 0) := "1110010"; -- 1er etat avec AB = 11 sens anti-horaire, pulse pos_dn
  constant E_ah_3  : Std_Logic_Vector(6 downto 0) := "1110000"; -- etat sens anti-horaire avec AB = 11 stable
  constant E_ah_2p : Std_Logic_Vector(6 downto 0) := "1100010"; -- 1er etat avec AB = 10 sens anti-horaire, pulse pos_dn
  constant E_ah_2  : Std_Logic_Vector(6 downto 0) := "1100000"; -- etat sens anti-horaire avec AB = 10 stable

begin
  -- decodeur d'etat futur
  Fut: process (capt_b_i, capt_a_i, Etat_pres)
  begin
    Etat_fut <= E_reset;  -- etat par defaut
    case Etat_pres is

    -- gestion cas reset et erreur ---------------------------------------------
      when E_reset =>  -- par defaut nous allons dans les etats sens horaire stable
        if ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_0; -- aller etat stable 0 horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_1; -- aller etat stable 1 horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_3; -- aller etat stable 3 horaire
        else --((capt_b_i = '1') and (capt_a_i = '0')) 
           Etat_fut <= E_hr_2; -- aller etat stable 2 horaire
        end if;
      when E_er_hr_p =>  --erreur double changement AB sens horaire, pulse Err
                         -- puis retour etat stable sens horaire
        if ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_0; -- aller etat stable 0 horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_1; -- aller etat stable 1 horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_3; -- aller etat stable 3 horaire
        else --((capt_b_i = '1') and (capt_a_i = '0')) 
           Etat_fut <= E_hr_2; -- aller etat stable 2 horaire
        end if;
      when E_er_ah_p =>  --erreur double changement AB sens horaire, pulse Err
                         -- puis retour etat stable sens anti-horaire
        if ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_0; -- aller etat stable 0 anti-horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_1; -- aller etat stable 1 anti-horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_3; -- aller etat stable 3 anti-horaire
        else --((capt_b_i = '1') and (capt_a_i = '0')) 
           Etat_fut <= E_ah_2; -- aller etat stable 2 anti-horaire
        end if;

    -- gestion sens horaire ----------------------------------------------------
      when E_hr_0p => --1er etat 1 avec AB = 00 sens horaire
        if ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_0; -- aller etat stable 0 horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_1p; -- aller 1er etat 1 horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_2p; -- aller 1er etat 2 anti-horaire 
        else -- ((capt_b_i = '1') and (capt_a_i = '1')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_hr_p;
        end if;
      when E_hr_0 => --etat 0 stable avec AB = 00 sens horaire
        if ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_0; -- rester etat stable 0 horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_1p; -- aller 1er etat 1 horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_2p; -- aller 1er etat 2 anti-horaire 
        else -- ((capt_b_i = '1') and (capt_a_i = '1')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_hr_p;
        end if;
      when E_hr_1p =>  --1er etat 1 avec AB = 01 sens horaire
        if ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_1; -- aller etat stable 1 horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_3p; -- aller 1er etat 3 horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_0p; -- aller 1er etat 0 anti-horaire 
        else -- ((capt_b_i = '1') and (capt_a_i = '0')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_hr_p;
        end if;
      when E_hr_1 =>  --etat 1 stable avec AB = 01 sens horaire
        if ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_1; -- rester etat stable 1 horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_3p; -- aller 1er etat 3 horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_0p; -- aller 1er etat 0 anti-horaire 
        else -- ((capt_b_i = '1') and (capt_a_i = '0')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_hr_p;
        end if;
      when E_hr_3p =>  --1er etat 3 avec AB = 11 sens horaire
        if ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_3; -- aller etat stable 3 horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_2p; -- aller 1er etat 2 horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_1p; -- aller 1er etat 1 anti-horaire 
        else -- ((capt_b_i = '0') and (capt_a_i = '0')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_hr_p;
        end if;
      when E_hr_3 =>  --etat 3 stable avec AB = 11 sens horaire
        if ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_3; -- rester etat stable 3 horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_2p; -- aller 1er etat 2 horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_1p; -- aller 1er etat 1 anti-horaire 
        else -- ((capt_b_i = '0') and (capt_a_i = '0')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_hr_p;
        end if;
      when E_hr_2p =>  --1er etat 2 avec AB = 10 sens horaire
        if ((capt_b_i = '1') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_2; -- aller etat stable 2 horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_0p; -- aller 1er etat 0 horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_3p; -- aller 1er etat 3 anti-horaire 
        else -- ((capt_b_i = '0') and (capt_a_i = '1')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_hr_p;
        end if;
      when E_hr_2 =>  --etat 2 stable avec AB = 10 sens horaire
        if ((capt_b_i = '1') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_2; -- rester etat stable 2 horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_0p; -- aller 1er etat 0 horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_3p; -- aller 1er etat 3 anti-horaire 
        else -- ((capt_b_i = '0') and (capt_a_i = '1')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_hr_p;
        end if;
    
    -- gestion sens anti-horaire -----------------------------------------------
      when E_ah_0p =>  --1er etat 0 avec AB = 00 sens anti-horaire
        if ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_0; -- aller etat stable 0 anti-horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_2p; -- aller 1er etat 2 anti-horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_1p; -- aller 1er etat 1 horaire 
        else -- ((capt_b_i = '1') and (capt_a_i = '1')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_ah_p;
        end if;
      when E_ah_0 => --etat 0 stable avec AB = 00 sens anti-horaire
        if ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_0; -- rester etat stable 0 anti-horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_2p; -- aller 1er etat 2 anti-horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_1p; -- aller 1er etat 1 horaire 
        else -- ((capt_b_i = '1') and (capt_a_i = '1')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_ah_p;
        end if;
      when E_ah_2p =>  --1er etat 2 avec AB = 10 sens anti-horaire
        if ((capt_b_i = '1') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_2; -- aller etat stable 2 anti-horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_3p; -- aller 1er etat 3 anti-horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_0p; -- aller 1er etat 0 horaire 
        else -- ((capt_b_i = '0') and (capt_a_i = '1')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_ah_p;
        end if;
      when E_ah_2 =>  --etat 2 stable avec AB = 10 sens anti-horaire
        if ((capt_b_i = '1') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_2; -- rester etat stable 2 anti-horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_3p; -- aller 1er etat 0 anti-horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_0p; -- aller 1er etat 0 horaire 
        else -- ((capt_b_i = '0') and (capt_a_i = '1')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_ah_p;
        end if;
      when E_ah_3p =>  --1er etat 3 avec AB = 11 sens anti-horaire
        if ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_3; -- aller etat stable 3 anti-horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_1p; -- aller 1er etat 1 anti-horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_2p; -- aller 1er etat 2 horaire 
        else -- ((capt_b_i = '0') and (capt_a_i = '0')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_ah_p;
        end if;
      when E_ah_3 =>  --etat 3 stable avec AB = 11 sens anti-horaire
        if ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_3; -- rester etat stable 3 anti-horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_1p; -- aller 1er etat 1 anti-horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '0')) then
           Etat_fut <= E_hr_2p; -- aller 1er etat 2 horaire 
        else -- ((capt_b_i = '0') and (capt_a_i = '0')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_ah_p;
        end if;
      when E_ah_1p =>  --1er etat 1 avec AB = 01 sens anti-horaire
        if ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_1; -- aller etat stable 1 anti-horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_0p; -- aller 1er etat 3 anti-horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_3p; -- aller 1er etat 3 horaire 
        else -- ((capt_b_i = '1') and (capt_a_i = '0')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_ah_p;
        end if;
      when E_ah_1 =>  --etat 1 stable avec AB = 01 sens anti-horaire
        if ((capt_b_i = '0') and (capt_a_i = '1')) then
           Etat_fut <= E_ah_1; -- rester etat stable 1 anti-horaire
        elsif ((capt_b_i = '0') and (capt_a_i = '0')) then
           Etat_fut <= E_ah_0p; -- aller 1er etat 3 anti-horaire
        elsif ((capt_b_i = '1') and (capt_a_i = '1')) then
           Etat_fut <= E_hr_3p; -- aller 1er etat 3 horaire 
        else -- ((CaptB_i = '1') and (capt_a_i = '0')) 
             -- double changement de AB => erreur
           Etat_fut <= E_er_ah_p;
        end if;

    -- autre combinaison => etat reset -----------------------------------------
      when others =>
           Etat_fut <= E_reset;

    end case;
  end process;


  --process synchrone, mise a jours des bits d'etats
  Mem:process (reset_i, clock_i)
  begin
    if reset_i = '1' then
      -- remise a zero asynchrone 
      Etat_Pres <= E_reset;
    elsif rising_edge(clock_i) then
      Etat_Pres <= Etat_fut;
     end if;
  end process;

  -- decodeur de sortie --------------------------------------------------------
  -- signification des bits du code des etats:
  -- bit    6     5  4     3       2        1       0
  --       '-'  num_seq  DetErr  SensHr   IncDn   IncUp
  -- si bit 6 = '0' etats reset et erreur
  -- si bit 6 = '1' etat de fct normal

  pos_up_o  <= Etat_Pres(0);
  pos_dn_o  <= Etat_Pres(1);
  
--| Gestion cpt index pas fiable |---------------------------------------------
-- il y a une erreur si inversion de direction durant la phase P0 (cas 3 capteurs sont à 0)
-- cela est detecte par le tb de Bertrand Rey

  -- pulse indexe si pos_up/pos_dn actif
  -- ET num sequence AB = 00, bits Etat_pres(5..4) = "00"
  sens_hr_o <= Etat_Pres(2);
  det_err_o <= Etat_Pres(3);


end M_Etats;
