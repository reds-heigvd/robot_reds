-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : ff_d.vhd_.vhd
--    rem: composant nomme differement que le standard DFF
--         (used by tools like: Altera, Xilinx, ...)
--
-- Description  : 
-- 
-- Auteur       : Etienne Messerli
-- Date         : 20.10.2014
-- Version      : 0.0
-- 
-- Utilise      : Divers projet de labo
-- 
--| Modifications |------------------------------------------------------------
-- Version   Auteur Date               Description
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity ff_d is
  port(clk_i   : in     std_logic;
       reset_i : in     std_logic;
       d_i     : in     std_logic;
       q_o     : out    std_logic  );
end ff_d ;

architecture comport of ff_d is
begin
  process(reset_i, clk_i)
  begin
    if reset_i = '1' then
      q_o  <= '0';
    elsif Rising_Edge(clk_i) then
      q_o  <= d_i;
    end if;
  end process;
end comport;

