-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : cpt_UpDn_gen.vhd
-- Description  : Compteur up/down generic avec commande
--           - reset      : remise a zero asynchrone
--           - load       : chargement synchrone
--           - init_zero  : remise a zero SYNCHRONE
--           - UpnDown    : choix mode comptage ('1') ou decomptage ('0')
--           - enable     : autorisation mode comptage ou decomptage
--         priorite : reset (asych.), load, init_zero, Up/Down, hold
--
-- Auteur       : Jean-Pierre Miceli
-- Date         : 01.09.2004
-- Version      : 2.0
--
-- Utilise      : Exercice formation VHDL
--
--| Modifications |-----------------------------------------------------------
-- Version   Auteur Date               Description
-- 2.0       EMI    04.01.2015         Mise a jour de la description pour 
--                                     labo CSN
--
------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity cpt_UpDn_gen is
   generic(TAILLE   : natural := 16);
   port(clock_i     : in  std_logic ;
        reset_i     : in  std_logic ; --remise a zero asynchrone
        --les commandes suivantes sont TOUTES synchrones
        load_i      : in  std_logic ; --chargement
        init_zero_i : in  std_logic ; --remise a zero
        UpnDn_i     : in  std_logic ; --selection du mode up ou down
        en_i        : in  std_logic ; --autorisation comptage/decomptage
        --autres ports
        val_i       : in  std_logic_vector (TAILLE - 1 downto 0);
        cpt_o       : out std_logic_vector (TAILLE - 1 downto 0);
        eq_zero_o   : out std_logic 
        );
end Cpt_UpDn_gen ;

architecture rtl of Cpt_UpDn_gen is
  
  -- signaux internes
  signal cpt_pres_s, cpt_fut_s : unsigned(cpt_o'range);
  
begin
  
  --Decodeur d'etats futurs
  cpt_fut_s <= 
      unsigned(val_i) when load_i = '1'                 else -- chargement
      (others => '0') when init_zero_i = '1'            else --mise a zero
      cpt_pres_s + 1  when en_i = '1' and UpnDn_i = '1' else --comptage
      cpt_pres_s - 1  when en_i = '1' and UpnDn_i = '0' else --decomptage
      cpt_pres_s;                                            --maintient
  
  --fonction de mémorisation
  process(clock_i, reset_i)
  begin 
    if (reset_i = '1') then 
      cpt_pres_s <= (others => '0');
    elsif rising_edge(clock_i) then
      cpt_pres_s <= cpt_fut_s;
    end if;
  end process;
  
  --Affectation des sorties
  cpt_o     <= std_logic_vector(cpt_pres_s);
  eq_zero_o <= '1' when cpt_pres_s = 0 else '0';
  
end rtl;

