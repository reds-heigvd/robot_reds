-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : speed_compute.vhd
-- Description  : gets a position at regular intervals and calculates a speed
--
-- Author       : Sebastien Masle
-- Date         : 25.03.2021
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       SMS    25.03.2021         Creation
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity speed_compute is
    port(
        clock_i     : in  std_logic; --system clock
        reset_i     : in  std_logic; --asynchronous reset
        timer_top_i : in  std_logic;
        init_i      : in  std_logic;
        init_val_i  : in  std_logic_vector(31 downto 0);
        trigger_i   : in  std_logic;
        dir_i       : in  std_logic;
        speed_o     : out std_logic_vector(31 downto 0)
        );
end speed_compute;

architecture struct of speed_compute is

    signal speed_s   : signed(31 downto 0);
    signal dir_s     : std_logic;
    signal inv_dir_s : std_logic;

begin

    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            dir_s     <= '0';
            inv_dir_s <= '0';
        elsif rising_edge(clock_i) then
            if init_i = '1' or timer_top_i = '1' then
                dir_s     <= dir_i;
                inv_dir_s <= '0';
            else
                if dir_s /= dir_i then
                    dir_s <= dir_i;
                    inv_dir_s <= not inv_dir_s;
                end if;
            end if;
        end if;
    end process;

    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            speed_s <= (others => '0');
        elsif rising_edge(clock_i) then
            if init_i = '1' then
                speed_s <= signed(init_val_i);
            elsif timer_top_i = '1' then
                speed_s <= (others => '0');
                dir_s   <= dir_i;
            elsif trigger_i = '1' then
                if inv_dir_s = '0' then
                    speed_s <= speed_s + 1;
                else
                    speed_s <= speed_s - 1;
                end if;
            end if;
        end if;
    end process;

    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            speed_o <= (others => '0');
        elsif rising_edge(clock_i) then
            if timer_top_i = '1' then
                if speed_s >= 0 then
                    speed_o <= std_logic_vector(speed_s);
                else
                    speed_o <= std_logic_vector(-speed_s);
                end if;
            end if;
        end if;
    end process;

end struct;