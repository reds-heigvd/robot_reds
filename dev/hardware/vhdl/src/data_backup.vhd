-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : data_backup.vhd
-- Description  : backup input data when memorize signal occurs
--
-- Author       : Sebastien Masle
-- Date         : 25.03.2021
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       SMS    25.03.2021         Creation
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity data_backup is
    port(
        clock_i    : in  std_logic; --system clock
        reset_i    : in  std_logic; --asynchronous reset
        init_i     : in  std_logic;
        memorize_i : in  std_logic;
        init_val_i : in  std_logic_vector(31 downto 0);
        data_i     : in  std_logic_vector(31 downto 0);
        mem_data_o : out std_logic_vector(31 downto 0)
        );
end data_backup;

architecture struct of data_backup is

    signal prev_positions_s : unsigned(31 downto 0);

begin

    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            mem_data_o <= (others => '0');
        elsif rising_edge(clock_i) then
            if memorize_i = '1' then
                mem_data_o <= data_i;
            end if;
        end if;
    end process;

end struct;