-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : agribot_sensor_top.vhd
-- Description  : top design for agribot sensor baord project
--
-- Author       : Sebastien Masle
-- Date         : 25.03.2021
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       SMS    25.03.2021         Creation
-------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library work;
    use work.function_pkg.all;
    use work.i2c_slave_pkg.all;

entity agribot_sensor_top is
    port(
        clock_25MHz_i  : in  std_logic;
        nReset_i       : in  std_logic;
        --i2c signals
        sda_io         : inout std_logic; 
        scl_i          : in    std_logic;
        -- incremental encoders signals (F stands for Front - B stands for Back
        --                               R stands for Right - L stands for Left)
        encoder_FR_a_i : in  std_logic;  -- FR encoder - A signal
        encoder_FR_b_i : in  std_logic;  -- FR encoder - B signal
        encoder_FL_a_i : in  std_logic;  -- FL encoder - A signal
        encoder_FL_b_i : in  std_logic;  -- FL encoder - B signal
        encoder_BR_a_i : in  std_logic;  -- BR encoder - A signal
        encoder_BR_b_i : in  std_logic;  -- BR encoder - B signal
        encoder_BL_a_i : in  std_logic;  -- BL encoder - A signal
        encoder_BL_b_i : in  std_logic); -- BL encoder - B signal
end entity agribot_sensor_top;

architecture struct of agribot_sensor_top is

    component pll_100MHz is
        port(inclk0 : in  std_logic  := '0';
             c0     : out std_logic
        );
    end component;

    component i2c_slave is
        generic(extended_address_g   : boolean := false;
                register_size_g      : natural; -- register size in bytes: must be 1, 2 or 4.
                i2c_slave_address_g  : std_logic_vector((bool_to_std_logic(EXTENDED_ADDRESS) * 3) + 6 downto 0)
                );
        port(clock_i       : in    std_logic; --system clock
             reset_i       : in    std_logic; --asynchronous reset
             -- application inputs and outputs
             command_o     : out std_logic_vector((register_size_g * 8) - 1 downto 0);
             status_i      : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
             FR_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
             FL_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
             BR_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
             BL_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
             FR_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
             FL_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
             BR_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
             BL_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
             interval_o    : out std_logic_vector((register_size_g * 8) - 1 downto 0);
             -- i2c clock and data
             scl_i         : in    std_logic;
             sda_io        : inout std_logic);
    end component;

    component acqu_pos
        port (clock_i       : in  std_logic;  --Horloge du systeme
              reset_i       : in  std_logic;  --Remise a Zero asychrone
              init_pos_i    : in  std_logic;  --Initialisation a zero, sychrone, des compteurs (pos, idx, err)
              capt_a_i      : in  std_logic;  --Encodeur phase A
              capt_b_i      : in  std_logic;  --Encodeur phase B
              dir_cw_o      : out std_logic;  --Direction: '1' CW (horaire), '0' CCW (anti-horaire)
              position_o    : out std_logic_vector(31 downto 0); --position
              trigger_o     : out std_logic;  --Indication d'un changement de position de l'encodeur
              det_err_o     : out std_logic;  --Detection d'une erreur (double changement A et B)
              err_o         : out std_logic;  --Il y a eu une erreur de double changement A et B)
              nbr_err_o     : out std_logic_vector(4 downto 0)  --nombre d'erreur detectees
        );
    end component;

    component speed_compute is
        port(
            clock_i     : in  std_logic; --system clock
            reset_i     : in  std_logic; --asynchronous reset
            timer_top_i : in  std_logic;
            init_i      : in  std_logic;
            init_val_i  : in  std_logic_vector(31 downto 0);
            trigger_i   : in  std_logic;
            dir_i       : in  std_logic;
            speed_o     : out std_logic_vector(31 downto 0)
            );
    end component;

    component timer is
        port(
            clock_i     : in  std_logic; --system clock
            reset_i     : in  std_logic; --asynchronous reset
            enable_i    : in  std_logic;
            init_i      : in  std_logic;
            load_val_i  : in  std_logic_vector(31 downto 0);
            timer_top_o : out std_logic
            );
    end component;

    component data_backup is
        port(
            clock_i    : in  std_logic; --system clock
            reset_i    : in  std_logic; --asynchronous reset
            init_i     : in  std_logic;
            memorize_i : in  std_logic;
            init_val_i : in  std_logic_vector(31 downto 0);
            data_i     : in  std_logic_vector(31 downto 0);
            mem_data_o : out std_logic_vector(31 downto 0)
            );
    end component;

    constant SYS_FREQ : natural := 100000000; -- system frequency in Hz

    signal clock_100_MHz_s   : std_logic;
    signal reset_s           : std_logic;

    signal command_s         : std_logic_vector(31 downto 0);
    signal status_s          : std_logic_vector(31 downto 0);
    signal FR_position_s     : std_logic_vector(31 downto 0);
    signal FR_dir_s          : std_logic;
    signal FR_trigger_s      : std_logic;
    signal FR_mem_position_s : std_logic_vector(31 downto 0);
    signal FL_position_s     : std_logic_vector(31 downto 0);
    signal FL_dir_s          : std_logic;
    signal FL_trigger_s      : std_logic;
    signal FL_mem_position_s : std_logic_vector(31 downto 0);
    signal BR_position_s     : std_logic_vector(31 downto 0);
    signal BR_dir_s          : std_logic;
    signal BR_trigger_s      : std_logic;
    signal BR_mem_position_s : std_logic_vector(31 downto 0);
    signal BL_position_s     : std_logic_vector(31 downto 0);
    signal BL_dir_s          : std_logic;
    signal BL_trigger_s      : std_logic;
    signal BL_mem_position_s : std_logic_vector(31 downto 0);
    signal FR_speed_s        : std_logic_vector(31 downto 0);
    signal FR_mem_speed_s    : std_logic_vector(31 downto 0);
    signal FL_speed_s        : std_logic_vector(31 downto 0);
    signal FL_mem_speed_s    : std_logic_vector(31 downto 0);
    signal BR_speed_s        : std_logic_vector(31 downto 0);
    signal BR_mem_speed_s    : std_logic_vector(31 downto 0);
    signal BL_speed_s        : std_logic_vector(31 downto 0);
    signal BL_mem_speed_s    : std_logic_vector(31 downto 0);
    signal interval_s        : std_logic_vector(31 downto 0);

    signal reset_FR_pos_s         : std_logic;
    signal reset_FL_pos_s         : std_logic;
    signal reset_BR_pos_s         : std_logic;
    signal reset_BL_pos_s         : std_logic;
    signal FR_position_error_s    : std_logic;
    signal FL_position_error_s    : std_logic;
    signal BR_position_error_s    : std_logic;
    signal BL_position_error_s    : std_logic;
    signal reset_FR_speed_s       : std_logic;
    signal reset_FL_speed_s       : std_logic;
    signal reset_BR_speed_s       : std_logic;
    signal reset_BL_speed_s       : std_logic;
    signal memorize_FR_position_s : std_logic;
    signal memorize_FL_position_s : std_logic;
    signal memorize_BR_position_s : std_logic;
    signal memorize_BL_position_s : std_logic;
    signal memorize_FR_speed_s    : std_logic;
    signal memorize_FL_speed_s    : std_logic;
    signal memorize_BR_speed_s    : std_logic;
    signal memorize_BL_speed_s    : std_logic;

    signal top_ms_s               : std_logic;
    signal top_speed_s            : std_logic;

begin
    
    pll : pll_100MHz
        port map(inclk0 => clock_25MHz_i,
                 c0     => clock_100_MHz_s
        );
    
    reset_s <= not nReset_i;

    slave_i2c : i2c_slave
    generic map(
        extended_address_g  => EXTENDED_ADDRESS,
        register_size_g     => REGISTER_SIZE,
        i2c_slave_address_g => I2C_SLAVE_ADDRESS)
    port map(
        clock_i        => clock_100_MHz_s,
        reset_i        => reset_s,
        -- application inputs and outputs
        command_o      => command_s,
        status_i       => status_s,
        FR_position_i  => FR_mem_position_s,
        FL_position_i  => FL_mem_position_s,
        BR_position_i  => BR_mem_position_s,
        BL_position_i  => BL_mem_position_s,
        FR_speed_i     => FR_mem_speed_s,
        FL_speed_i     => FL_mem_speed_s,
        BR_speed_i     => BR_mem_speed_s,
        BL_speed_i     => BL_mem_speed_s,
        interval_o     => interval_s,
        -- i2c clock and data
        sda_io         => sda_io,
        scl_i          => scl_i
    );

    FR_encoder : acqu_pos
    port map(clock_i    => clock_100_MHz_s,
             reset_i    => reset_s,
             init_pos_i => reset_FR_pos_s,
             capt_a_i   => encoder_FR_a_i,
             capt_b_i   => encoder_FR_b_i,
             dir_cw_o   => FR_dir_s,
             position_o => FR_position_s,
             trigger_o  => FR_trigger_s,
             det_err_o  => open,
             err_o      => FR_position_error_s,
             nbr_err_o  => open
    );

    FL_encoder : acqu_pos
    port map(clock_i    => clock_100_MHz_s,
             reset_i    => reset_s,
             init_pos_i => reset_FL_pos_s,
             capt_a_i   => encoder_FL_a_i,
             capt_b_i   => encoder_FL_b_i,
             dir_cw_o   => FL_dir_s,
             position_o => FL_position_s,
             trigger_o  => FL_trigger_s,
             det_err_o  => open,
             err_o      => FL_position_error_s,
             nbr_err_o  => open
    );

    BR_encoder : acqu_pos
    port map(clock_i    => clock_100_MHz_s,
             reset_i    => reset_s,
             init_pos_i => reset_BR_pos_s,
             capt_a_i   => encoder_BR_a_i,
             capt_b_i   => encoder_BR_b_i,
             dir_cw_o   => BR_dir_s,
             position_o => BR_position_s,
             trigger_o  => BR_trigger_s,
             det_err_o  => open,
             err_o      => BR_position_error_s,
             nbr_err_o  => open
    );

    BL_encoder : acqu_pos
    port map(clock_i    => clock_100_MHz_s,
             reset_i    => reset_s,
             init_pos_i => reset_BL_pos_s,
             capt_a_i   => encoder_BL_a_i,
             capt_b_i   => encoder_BL_b_i,
             dir_cw_o   => BL_dir_s,
             position_o => BL_position_s,
             trigger_o  => BL_trigger_s,
             det_err_o  => open,
             err_o      => BL_position_error_s,
             nbr_err_o  => open
    );

    timer_ms : timer
    port map(clock_i     => clock_100_MHz_s,
             reset_i     => reset_s,
             enable_i    => '1',
             init_i      => '0',
            --  load_val_i  => std_logic_vector(to_unsigned(SYS_FREQ/1000, 32) - 1),
             load_val_i  => std_logic_vector(to_unsigned(SYS_FREQ/1000000, 32) - 1), -- for simulation
             timer_top_o => top_ms_s
    );
    
    timer_speed : timer
    port map(clock_i     => clock_100_MHz_s,
             reset_i     => reset_s,
             enable_i    => top_ms_s,
             init_i      => '0',
             load_val_i  => interval_s,
             timer_top_o => top_speed_s
    );

    FR_speed : speed_compute
    port map(clock_i     => clock_100_MHz_s,
             reset_i     => reset_s,
             timer_top_i => top_speed_s,
             init_i      => reset_FR_speed_s,
             init_val_i  => std_logic_vector(to_unsigned(0, 32)),
             trigger_i   => FR_trigger_s,
             dir_i       => FR_dir_s,
             speed_o     => FR_speed_s
    );

    FL_speed : speed_compute
    port map(clock_i     => clock_100_MHz_s,
             reset_i     => reset_s,
             timer_top_i => top_speed_s,
             init_i      => reset_FL_speed_s,
             init_val_i  => std_logic_vector(to_unsigned(0, 32)),
             trigger_i   => FL_trigger_s,
             dir_i       => FL_dir_s,
             speed_o     => FL_speed_s
    );

    BR_speed : speed_compute
    port map(clock_i     => clock_100_MHz_s,
             reset_i     => reset_s,
             timer_top_i => top_speed_s,
             init_i      => reset_BR_speed_s,
             init_val_i  => std_logic_vector(to_unsigned(0, 32)),
             trigger_i   => BR_trigger_s,
             dir_i       => BR_dir_s,
             speed_o     => BR_speed_s
    );

    BL_speed : speed_compute
    port map(clock_i     => clock_100_MHz_s,
             reset_i     => reset_s,
             timer_top_i => top_speed_s,
             init_i      => reset_BL_speed_s,
             init_val_i  => std_logic_vector(to_unsigned(0, 32)),
             trigger_i   => BL_trigger_s,
             dir_i       => BL_dir_s,
             speed_o     => BL_speed_s
    );

    FR_mem_position : data_backup
    port map(clock_i    => clock_100_MHz_s,
             reset_i    => reset_s,
             init_i     => reset_FR_pos_s,
             memorize_i => memorize_FR_position_s,
             init_val_i => std_logic_vector(to_unsigned(0, 32)),
             data_i     => FR_position_s,
             mem_data_o => FR_mem_position_s
    );

    FL_mem_position : data_backup
    port map(clock_i    => clock_100_MHz_s,
             reset_i    => reset_s,
             init_i     => reset_FL_pos_s,
             memorize_i => memorize_FL_position_s,
             init_val_i => std_logic_vector(to_unsigned(0, 32)),
             data_i     => FL_position_s,
             mem_data_o => FL_mem_position_s
    );

    BR_mem_position : data_backup
    port map(clock_i    => clock_100_MHz_s,
             reset_i    => reset_s,
             init_i     => reset_BR_pos_s,
             memorize_i => memorize_BR_position_s,
             init_val_i => std_logic_vector(to_unsigned(0, 32)),
             data_i     => BR_position_s,
             mem_data_o => BR_mem_position_s
    );

    BL_mem_position : data_backup
    port map(clock_i    => clock_100_MHz_s,
             reset_i    => reset_s,
             init_i     => reset_BL_pos_s,
             memorize_i => memorize_BL_position_s,
             init_val_i => std_logic_vector(to_unsigned(0, 32)),
             data_i     => BL_position_s,
             mem_data_o => BL_mem_position_s
    );

    FR_mem_speed : data_backup
    port map(clock_i    => clock_100_MHz_s,
             reset_i    => reset_s,
             init_i     => reset_FR_speed_s,
             memorize_i => memorize_FR_speed_s,
             init_val_i => std_logic_vector(to_unsigned(0, 32)),
             data_i     => FR_speed_s,
             mem_data_o => FR_mem_speed_s
    );

    FL_mem_speed : data_backup
    port map(clock_i    => clock_100_MHz_s,
             reset_i    => reset_s,
             init_i     => reset_FL_speed_s,
             memorize_i => memorize_FL_speed_s,
             init_val_i => std_logic_vector(to_unsigned(0, 32)),
             data_i     => FL_speed_s,
             mem_data_o => FL_mem_speed_s
    );

    BR_mem_speed : data_backup
    port map(clock_i    => clock_100_MHz_s,
             reset_i    => reset_s,
             init_i     => reset_BR_speed_s,
             memorize_i => memorize_BR_speed_s,
             init_val_i => std_logic_vector(to_unsigned(0, 32)),
             data_i     => BR_speed_s,
             mem_data_o => BR_mem_speed_s
    );

    BL_mem_speed : data_backup
    port map(clock_i    => clock_100_MHz_s,
             reset_i    => reset_s,
             init_i     => reset_BL_speed_s,
             memorize_i => memorize_BL_speed_s,
             init_val_i => std_logic_vector(to_unsigned(0, 32)),
             data_i     => BL_speed_s,
             mem_data_o => BL_mem_speed_s
    );

    reset_FR_pos_s         <= command_s(0);
    reset_FL_pos_s         <= command_s(1);
    reset_BR_pos_s         <= command_s(2);
    reset_BL_pos_s         <= command_s(3);
    reset_FR_speed_s       <= command_s(4);
    reset_FL_speed_s       <= command_s(5);
    reset_BR_speed_s       <= command_s(6);
    reset_BL_speed_s       <= command_s(7);
    memorize_FR_position_s <= command_s(8);
    memorize_FL_position_s <= command_s(9);
    memorize_BR_position_s <= command_s(10);
    memorize_BL_position_s <= command_s(11);
    memorize_FR_speed_s    <= command_s(12);
    memorize_FL_speed_s    <= command_s(13);
    memorize_BR_speed_s    <= command_s(14);
    memorize_BL_speed_s    <= command_s(15);

    status_s <= x"0000000" & BL_position_error_s & BR_position_error_s & FL_position_error_s & FR_position_error_s;

end struct;