-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : i2c_slave_tb.vhd
-- Description  : i2c slave test bench
--
-- Author       : Sebastien Masle
-- Date         : 11.03.2021
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       SMS    11.03.2021         Creation
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library work;
    use work.function_pkg.all;
    use work.i2c_slave_pkg.all;
    use work.i2c_slave_tb_pkg.all;

entity i2c_slave_tb is
-- Test bench does not have any IO
end entity i2c_slave_tb;

architecture tb of i2c_slave_tb is

    component i2c_slave
        generic(extended_address_g   : boolean; -- true if 10 bit address is used, false if 7 bits address is used
                register_size_g      : natural; -- register size in bytes: must be 1, 2 or 4.
                i2c_slave_address_g  : std_logic_vector((bool_to_std_logic(extended_address_g) * 3) + 6 downto 0)
                );
        port(clock_i : in    std_logic; --system clock
             reset_i : in    std_logic; --asynchronous reset
             -- application inputs and outputs
             test_out_o : out std_logic_vector((register_size_g * 8) - 1 downto 0);
             -- i2c clock and data
             scl_i   : in    std_logic;
             sda_io  : inout std_logic);
    end component;

    -- A definir par l'utilisateur --------------------------------------------------------
    --Constantes
    constant PERIODE_CLK_SYS  : time := 100 ns;
    constant PERIODE_I2C_SCLK : time := 1000 ns;
    constant UPDATE_TIME       : time := 0 ns; --Permet la mise à jour des signaux

    type test_array_t is array (1 to NUMBER_OF_TESTS) of std_logic_vector(31 downto 0);

    type test_case_struct_t is record
        slv_addr       : std_logic_vector(9 downto 0);  -- address of the slave i2c device to access
        slv_addr_exist : boolean;                       -- indicates if a slave i2c device at slv_addr is connected to the test bench
        rd_nWr         : std_logic;                     -- indicates if it is a read or a write access: 0 - write / 1 - read
        reg_addr       : std_logic_vector(7 downto 0);  -- address of the register to access
        reg_addr_exist : boolean;                       -- indicates if the register at reg_addr exists
        reg_data       : std_logic_vector(31 downto 0); -- data that is supposed to be read in case of read / data to write in case of write
    end record test_case_struct_t;

    type test_case_t is array(natural range<>) of test_case_struct_t;
    type test_case_array_t is array(1 to NUMBER_OF_TESTS) of test_case_t;

    constant TEST_CASE : test_case_array_t := ( -- 1st test
                                               (("0000000001", false, '0', x"00", true, x"00000000"), -- no device at address "0000000001"
                                                (I2C_SLAVE_ADDRESS_ARRAY(1), true, '1', x"00", true, x"000000C5"),
                                                (I2C_SLAVE_ADDRESS_ARRAY(1), true, '0', x"01", true, x"0000006D"),
                                                (I2C_SLAVE_ADDRESS_ARRAY(1), true, '1', x"01", true, x"0000006D")),
                                                -- 2nd test
                                               ((I2C_SLAVE_ADDRESS_ARRAY(2), true, '0', x"00", true, x"1234ABCD"), -- this register is read only, the data written should have no effect
                                                (I2C_SLAVE_ADDRESS_ARRAY(2), true, '1', x"00", true, x"000000C5"),
                                                (I2C_SLAVE_ADDRESS_ARRAY(2), true, '0', x"01", true, x"BADDCAFE"),
                                                (I2C_SLAVE_ADDRESS_ARRAY(2), true, '1', x"01", true, x"BADDCAFE")),
                                                -- 3rd test
                                               ((I2C_SLAVE_ADDRESS_ARRAY(3), true, '0', x"02", false, x"0000003F"), -- this register address does not exist
                                                (I2C_SLAVE_ADDRESS_ARRAY(3), true, '1', x"00", true, x"000000C5"),
                                                (I2C_SLAVE_ADDRESS_ARRAY(3), true, '0', x"01", true, x"000000A2"),
                                                (I2C_SLAVE_ADDRESS_ARRAY(3), true, '1', x"01", true, x"000000A2")),
                                                -- 4th test
                                               (("1100000001", false, '0', x"00", true, x"00000000"), -- no device at address "1100000001"
                                                (I2C_SLAVE_ADDRESS_ARRAY(4), true, '1', x"00", true, x"000000C5"),
                                                (I2C_SLAVE_ADDRESS_ARRAY(4), true, '0', x"01", true, x"0000F00D"),
                                                (I2C_SLAVE_ADDRESS_ARRAY(4), true, '1', x"01", true, x"0000F00D"))
                                               );

    signal sim_end_s : boolean := false;

    -- signaux internes
    signal clk_sti     : std_logic;
    signal reset_sti   : std_logic;

    signal test_s      : test_array_t;
    signal data_read_s : std_logic_vector(31 downto 0) := (others => '0');

    -- Open Collector
    signal en_OC_s    : std_Logic;
    signal sda_in_s   : std_Logic;
    signal sda_out_s  : std_Logic;

    signal i2c_sda_s  : std_logic;
    signal i2c_scl_s  : std_logic;

begin

    -- Open Collector
    sda_in_s  <= to_X01(i2c_sda_s);  -- transformer état 'H' en '1' pour la simulation
    i2c_sda_s <= '0' when (en_OC_s = '1' and sda_out_s = '0') else
                 'Z';
    
    --Model of pull-up
    i2c_sda_s  <= 'H';

    ------------------------------------------------------------------
    --clock generation
    ------------------------------------------------------------------
    process
    begin
        while not sim_end_s loop
            clk_sti <= '1', '0' after PERIODE_CLK_SYS/2;
            wait for PERIODE_CLK_SYS;
        end loop;
        wait;
    end process;

    i2c_slave_gen : for I in 1 to NUMBER_OF_TESTS generate
        for all : i2c_slave use entity work.i2c_slave(struct);
    begin
        uut : i2c_slave
            generic map(
                extended_address_g   => EXTENDED_ADDRESS_ARRAY(I),
                register_size_g      => REGISTER_SIZE_ARRAY(I),
                i2c_slave_address_g  => I2C_SLAVE_ADDRESS_ARRAY(I)((bool_to_std_logic(EXTENDED_ADDRESS_ARRAY(I)) * 3) + 6 downto 0)
            )
            port map(
                clock_i => clk_sti,
                reset_i => reset_sti,
                -- application inputs and outputs
                test_out_o => test_s(I)((REGISTER_SIZE_ARRAY(I) * 8) - 1 downto 0),
                -- i2c clock and data
                scl_i   => i2c_scl_s,
                sda_io  => i2c_sda_s);
    end generate;

    process
        variable error_counter_v : integer := 0;

        procedure cycle(nb_cycle : integer := 1) is
        begin
            for i in 1 to nb_cycle loop
                wait until falling_edge(clk_sti);
                wait for 2 ns;
            end loop;
        end cycle;
        
        procedure reset_seq is
        begin
            reset_sti <= '1';
            i2c_scl_s <= 'H';
            en_OC_s   <= '0';
            sda_out_s <= '1';
            cycle(2);
            reset_sti <= '0';
            cycle;
        end reset_seq;
    begin

        -- Reset system at the beginning
        reset_seq; -- reset procedure

        cycle(2);

        for I in TEST_CASE'range loop
            for J in TEST_CASE(I)'range loop

                if TEST_CASE(I)(J).rd_nWr = '0' then
                    report "-------------------------------------------------------------"  & LF &
                           "         Starting test " & integer'image(1+((I-1)*TEST_CASE(I)'length)+J)  & LF &
                           "         slave address: " & vector_to_string(TEST_CASE(I)(J).slv_addr((bool_to_std_logic(EXTENDED_ADDRESS) * 3) + 6 downto 0)) & LF &
                           "         slave register address to write: " & vector_to_string(TEST_CASE(I)(J).reg_addr) & LF &
                           "         data to write: " & vector_to_string(TEST_CASE(I)(J).reg_data((REGISTER_SIZE_ARRAY(I) * 8) - 1 downto 0)) & LF &
                           "         -------------------------------------------------------------" severity NOTE;
                else
                    report "-------------------------------------------------------------"  & LF &
                           "         Starting test " & integer'image(1+((I-1)*TEST_CASE(I)'length)+J)  & LF &
                           "         slave address: " & vector_to_string(TEST_CASE(I)(J).slv_addr((bool_to_std_logic(EXTENDED_ADDRESS) * 3) + 6 downto 0)) & LF &
                           "         slave register address to read: " & vector_to_string(TEST_CASE(I)(J).reg_addr) & LF &
                           "         data to read: " & vector_to_string(TEST_CASE(I)(J).reg_data((REGISTER_SIZE_ARRAY(I) * 8) - 1 downto 0)) & LF &
                           "         -------------------------------------------------------------" severity NOTE;
                end if;

                -- start condition
                en_OC_s   <= '1';
                sda_out_s <= '0';

                cycle(5);

                i2c_scl_s <= '0';

                -- send slave address
                if EXTENDED_ADDRESS_ARRAY(I) = true then
                    for K in I2C_10BITS_ADDR'range loop
                        cycle(2);
                        sda_out_s <= I2C_10BITS_ADDR(K);
                        cycle(3);
                        i2c_scl_s <= '1';
                        cycle(5);
                        i2c_scl_s <= '0';
                    end loop;
                    for K in  TEST_CASE(I)(J).slv_addr'high downto TEST_CASE(I)(J).slv_addr'high - 1 loop
                        cycle(2);
                        sda_out_s <= TEST_CASE(I)(J).slv_addr(K);
                        cycle(3);
                        i2c_scl_s <= '1';
                        cycle(5);
                        i2c_scl_s <= '0';
                    end loop;
                else
                    for K in 6 downto 0 loop
                        cycle(2);
                        sda_out_s <= TEST_CASE(I)(J).slv_addr(K);
                        cycle(3);
                        i2c_scl_s <= '1';
                        cycle(5);
                        i2c_scl_s <= '0';
                    end loop;
                end if;

                -- set access direction
                cycle(2);
                sda_out_s <= '0'; -- no matter if TEST_CASE(I)(J).rd_nWr is a read or write, the first transfer is always a write, either to write something or to set the register to read.
                cycle(3);
                i2c_scl_s <= '1';
                cycle(5);
                i2c_scl_s <= '0';

                if EXTENDED_ADDRESS_ARRAY(I) = true then
                    -- check that at least one slave has responded to 10 bits address MSB
                    -- free sda line for slave ack
                    en_OC_s <= '0';
                    cycle(5);
                    i2c_scl_s <= '1';
                    if sda_in_s = '1' then -- NACK slave address
                        if TEST_CASE(I)(J).slv_addr_exist = false then
                            report ">> I2C Master: no slave at address " & vector_to_string(TEST_CASE(I)(J).slv_addr) & " (MSB only) on bus, this was expected" severity NOTE;
                        else
                            report ">> I2C Master: no slave at address " & vector_to_string(TEST_CASE(I)(J).slv_addr) & " (MSB only) on bus, this is not correct" severity ERROR;
                            error_counter_v := error_counter_v + 1;
                        end if;
                        cycle(5);
                        i2c_scl_s <= '0';
                    else -- ACK slave address
                        if TEST_CASE(I)(J).slv_addr_exist = false then
                            report ">> I2C Master: slave present at address " & vector_to_string(TEST_CASE(I)(J).slv_addr) & " (MSB only) on bus, this is not correct" severity ERROR;
                            error_counter_v := error_counter_v + 1;
                            cycle(5);
                            i2c_scl_s <= '0';
                        else
                            report ">> I2C Master: slave present at address " & vector_to_string(TEST_CASE(I)(J).slv_addr) & " (MSB only) on bus, this was expected" severity NOTE;
                            cycle(5);
                            i2c_scl_s <= '0';
                            en_OC_s   <= '1';
                            sda_out_s <= '1'; -- do not drive SDA
                        end if;
                    end if;
                    -- send slave address LSB when 10 bits addressing
                    for K in  7 downto 0 loop
                        cycle(2);
                        sda_out_s <= TEST_CASE(I)(J).slv_addr(K);
                        cycle(3);
                        i2c_scl_s <= '1';
                        cycle(5);
                        i2c_scl_s <= '0';
                    end loop;
                end if;

                -- free sda line for slave ack
                en_OC_s <= '0';
                cycle(5);
                i2c_scl_s <= '1';
                if sda_in_s = '1' then -- NACK slave address
                    if TEST_CASE(I)(J).slv_addr_exist = false then
                        report ">> I2C Master: no slave at address " & vector_to_string(TEST_CASE(I)(J).slv_addr) & " on bus, this was expected" severity NOTE;
                    else
                        report ">> I2C Master: no slave at address " & vector_to_string(TEST_CASE(I)(J).slv_addr) & " on bus, this is not correct" severity ERROR;
                        error_counter_v := error_counter_v + 1;
                    end if;
                    cycle(5);
                    i2c_scl_s <= '0';
                else -- ACK slave address
                    if TEST_CASE(I)(J).slv_addr_exist = false then
                        report ">> I2C Master: slave present at address " & vector_to_string(TEST_CASE(I)(J).slv_addr) & " on bus, this is not correct" severity ERROR;
                        error_counter_v := error_counter_v + 1;
                        cycle(5);
                        i2c_scl_s <= '0';
                    else
                        report ">> I2C Master: slave present at address " & vector_to_string(TEST_CASE(I)(J).slv_addr) & " on bus, this was expected" severity NOTE;
                        cycle(5);
                        i2c_scl_s <= '0';
                        en_OC_s   <= '1';
                        sda_out_s <= '1'; -- do not drive SDA
                        -- send register address
                        for K in TEST_CASE(I)(J).reg_addr'range loop
                            cycle(2);
                            sda_out_s <= TEST_CASE(I)(J).reg_addr(K);
                            cycle(3);
                            i2c_scl_s <= '1';
                            cycle(5);
                            i2c_scl_s <= '0';
                        end loop;
                        -- free sda line for slave ack
                        en_OC_s <= '0';
                        cycle(5);
                        i2c_scl_s <= '1';
                        -- check and and verify that it corresponds with the test case
                        if sda_in_s = '1' then -- NACK register address
                            if TEST_CASE(I)(J).reg_addr_exist = false then
                                report ">> I2C Master: no register at address " & vector_to_string(TEST_CASE(I)(J).reg_addr) & " on bus, this was expected" severity NOTE;
                            else
                                report ">> I2C Master: no register at address " & vector_to_string(TEST_CASE(I)(J).reg_addr) & " on bus, this is not correct" severity ERROR;
                                error_counter_v := error_counter_v + 1;
                            end if;
                            cycle(5);
                            i2c_scl_s <= '0';
                        else  -- ACK register address
                            if TEST_CASE(I)(J).reg_addr_exist = false then
                                report ">> I2C Master: register present at address " & vector_to_string(TEST_CASE(I)(J).reg_addr) & " on bus, this is not correct" severity ERROR;
                                error_counter_v := error_counter_v + 1;
                                cycle(5);
                                i2c_scl_s <= '0';
                            else
                                report ">> I2C Master: register present at address " & vector_to_string(TEST_CASE(I)(J).reg_addr) & " on bus, this was expected" severity NOTE;
                                cycle(5);
                                i2c_scl_s <= '0';
                                if TEST_CASE(I)(J).rd_nWr = '0' then
                                    -- write access
                                    en_OC_s   <= '1';
                                    sda_out_s <= '1'; -- do not drive SDA
                                    for K in REGISTER_SIZE_ARRAY(I) - 1 downto 0 loop
                                        for L in 7 downto 0 loop -- can only send one byte at a time
                                            cycle(2);
                                            sda_out_s <= TEST_CASE(I)(J).reg_data(K*8 + L);
                                            cycle(3);
                                            i2c_scl_s <= '1';
                                            cycle(5);
                                            i2c_scl_s <= '0';
                                        end loop;
                                        -- free sda line for slave ack
                                        en_OC_s <= '0';
                                        cycle(5);
                                        i2c_scl_s <= '1';
                                        if sda_in_s = '1' then -- NACK 
                                            if K = 0 then -- last byte send, slave should send NACK
                                                report ">> I2C Master: last data byte send, slave sent a NACK, this is expected behavior" severity NOTE;
                                            else
                                                report ">> I2C Master: received a NACK while not all bytes have been sent, this is not correct" severity ERROR;
                                                error_counter_v := error_counter_v + 1;
                                                cycle(5);
                                                i2c_scl_s <= '0';
                                                exit;
                                            end if;
                                        else
                                            if K = 0 then -- last byte send, slave should send NACK
                                                report ">> I2C Master: last data byte send, slave sent a ACK, this is not correct" severity ERROR;
                                                error_counter_v := error_counter_v + 1;
                                            else
                                                report ">> I2C Master: received a ACK and there is still bytes to send, this is expected behavior" severity NOTE;
                                            end if;
                                        end if;
                                        cycle(5);
                                        i2c_scl_s <= '0';
                                        cycle(2);
                                        en_OC_s   <= '1';
                                    end loop;
                                else
                                    -- read access
                                    -- send repeted start
                                    cycle(5);
                                    i2c_scl_s <= '1';
                                    cycle(2);
                                    en_OC_s   <= '1';
                                    sda_out_s <= '0';
                                    cycle(3);
                                    i2c_scl_s <= '0';
                                    -- send slave address again (only MSB when 10 bits addressing)
                                    if EXTENDED_ADDRESS_ARRAY(I) = true then
                                        for K in I2C_10BITS_ADDR'range loop
                                            cycle(2);
                                            sda_out_s <= I2C_10BITS_ADDR(K);
                                            cycle(3);
                                            i2c_scl_s <= '1';
                                            cycle(5);
                                            i2c_scl_s <= '0';
                                        end loop;
                                        for K in  TEST_CASE(I)(J).slv_addr'high downto TEST_CASE(I)(J).slv_addr'high - 1 loop
                                            cycle(2);
                                            sda_out_s <= TEST_CASE(I)(J).slv_addr(K);
                                            cycle(3);
                                            i2c_scl_s <= '1';
                                            cycle(5);
                                            i2c_scl_s <= '0';
                                        end loop;
                                    else
                                        for K in 6 downto 0 loop
                                            cycle(2);
                                            sda_out_s <= TEST_CASE(I)(J).slv_addr(K);
                                            cycle(3);
                                            i2c_scl_s <= '1';
                                            cycle(5);
                                            i2c_scl_s <= '0';
                                        end loop;
                                    end if;
                        
                                    -- set access direction
                                    cycle(2);
                                    sda_out_s <= TEST_CASE(I)(J).rd_nWr;
                                    cycle(3);
                                    i2c_scl_s <= '1';
                                    cycle(5);
                                    i2c_scl_s <= '0';
                        
                                    -- free sda line for slave ack
                                    en_OC_s <= '0';
                                    cycle(5);
                                    i2c_scl_s <= '1';
                                    if sda_in_s = '1' then -- NACK slave address
                                        if TEST_CASE(I)(J).slv_addr_exist = false then
                                            report ">> I2C Master: no slave at address " & vector_to_string(TEST_CASE(I)(J).slv_addr) & " on bus, this was expected" severity NOTE;
                                        else
                                            report ">> I2C Master: no slave at address " & vector_to_string(TEST_CASE(I)(J).slv_addr) & " on bus, this is not correct" severity ERROR;
                                            error_counter_v := error_counter_v + 1;
                                        end if;
                                        cycle(5);
                                        i2c_scl_s <= '0';
                                    else -- ACK slave address
                                        if TEST_CASE(I)(J).slv_addr_exist = false then
                                            report ">> I2C Master: slave present at address " & vector_to_string(TEST_CASE(I)(J).slv_addr) & " on bus, this is not correct" severity ERROR;
                                            error_counter_v := error_counter_v + 1;
                                            cycle(5);
                                            i2c_scl_s <= '0';
                                        else
                                            report ">> I2C Master: slave present at address " & vector_to_string(TEST_CASE(I)(J).slv_addr) & " on bus, this was expected" severity NOTE;
                                            cycle(5);
                                            i2c_scl_s <= '0';
                                            -- read data from slave
                                            for K in REGISTER_SIZE_ARRAY(I) - 1 downto 0 loop
                                                en_OC_s <= '0';
                                                for L in 7 downto 0 loop -- can only receive one byte at a time
                                                    cycle(5);
                                                    i2c_scl_s <= '1';
                                                    data_read_s(K * 8 + L) <= sda_in_s;
                                                    cycle(5);
                                                    i2c_scl_s <= '0';
                                                end loop;
                                                if K = 0 then -- send NACK - cannot receive any more bytes
                                                    cycle(2);
                                                    en_OC_s   <= '1';
                                                    sda_out_s <= '1';
                                                    cycle(3);
                                                    i2c_scl_s <= '1';
                                                    cycle(5);
                                                    i2c_scl_s <= '0';
                                                else -- send ACK - can receive more bytes
                                                    cycle(2);
                                                    en_OC_s   <= '1';
                                                    sda_out_s <= '0';
                                                    cycle(3);
                                                    i2c_scl_s <= '1';
                                                    cycle(5);
                                                    i2c_scl_s <= '0';
                                                end if;
                                            end loop;
                                            if TEST_CASE(I)(J).reg_data((REGISTER_SIZE_ARRAY(I) * 8) - 1 downto 0) = data_read_s((REGISTER_SIZE_ARRAY(I) * 8) - 1 downto 0) then
                                                report ">> I2C Master: data read at register address " & vector_to_string(TEST_CASE(I)(J).reg_addr) & " is " & vector_to_string(data_read_s((REGISTER_SIZE_ARRAY(I) * 8) - 1 downto 0)) & ", this is correct" severity NOTE;
                                            else
                                                report ">> I2C Master: data read at register address " & vector_to_string(TEST_CASE(I)(J).reg_addr) & " is " & vector_to_string(data_read_s((REGISTER_SIZE_ARRAY(I) * 8) - 1 downto 0)) & ", this is not correct, expected data is " & vector_to_string(TEST_CASE(I)(J).reg_data((REGISTER_SIZE_ARRAY(I) * 8) - 1 downto 0)) severity ERROR;
                                                error_counter_v := error_counter_v + 1;
                                            end if;
                                        end if;
                                    end if;
                                end if;
                            end if;
                        end if;
                    end if;
                end if;
                cycle(2);
                en_OC_s   <= '1';
                sda_out_s <= '0';
                cycle(3);
                i2c_scl_s <= '1';

                -- stop condition
                cycle(2);
                sda_out_s <= '1';

                cycle(20);

            end loop;
        end loop;

        report "-------------------------------------------------------------" & LF &
            "         >>  Number of errors found with given test cases: " & integer'image(error_counter_v) & LF &
            "         -------------------------------------------------------------";
        report "end of simulation, check log for details";

        sim_end_s <= true;
        wait;

    end process;

end tb;