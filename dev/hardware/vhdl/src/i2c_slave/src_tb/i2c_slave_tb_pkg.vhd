-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : i2c_slave_tb_pkg.vhd
-- Description  : package with constants array for several testbench cases
--
-- Author       : Sebastien Masle
-- Date         : 18.03.2021
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       SMS    18.03.2021         Creation
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;

library ieee;
    use work.function_pkg.all;

package i2c_slave_tb_pkg is

    constant NUMBER_OF_TESTS : natural := 4;

    type extended_address_array_t is array(1 to NUMBER_OF_TESTS) of boolean;
    constant EXTENDED_ADDRESS_ARRAY : extended_address_array_t := (false,
                                                                   false,
                                                                   true,
                                                                   true);
    
    type register_size_t is array(1 to NUMBER_OF_TESTS) of natural;
    constant REGISTER_SIZE_ARRAY    : register_size_t := (1,  -- register size in bytes: must be a power of 2.
                                                          4,
                                                          1,
                                                          2);
    
    type i2c_slave_address_g is array(1 to NUMBER_OF_TESTS) of std_logic_vector(9 downto 0);
    constant I2C_SLAVE_ADDRESS_ARRAY : i2c_slave_address_g := ("0000100110",  -- all addresses are 10 bits, for 7 bits addressing tests, MSB will be truncated
                                                               "0000110001",
                                                               "0100100110",
                                                               "1010110001");

end i2c_slave_tb_pkg;