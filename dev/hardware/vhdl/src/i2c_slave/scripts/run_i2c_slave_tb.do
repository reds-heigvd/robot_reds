###############################################################################
## HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
## Institut REDS, Reconfigurable & Embedded Digital Systems
##
## File         : run_i2c_tb.do
## Description  : compiles necessary files and runs i2c_slave simulation
## 
## Author       : Sebastien Masle
## Date         : 01.02.2021
## Version      : 0.0
##
## Dependencies : 
## 
##| Modifications |------------------------------------------------------------
## Version   Author Date               Description
## 0.0       SMS    See header         Initial version
##
###############################################################################

#create library work        
vlib work
#map library work to work
vmap work work

# compile files
vcom -2008 -reportprogress 300 -work work   ../src/function_pkg.vhd
vcom -2008 -reportprogress 300 -work work   ../src/i2c_slave_pkg.vhd
vcom -2008 -reportprogress 300 -work work   ../src/i2c_register_bank.vhd
vcom -2008 -reportprogress 300 -work work   ../src/i2c_slave_fsm.vhd
vcom -2008 -reportprogress 300 -work work   ../src/i2c_slave.vhd

# tb compilation
vcom -2008 -reportprogress 300 -work work   ../src_tb/i2c_slave_tb_pkg.vhd
vcom -2008 -reportprogress 300 -work work   ../src_tb/i2c_slave_tb.vhd

# load simulation
vsim -voptargs="+acc" work.slave_i2c_tb 

# add tb signals to wave
add wave *
