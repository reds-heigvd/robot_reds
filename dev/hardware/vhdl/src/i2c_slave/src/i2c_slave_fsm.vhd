-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : i2c_slave_fsm.vhd
-- Description  : i2c slave control state machine
--
-- Author       : Sebastien Masle
-- Date         : 22.02.2021
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       SMS    22.02.2021         Creation
-- 1.0       SMS    10.06.2021         add burst support
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity i2c_slave_fsm is
    generic(
        extended_address_g : boolean := false); -- true if 10 bit address is used, false if 7 bits address is used
    port(
        clock_i               : in  std_logic; --system clock
        reset_i               : in  std_logic; --asynchronous reset
        scl_rising_edge_i     : in  std_logic;
        scl_falling_edge_i    : in  std_logic;
        start_detected_i      : in  std_logic;
        stop_detected_i       : in  std_logic;
        low_address_ok_i      : in  std_logic;
        high_address_ok_i     : in  std_logic;
        end_high_address_i    : in  std_logic;
        end_low_address_i     : in  std_logic;
        device_accessed_i     : in  std_logic;
        rd_nWr_i              : in  std_logic;
        data_sent_i           : in  std_logic;
        all_bytes_sent_i      : in  std_logic;
        load_device_addr_o    : out std_logic;
        shift_device_addr_o   : out std_logic;
        set_device_accessed_o : out std_logic;
        clr_device_accessed_o : out std_logic;
        save_access_dir_o     : out std_logic;
        data_reg_end_i        : in  std_logic;
        load_data_reg_o       : out std_logic;
        shift_data_reg_o      : out std_logic;
        reg_addr_ptr_set_i    : in  std_logic;
        addr_ptr_exists_i     : in  std_logic;
        write_finished_i      : in  std_logic;
        set_reg_addr_ptr_o    : out std_logic;
        inc_reg_addr_ptr_o    : out std_logic;
        clr_reg_addr_ptr_o    : out std_logic;
        read_access_o         : out std_logic;
        write_access_o        : out std_logic;
        clr_byte_counter_o    : out std_logic;
        dec_byte_counter_o    : out std_logic;
        sda_i                 : in  std_logic;
        sda_en_o              : out std_logic);
end i2c_slave_fsm;

architecture fsm of i2c_slave_fsm is
  
    --| Types declarations     |--------------------------------------------------------------   
    type I2C_STATE is(READY,
                      WAIT_SCL_RISE_1,
                      GET_HIGH_ADDRESS,
                      CHECK_HIGH_ADDRESS, -- and save direction access at the same time
                      SAVE_ACCESS_DIR_1,
                      WAIT_SCL_FALL_1,
                      ACK_HIGH_ADDRESS,
                      WAIT_SCL_RISE_2,
                      GET_LOW_ADDRESS,
                      CHECK_LOW_ADDRESS,
                      DEVICE_ACCESSED,
                      --   CLR_DEVICE_ACCESSED,
                      SAVE_ACCESS_DIR_2,
                      WAIT_SCL_FALL_2,
                      ACK_ADDRESS,
                      -- slave transmitter
                      LOAD_DATA_TO_READ,
                      SEND_DATA,
                      SHIFT_RD_DATA,
                      WAIT_SCL_RISE_3,
                      READ_ACK,
                      INC_REG_PTR_1,
                      DEC_BYTE_CNT_1,
                      WAIT_SCL_FALL_3,
                      CLEAR_POINTER,
                      -- slave receiver
                      LOAD_GET_DATA_REG,
                      WAIT_SCL_RISE_4,
                      GET_DATA,
                      LOAD_DEV_ADDR,
                      SAVE_POINTER,
                      WRITE_REGISTER,
                      WAIT_SCL_FALL_4,
                      ACK_WRITE,
                      INC_REG_PTR_2,
                      DEC_BYTE_CNT_2,
                      WAIT_SCL_FALL_5,
                      ACK_REG_WRITE,
                      NACK_WRITE);

    --| Constants declarations |--------------------------------------------------------------
  
    --| Signals declarations   |--------------------------------------------------------------
    signal current_state_s : I2C_STATE;
    signal next_state_s    : I2C_STATE;
    signal read_access_s   : std_logic;
    
begin

    process(all)
    begin
        --default state
        next_state_s <= current_state_s;
    
        case current_state_s is
            -- SMS
            -- idle current_state_s
            when READY               => if(start_detected_i = '1') then
                                            next_state_s <= WAIT_SCL_RISE_1;
                                        end if;
            when WAIT_SCL_RISE_1     => if scl_rising_edge_i = '1' then
                                            next_state_s <= GET_HIGH_ADDRESS;
                                        end if;
            when GET_HIGH_ADDRESS    => next_state_s <= CHECK_HIGH_ADDRESS;
            when CHECK_HIGH_ADDRESS  => if end_high_address_i = '1' then
                                            if high_address_ok_i = '1' then
                                                if extended_address_g = true then
                                                    if device_accessed_i = '1' then
                                                        -- next_state_s <= CLR_DEVICE_ACCESSED;
                                                        next_state_s <= SAVE_ACCESS_DIR_2;
                                                    else
                                                        -- next_state_s <= WAIT_SCL_FALL_1;
                                                        next_state_s <= SAVE_ACCESS_DIR_1;
                                                    end if;
                                                else
                                                    next_state_s <= SAVE_ACCESS_DIR_2;
                                                end if;
                                            else
                                                next_state_s <= READY;
                                            end if;
                                        else
                                            next_state_s <= WAIT_SCL_RISE_1;
                                        end if;
            when SAVE_ACCESS_DIR_1   => next_state_s <= WAIT_SCL_FALL_1;
            when WAIT_SCL_FALL_1     => if scl_falling_edge_i = '1' then
                                            next_state_s <= ACK_HIGH_ADDRESS;
                                        end if;
            when ACK_HIGH_ADDRESS    => if scl_falling_edge_i = '1' then
                                            next_state_s <= WAIT_SCL_RISE_2;
                                        end if;
            when WAIT_SCL_RISE_2     => if scl_rising_edge_i = '1' then
                                            next_state_s <= GET_LOW_ADDRESS;
                                        end if;
            when GET_LOW_ADDRESS     => next_state_s <= CHECK_LOW_ADDRESS;
            when CHECK_LOW_ADDRESS   => if end_low_address_i = '1' then
                                            if low_address_ok_i = '1' then
                                                -- next_state_s <= DEVICE_ACCESSED;
                                                next_state_s <= WAIT_SCL_FALL_2;
                                            else
                                                next_state_s <= READY;
                                            end if;
                                        else
                                            next_state_s <= WAIT_SCL_RISE_2;
                                        end if;
            -- when DEVICE_ACCESSED     => next_state_s <= WAIT_SCL_FALL_2;
            -- when CLR_DEVICE_ACCESSED => next_state_s <= WAIT_SCL_FALL_2;
            when SAVE_ACCESS_DIR_2   => next_state_s <= WAIT_SCL_FALL_2;
            when WAIT_SCL_FALL_2     => if scl_falling_edge_i = '1' then
                                            next_state_s <= ACK_ADDRESS;
                                        end if;
            when ACK_ADDRESS         => if scl_falling_edge_i = '1' then
                                            if rd_nWr_i = '1' then
                                                next_state_s <= LOAD_DATA_TO_READ;
                                            else
                                                next_state_s <= LOAD_GET_DATA_REG;
                                            end if;
                                        end if;
            -- read access
            when LOAD_DATA_TO_READ   =>  next_state_s <= SEND_DATA; -- also decrements byte counter
            when SEND_DATA           => if scl_falling_edge_i = '1' then
                                            if data_sent_i = '1' then
                                                next_state_s <= WAIT_SCL_RISE_3;
                                            else
                                                next_state_s <= SHIFT_RD_DATA;
                                            end if;
                                        end if;
            when SHIFT_RD_DATA       => next_state_s <= SEND_DATA;
            when WAIT_SCL_RISE_3     => if scl_rising_edge_i = '1' then
                                            next_state_s <= READ_ACK;
                                        end if;
            when READ_ACK            => if sda_i = '0' then
                                            if all_bytes_sent_i = '1' then
                                                next_state_s <= INC_REG_PTR_1;
                                            else
                                                -- next_state_s <= WAIT_SCL_FALL_3;
                                                next_state_s <= DEC_BYTE_CNT_1;
                                            end if;
                                        else
                                            next_state_s <= CLEAR_POINTER;
                                        end if;
            when INC_REG_PTR_1       => next_state_s <= WAIT_SCL_FALL_3;
            when DEC_BYTE_CNT_1      => next_state_s <= WAIT_SCL_FALL_3;
            when WAIT_SCL_FALL_3     => if scl_falling_edge_i = '1' then
                                            next_state_s <= LOAD_DATA_TO_READ;
                                        end if;
            when CLEAR_POINTER       => next_state_s <= READY;
            -- write access
            when LOAD_GET_DATA_REG   => next_state_s <= WAIT_SCL_RISE_4;
            when WAIT_SCL_RISE_4     => if (data_reg_end_i = '1') then
                                            if (reg_addr_ptr_set_i = '1') then
                                                next_state_s <= WRITE_REGISTER;
                                            else
                                                next_state_s <= SAVE_POINTER;
                                            end if;
                                        elsif(start_detected_i = '1') then
                                            next_state_s <= LOAD_DEV_ADDR;
                                        elsif (stop_detected_i = '1') then
                                            next_state_s <= CLEAR_POINTER;
                                        elsif scl_rising_edge_i = '1' then
                                            next_state_s <= GET_DATA;
                                        end if;
            when GET_DATA            => if(start_detected_i = '1') then
                                            next_state_s <= LOAD_DEV_ADDR;
                                        elsif (stop_detected_i = '1') then
                                            next_state_s <= CLEAR_POINTER;
                                        else
                                            next_state_s <= WAIT_SCL_RISE_4;
                                        end if;
            when LOAD_DEV_ADDR       => next_state_s <= WAIT_SCL_RISE_1;
            when WRITE_REGISTER      => next_state_s <= WAIT_SCL_FALL_4;
            when WAIT_SCL_FALL_4     => if scl_falling_edge_i = '1' then
                                            if (addr_ptr_exists_i = '1') then
                                                next_state_s <= ACK_WRITE;
                                            else
                                                next_state_s <= NACK_WRITE;
                                            end if;
                                        end if;
            when ACK_WRITE           => if scl_falling_edge_i = '1' then
                                            if write_finished_i = '1' then
                                                next_state_s <= INC_REG_PTR_2;
                                            else
                                                next_state_s <= DEC_BYTE_CNT_2;
                                            end if;
                                        end if;
            when INC_REG_PTR_2       => next_state_s <= LOAD_GET_DATA_REG;
            when DEC_BYTE_CNT_2      => next_state_s <= LOAD_GET_DATA_REG;
            when SAVE_POINTER        => next_state_s <= WAIT_SCL_FALL_5;
            when WAIT_SCL_FALL_5     => if scl_falling_edge_i = '1' then
                                            if addr_ptr_exists_i = '1' then
                                                next_state_s <= ACK_REG_WRITE;
                                            else
                                                next_state_s <= NACK_WRITE;
                                            end if;
                                        end if;
            when ACK_REG_WRITE       => if scl_falling_edge_i = '1' then
                                            next_state_s <= LOAD_GET_DATA_REG;
                                        end if;
            when NACK_WRITE          => if scl_falling_edge_i = '1' then
                                            next_state_s <= READY;
                                        end if;
            when others              => next_state_s <= READY;
        end case;
    end process;

    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            current_state_s <= READY;
        elsif rising_edge(clock_i) then
            current_state_s <= next_state_s;
        end if;
    end process;

    load_device_addr_o    <= '1' when current_state_s = READY or current_state_s = LOAD_DEV_ADDR else '0';
    shift_device_addr_o   <= '1' when current_state_s = GET_HIGH_ADDRESS or current_state_s = GET_LOW_ADDRESS else '0';
    -- set_device_accessed_o <= '1' when current_state_s = DEVICE_ACCESSED else '0';
    set_device_accessed_o <= '1' when current_state_s = SAVE_POINTER else '0';
    -- clr_device_accessed_o <= '1' when current_state_s = CLR_DEVICE_ACCESSED or current_state_s = NACK_WRITE else '0';
    clr_device_accessed_o <= '1' when current_state_s = SAVE_ACCESS_DIR_2 or current_state_s = NACK_WRITE or current_state_s = ACK_WRITE else '0'; -- also in ACK_WRITE, when we are in this branch of the FSM, device has already been accessed
    load_data_reg_o       <= '1' when current_state_s = LOAD_GET_DATA_REG or current_state_s = LOAD_DATA_TO_READ else '0';
    shift_data_reg_o      <= '1' when current_state_s = GET_DATA or current_state_s = SHIFT_RD_DATA else '0';
    set_reg_addr_ptr_o    <= '1' when current_state_s = SAVE_POINTER else '0';
    inc_reg_addr_ptr_o    <= '1' when current_state_s = INC_REG_PTR_1 or current_state_s = INC_REG_PTR_2 else '0';
    clr_reg_addr_ptr_o    <= '1' when current_state_s = CLEAR_POINTER or current_state_s = NACK_WRITE else '0';
    save_access_dir_o     <= '1' when current_state_s = SAVE_ACCESS_DIR_1 or current_state_s = SAVE_ACCESS_DIR_2 else '0';
    read_access_s         <= '1' when current_state_s = LOAD_DATA_TO_READ or current_state_s = SEND_DATA or current_state_s = SHIFT_RD_DATA else '0';
    read_access_o         <= read_access_s;
    write_access_o        <= '1' when current_state_s = WRITE_REGISTER else '0';
    clr_byte_counter_o    <= '1' when current_state_s = CLEAR_POINTER or current_state_s = NACK_WRITE or current_state_s = INC_REG_PTR_1 or current_state_s = INC_REG_PTR_2 else '0';
    dec_byte_counter_o    <= '1' when current_state_s = DEC_BYTE_CNT_1 or current_state_s = DEC_BYTE_CNT_2 else '0';
    sda_en_o              <= '1' when current_state_s = ACK_HIGH_ADDRESS or current_state_s = ACK_ADDRESS or current_state_s = ACK_REG_WRITE or current_state_s = ACK_WRITE or read_access_s = '1' else '0';
    
end fsm;
