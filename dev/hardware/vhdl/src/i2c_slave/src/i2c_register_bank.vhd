-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : i2c_register_bank.vhd
-- Description  : register associated to the i2c slave
--
-- Author       : Sebastien Masle
-- Date         : 05.03.2021
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       SMS    05.03.2021         Creation
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library work;
    use work.function_pkg.all;
    use work.i2c_slave_pkg.all;

entity i2c_register_bank is
    generic(register_size_g      : natural -- register size in bytes: must be 1, 2 or 4.
            );
    port(
        clock_i         : in  std_logic; --system clock
        reset_i         : in  std_logic; --asynchronous reset
        address_i       : in  std_logic_vector(7 downto 0);
        data_to_read_o  : out std_logic_vector(7 downto 0);
        data_to_write_i : in  std_logic_vector(7 downto 0);
        byte_index_i    : in  std_logic_vector(ilogup(register_size_g) - 1 downto 0);
        write_i         : in  std_logic;
        -- application registers
        command_o     : out std_logic_vector((register_size_g * 8) - 1 downto 0);
        status_i      : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
        FR_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
        FL_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
        BR_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
        BL_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
        FR_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
        FL_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
        BR_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
        BL_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
        interval_o    : out std_logic_vector((register_size_g * 8) - 1 downto 0)
        );
end i2c_register_bank;

architecture struct of i2c_register_bank is

    --| Types declarations     |--------------------------------------------------------------
    type register_bank_t is array(0 to REGISTER_BANK_ADDR'length - 1) of std_logic_vector((register_size_g * 8) - 1 downto 0);

    --| Constants declarations |--------------------------------------------------------------
    constant VERSION       : std_logic_vector((register_size_g * 8) - 1 downto 0) := x"00000001";
  
    --| Signals declarations   |--------------------------------------------------------------
    signal register_bank_s : register_bank_t;

begin

    -- read only registers
    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            register_bank_s(0)  <= (others => '0');
            register_bank_s(3)  <= (others => '0');
            register_bank_s(4)  <= (others => '0');
            register_bank_s(5)  <= (others => '0');
            register_bank_s(6)  <= (others => '0');
            register_bank_s(7)  <= (others => '0');
            register_bank_s(8)  <= (others => '0');
            register_bank_s(9)  <= (others => '0');
            register_bank_s(10) <= (others => '0');
            register_bank_s(11) <= (others => '0');
        elsif rising_edge(clock_i) then
            register_bank_s(0)  <= VERSION;
            register_bank_s(3)  <= status_i;
            register_bank_s(4)  <= FR_position_i;
            register_bank_s(5)  <= FL_position_i;
            register_bank_s(6)  <= BR_position_i;
            register_bank_s(7)  <= BL_position_i;
            register_bank_s(8)  <= FR_speed_i;
            register_bank_s(9)  <= FL_speed_i;
            register_bank_s(10) <= BR_speed_i;
            register_bank_s(11) <= BL_speed_i;
        end if;
    end process;

    resgister_write : for I in register_bank_s'range generate
        writable_reg : if REGISTER_BANK_ADDR(I).rw(0) = '1' generate -- writable register
            process(clock_i, reset_i)
            begin
                if reset_i = '1' then
                    if REGISTER_BANK_ADDR(I).rw(0) = '1' then
                        register_bank_s(I) <= (others => '0');
                    end if;
                elsif rising_edge(clock_i) then
                    -- automatically cleared registers, to have bits active only one clock cycle
                    if I = 2 then
                        register_bank_s(I)  <= (others => '0');
                    end if;
                    if address_i = REGISTER_BANK_ADDR(I).addr then
                        if write_i = '1' then
                            if register_size_g = 1 then
                                register_bank_s(I) <= data_to_write_i;
                            else
                                register_bank_s(I)(((to_integer(unsigned(byte_index_i)) + 1) * 8) - 1 downto (to_integer(unsigned(byte_index_i)) * 8)) <= data_to_write_i;
                            end if;
                        end if;
                    end if;
                end if;
            end process;
        end generate;
    end generate;

    process(all)
    begin
        data_to_read_o <= (others => '0');
        for I in register_bank_s'range loop
            if address_i = REGISTER_BANK_ADDR(I).addr and REGISTER_BANK_ADDR(I).rw(1) = '1' then
                if register_size_g = 1 then
                    data_to_read_o <= register_bank_s(I);
                else
                    data_to_read_o <= register_bank_s(I)(((to_integer(unsigned(byte_index_i)) + 1) * 8) - 1 downto (to_integer(unsigned(byte_index_i)) * 8));
                end if;
            end if;
        end loop;
    end process;

    -- outputs assignation
    command_o  <= register_bank_s(2);
    interval_o <= register_bank_s(12);
        
end struct;
