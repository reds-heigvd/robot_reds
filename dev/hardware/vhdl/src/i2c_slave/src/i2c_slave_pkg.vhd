-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : i2c_slave_pkg.vhd
-- Description  : package with constants and types for i2c_slave module
--
-- Author       : Sebastien Masle
-- Date         : 05.03.2021
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       SMS    05.03.2021         Creation
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;

library ieee;
    use work.function_pkg.all;

package i2c_slave_pkg is

    constant EXTENDED_ADDRESS  : boolean := false; -- true if 10 bit address is used, false if 7 bits address is used
    constant REGISTER_SIZE     : natural := 4; -- register size in bytes: must be 1, 2 or 4.

    constant I2C_10BITS_ADDR   : std_logic_vector(4 downto 0) := "11110"; -- i2c specifies "11110" as the first bit received when 10 bits addressing, then followed by the device address
    constant I2C_SLAVE_ADDRESS : std_logic_vector((bool_to_std_logic(EXTENDED_ADDRESS) * 3) + 6 downto 0) := "0001000";

    type register_t is record
        addr : std_logic_vector(7 downto 0); -- maximum of 256 registers
        rw   : std_logic_vector(1 downto 0); -- bit 1 : 1 --> read allowed / 0 --> read not allowed
                                             -- bit 0 : 1 --> write allowed / 0 --> write not allowed
    end record register_t;

    type register_bank_addr_t is array(natural range<>) of register_t;

    constant REGISTER_BANK_ADDR : register_bank_addr_t := (("00000000", "10"),
                                                           ("00000001", "11"),
                                                           ("00000010", "11"),
                                                           ("00000011", "10"),
                                                           ("00000100", "10"),
                                                           ("00000101", "10"),
                                                           ("00000110", "10"),
                                                           ("00000111", "10"),
                                                           ("00001000", "10"),
                                                           ("00001001", "10"),
                                                           ("00001010", "10"),
                                                           ("00001011", "10"),
                                                           ("00001100", "11"));

end i2c_slave_pkg;