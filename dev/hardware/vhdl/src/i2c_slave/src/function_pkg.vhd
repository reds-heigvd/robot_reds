-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : function_pkg.vhd
-- Description  : package with functions used in i2c IP
--
-- Author       : Sebastien Masle
-- Date         : 05.03.2021
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       SMS    05.03.2021         Creation
-------------------------------------------------------------------------------

library IEEE;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

package function_pkg is

  -- integer logarithm (rounded up) [MR version]
  function ilogup (x : natural; base : natural := 2) return natural;

  -- integer logarithm (rounded down) [MR version]
  function ilog (x : natural; base : natural := 2) return natural;

  function bool_to_std_logic (x : boolean) return natural;

  function vector_to_string (vector : std_logic_vector) return string;
  
end package function_pkg;

package body function_pkg is

    -- integer logarithm (rounded up) [MR version]
    function ilogup (x : natural; base : natural := 2) return natural is
        variable y : natural := 1;
    begin
        y:= 1;  --Mod EMI 26.03.2009
        while x > base ** y loop
            y := y + 1;
        end loop;
        return y;
    end ilogup;
  
  
    -- integer logarithm (rounded down) [MR version]
    function ilog (x : natural; base : natural := 2) return natural is
        variable y : natural := 1;
    begin
        y:= 1;  --Mod EMI 26.03.2009
        while x > base ** y loop
            y := y + 1;
        end loop;
        if x < base**y then
            y:=y-1;
        end if;
        return y;
    end ilog;

    function bool_to_std_logic(x : boolean) return natural is
    begin
        if x then
            return(1);
        else
            return(0);
        end if;
    end function bool_to_std_logic;

    function vector_to_string (vector : std_logic_vector) return string is
        variable s   : string (1 to vector'length) := (others => NUL);
        variable cnt : integer := 0;
    begin
        for i in vector'range loop
            cnt := cnt + 1;
            s(cnt) := std_logic'image(vector((i)))(2);
        end loop;
        return s;
    end function;
  
end package body function_pkg;
