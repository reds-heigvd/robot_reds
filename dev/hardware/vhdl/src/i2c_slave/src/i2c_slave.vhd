-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : i2c_slave.vhd
-- Description  : i2c slave module
--
-- Author       : Sebastien Masle
-- Date         : 04.03.2021
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       SMS    04.03.2021         Creation
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library work;
    use work.function_pkg.all;
    use work.i2c_slave_pkg.all;

entity i2c_slave is
    generic(extended_address_g   : boolean := false; -- useful for simulation
            register_size_g      : natural := 1; -- register size in bytes: must be 1, 2 or 4.
            i2c_slave_address_g  : std_logic_vector((bool_to_std_logic(EXTENDED_ADDRESS) * 3) + 6 downto 0) -- quartus does not accept extended_address_g
            );
    port(clock_i       : in    std_logic; --system clock
         reset_i       : in    std_logic; --asynchronous reset
         -- application inputs and outputs
         command_o     : out std_logic_vector((register_size_g * 8) - 1 downto 0);
         status_i      : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
         FR_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
         FL_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
         BR_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
         BL_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
         FR_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
         FL_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
         BR_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
         BL_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
         interval_o    : out std_logic_vector((register_size_g * 8) - 1 downto 0);
         -- i2c clock and data
         scl_i   : in    std_logic;
         sda_io  : inout std_logic);
end i2c_slave;

architecture struct of i2c_slave is

    component i2c_slave_fsm
        generic(
                extended_address_g : boolean := false); -- true if 10 bit address is used, false if 7 bits address is used
            port(
                clock_i               : in  std_logic; --system clock
                reset_i               : in  std_logic; --asynchronous reset
                scl_rising_edge_i     : in  std_logic;
                scl_falling_edge_i    : in  std_logic;
                start_detected_i      : in  std_logic;
                stop_detected_i       : in  std_logic;
                low_address_ok_i      : in  std_logic;
                high_address_ok_i     : in  std_logic;
                end_high_address_i    : in  std_logic;
                end_low_address_i     : in  std_logic;
                device_accessed_i     : in  std_logic;
                rd_nWr_i              : in  std_logic;
                data_sent_i           : in  std_logic;
                all_bytes_sent_i      : in  std_logic;
                load_device_addr_o    : out std_logic;
                shift_device_addr_o   : out std_logic;
                set_device_accessed_o : out std_logic;
                clr_device_accessed_o : out std_logic;
                save_access_dir_o     : out std_logic;
                data_reg_end_i        : in  std_logic;
                load_data_reg_o       : out std_logic;
                shift_data_reg_o      : out std_logic;
                reg_addr_ptr_set_i    : in  std_logic;
                addr_ptr_exists_i     : in  std_logic;
                write_finished_i      : in  std_logic;
                set_reg_addr_ptr_o    : out std_logic;
                inc_reg_addr_ptr_o    : out std_logic;
                clr_reg_addr_ptr_o    : out std_logic;
                read_access_o         : out std_logic;
                write_access_o        : out std_logic;
                clr_byte_counter_o    : out std_logic;
                dec_byte_counter_o    : out std_logic;
                sda_i                 : in  std_logic;
                sda_en_o              : out std_logic);
        end component;
        for all : i2c_slave_fsm use entity work.i2c_slave_fsm(fsm);

    component i2c_register_bank
        generic(register_size_g      : natural -- register size in bytes: must be 1, 2 or 4.
                );
        port(
            clock_i         : in  std_logic; --system clock
            reset_i         : in  std_logic; --asynchronous reset
            address_i       : in  std_logic_vector(7 downto 0);
            data_to_read_o  : out std_logic_vector(7 downto 0);
            data_to_write_i : in  std_logic_vector(7 downto 0);
            byte_index_i    : in  std_logic_vector(ilogup(register_size_g) - 1 downto 0);
            write_i         : in  std_logic;
            -- application registers
            command_o     : out std_logic_vector((register_size_g * 8) - 1 downto 0);
            status_i      : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
            FR_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
            FL_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
            BR_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
            BL_position_i : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
            FR_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
            FL_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
            BR_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
            BL_speed_i    : in  std_logic_vector((register_size_g * 8) - 1 downto 0);
            interval_o    : out std_logic_vector((register_size_g * 8) - 1 downto 0)
            );
    end component;
    for all : i2c_register_bank use entity work.i2c_register_bank(struct);

    --| Types declarations     |--------------------------------------------------------------

    --| Constants declarations |--------------------------------------------------------------
    constant DATA_SENT : std_logic_vector(7 downto 0) := (7 => '1', others => '0'); -- anticipate one clock cycle (signal used just before the last shift)
  
    --| Signals declarations   |--------------------------------------------------------------
    signal scl_rx_s              : std_logic;
    signal sda_rx_s              : std_logic;
    signal sda_tx_s              : std_logic;
    signal sda_en_s              : std_logic;
    signal prev_scl_s            : std_logic;
    signal prev_sda_s            : std_logic;
    signal scl_rising_edge_s     : std_logic;
    signal scl_falling_edge_s    : std_logic;
    signal sda_rising_edge_s     : std_logic;
    signal sda_falling_edge_s    : std_logic;
    signal start_detected_s      : std_logic;
    signal stop_detected_s       : std_logic;

    signal set_reg_addr_ptr_s    : std_logic;
    signal inc_reg_addr_ptr_s    : std_logic;
    signal clr_reg_addr_ptr_s    : std_logic;
    signal reg_addr_ptr_set_s    : std_logic;
    signal addr_ptr_exists_s     : std_logic;
    signal ptr_exists_array_s    : std_logic_vector(REGISTER_BANK_ADDR'range);
    signal pointer_addr_s        : std_logic_vector(7 downto 0);
    signal data_reg_end_s        : std_logic;
    signal write_finished_s      : std_logic;
    signal read_access_s         : std_logic;
    signal write_access_s        : std_logic;
    signal load_data_reg_s       : std_logic;
    signal shift_data_reg_s      : std_logic;
    signal data_sent_s           : std_logic;
    signal all_bytes_sent_s      : std_logic;
    signal bit_to_insert_s       : std_logic;
    signal data_srg_s            : std_logic_vector(8 downto 0);
    signal clr_byte_counter_s    : std_logic;
    signal dec_byte_counter_s    : std_logic;
    signal byte_counter_s        : unsigned(ilogup(register_size_g) - 1 downto 0);
    signal data_to_load_s        : std_logic_vector(7 downto 0);
    signal data_to_read_s        : std_logic_vector(7 downto 0);

    signal address_s             : std_logic_vector(8 downto 0);
    signal extended_address_s    : std_logic_vector(16 downto 0);
    signal load_device_addr_s    : std_logic;
    signal shift_device_addr_s   : std_logic;
    signal end_high_address_s    : std_logic;
    signal end_low_address_s     : std_logic;
    signal high_address_ok_s     : std_logic;
    signal low_address_ok_s      : std_logic;
    signal save_access_dir_s     : std_logic;
    signal rd_nWr_s              : std_logic;

    signal device_accessed_s     : std_logic;
    signal clr_device_accessed_s : std_logic;
    signal set_device_accessed_s : std_logic;


begin

    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            sda_rx_s <= '1';
            scl_rx_s <= '1';
        elsif rising_edge(clock_i) then
            sda_rx_s <= To_X01(sda_io);
            scl_rx_s <= To_X01(scl_i);
        end if;
    end process;

    sda_io <= '0' when (sda_en_s = '1' and sda_tx_s = '0') else 'Z';
    sda_tx_s <= data_srg_s(data_srg_s'high) when read_access_s = '1' else '0'; -- write on sda when read access or acknowledge (always '0')

        UC : i2c_slave_fsm
        generic map(
            extended_address_g => extended_address_g) -- true if 10 bit address is used, false if 7 bits address is used
        port map(
            clock_i               => clock_i,
            reset_i               => reset_i,
            scl_rising_edge_i     => scl_rising_edge_s,
            scl_falling_edge_i    => scl_falling_edge_s,
            start_detected_i      => start_detected_s,
            stop_detected_i       => stop_detected_s,
            low_address_ok_i      => low_address_ok_s,
            high_address_ok_i     => high_address_ok_s,
            end_high_address_i    => end_high_address_s,
            end_low_address_i     => end_low_address_s,
            device_accessed_i     => device_accessed_s,
            rd_nWr_i              => rd_nWr_s,
            data_sent_i           => data_sent_s,
            all_bytes_sent_i      => all_bytes_sent_s,
            load_device_addr_o    => load_device_addr_s,
            shift_device_addr_o   => shift_device_addr_s,
            set_device_accessed_o => set_device_accessed_s,
            clr_device_accessed_o => clr_device_accessed_s,
            save_access_dir_o     => save_access_dir_s,
            data_reg_end_i        => data_reg_end_s,
            load_data_reg_o       => load_data_reg_s,
            shift_data_reg_o      => shift_data_reg_s,
            reg_addr_ptr_set_i    => reg_addr_ptr_set_s,
            addr_ptr_exists_i     => addr_ptr_exists_s,
            write_finished_i      => write_finished_s,
            set_reg_addr_ptr_o    => set_reg_addr_ptr_s,
            inc_reg_addr_ptr_o    => inc_reg_addr_ptr_s,
            clr_reg_addr_ptr_o    => clr_reg_addr_ptr_s,
            read_access_o         => read_access_s,
            write_access_o        => write_access_s,
            clr_byte_counter_o    => clr_byte_counter_s,
            dec_byte_counter_o    => dec_byte_counter_s,
            sda_i                 => sda_rx_s,
            sda_en_o              => sda_en_s
            );

    register_bank : i2c_register_bank
        generic map(
            register_size_g      => register_size_g)
        port map(
            clock_i         => clock_i,
            reset_i         => reset_i,
            address_i       => pointer_addr_s,
            data_to_read_o  => data_to_read_s,
            data_to_write_i => data_srg_s(data_srg_s'high - 1 downto 0),
            byte_index_i    => std_logic_vector(byte_counter_s),
            write_i         => write_access_s,
            -- application registers
            command_o       => command_o,
            status_i        => status_i,
            FR_position_i   => FR_position_i,
            FL_position_i   => FL_position_i,
            BR_position_i   => BR_position_i,
            BL_position_i   => BL_position_i,
            FR_speed_i      => FR_speed_i,
            FL_speed_i      => FL_speed_i,
            BR_speed_i      => BR_speed_i,
            BL_speed_i      => BL_speed_i,
            interval_o      => interval_o
        );

    -- scl edges detection + start and stop condition detector
    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            prev_scl_s <= '1';
            prev_sda_s <= '1';
        elsif rising_edge(clock_i) then
            prev_scl_s <= scl_rx_s;
            prev_sda_s <= sda_rx_s;
        end if;
    end process;

    scl_rising_edge_s  <= not prev_scl_s and scl_rx_s;
    scl_falling_edge_s <= prev_scl_s and not scl_rx_s;

    sda_rising_edge_s  <= not prev_sda_s and sda_rx_s;
    sda_falling_edge_s <= prev_sda_s and not sda_rx_s;

    start_detected_s   <= sda_falling_edge_s and scl_rx_s;
    stop_detected_s    <= sda_rising_edge_s and scl_rx_s;

    -- address shift register
    address_shift_regsiter : if extended_address_g = true generate
        process(clock_i, reset_i)
        begin
            if reset_i = '1' then
                extended_address_s <= (0 => '1', others => '0');
            elsif rising_edge(clock_i) then
                if load_device_addr_s = '1' then
                    extended_address_s <= (0 => '1', others => '0');
                elsif shift_device_addr_s = '1' then
                    extended_address_s <= extended_address_s(extended_address_s'high - 1 downto 0) & sda_rx_s;
                end if;
            end if;
        end process;

        end_high_address_s <= extended_address_s(8);
        end_low_address_s  <= extended_address_s(extended_address_s'high);

        high_address_ok_s <= '1' when extended_address_s(7 downto 1) = (I2C_10BITS_ADDR & i2c_slave_address_g(i2c_slave_address_g'high downto i2c_slave_address_g'high - 1)) else '0';
        low_address_ok_s  <= '1' when extended_address_s(7 downto 0) = i2c_slave_address_g(i2c_slave_address_g'high - 2 downto 0) else '0';
    else generate
        process(clock_i, reset_i)
        begin
            if reset_i = '1' then
                address_s <= (0 => '1', others => '0');
            elsif rising_edge(clock_i) then
                if load_device_addr_s = '1' then
                    address_s <= (0 => '1', others => '0');
                elsif shift_device_addr_s = '1' then
                    address_s <= address_s(address_s'high - 1 downto 0) & sda_rx_s;
                end if;
            end if;
        end process;

        end_high_address_s <= address_s(address_s'high);
        end_low_address_s  <= '0';

        high_address_ok_s <= '1' when address_s(7 downto 1) = i2c_slave_address_g else '0';
        low_address_ok_s  <= '0';
    end generate;

    -- memorize access direction
    mem_access_dir : if extended_address_g = true generate
        process(clock_i, reset_i)
        begin
            if reset_i = '1' then
                rd_nWr_s <= '0';
            elsif rising_edge(clock_i) then
                if save_access_dir_s = '1' then
                    rd_nWr_s <= extended_address_s(0);
                end if;
            end if;
        end process;
    else generate
        process(clock_i, reset_i)
        begin
            if reset_i = '1' then
                rd_nWr_s <= '0';
            elsif rising_edge(clock_i) then
                if save_access_dir_s = '1' then
                    rd_nWr_s <= address_s(0);
                end if;
            end if;
        end process;
    end generate;

    -- set and clear device_accessed_s
    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            device_accessed_s <= '0';
        elsif rising_edge(clock_i) then
            if clr_device_accessed_s = '1' then
                device_accessed_s <= '0';
            elsif set_device_accessed_s = '1' then
                device_accessed_s <= '1';
            end if;
        end if;
    end process;

    -- memorize when pointer has been set
    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            reg_addr_ptr_set_s <= '0';
        elsif rising_edge(clock_i) then
            if clr_reg_addr_ptr_s = '1' then
                reg_addr_ptr_set_s <= '0';
            elsif set_reg_addr_ptr_s = '1' then
                reg_addr_ptr_set_s <= '1';
            end if;
        end if;
    end process;

    -- memorize pointer address
    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            pointer_addr_s <= (others => '0');
        elsif rising_edge(clock_i) then
            if clr_reg_addr_ptr_s = '1' then
                pointer_addr_s <= (others => '0');
            elsif set_reg_addr_ptr_s = '1' then
                pointer_addr_s <= data_srg_s(pointer_addr_s'range);
            elsif inc_reg_addr_ptr_s = '1' then
                pointer_addr_s <= std_logic_vector(unsigned(pointer_addr_s) + 1);
            end if;
        end if;
    end process;

    -- check if register address exists
    reg_addr_check : for I in ptr_exists_array_s'range generate
        ptr_exists_array_s(I) <= '1' when REGISTER_BANK_ADDR(I).addr = pointer_addr_s else '0';
    end generate;

    addr_ptr_exists_s <= '0' when unsigned(ptr_exists_array_s) = 0 else '1';

    -- byte counter
    byte_counter : if register_size_g /= 1 generate
        process(clock_i, reset_i)
        begin
            if reset_i = '1' then
                byte_counter_s <= (others => '1');
            elsif rising_edge(clock_i) then
                if clr_byte_counter_s = '1' then
                    byte_counter_s <= (others => '1');
                elsif dec_byte_counter_s = '1' then
                    byte_counter_s <= byte_counter_s - 1;
                end if;
            end if;
        end process;

        write_finished_s <= '1' when byte_counter_s = 0 else '0';
        all_bytes_sent_s <= '1' when byte_counter_s = 0 else '0';
    else generate
        write_finished_s <= '1';
        all_bytes_sent_s <= '1';
    end generate;

    -- data shift register for read and write access
    process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            data_srg_s <= (others => '0');
        elsif rising_edge(clock_i) then
            if load_data_reg_s = '1' then
                data_srg_s <= data_to_load_s & '1';
            elsif shift_data_reg_s = '1' then
                data_srg_s <= data_srg_s(data_srg_s'high - 1 downto 0) & bit_to_insert_s;
            end if;
        end if;
    end process;

    data_to_load_s  <= data_to_read_s when rd_nWr_s = '1' else
                       (others => '0');
    bit_to_insert_s <= '0' when rd_nWr_s = '1' else
                       sda_rx_s;
    data_reg_end_s <= data_srg_s(data_srg_s'high);
    data_sent_s    <= '1' when data_srg_s(DATA_SENT'range) = DATA_SENT else '0'; -- anticipate one clock cycle (signal used just before the last shift)

    
end struct;
