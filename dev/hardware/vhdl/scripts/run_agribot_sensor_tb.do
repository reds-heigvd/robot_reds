###############################################################################
## HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
## Institut REDS, Reconfigurable & Embedded Digital Systems
##
## File         : run_agribot_sensor_tb.do
## Description  : compiles necessary files and loads simulation
## 
## Author       : Sebastien Masle
## Date         : 26.03.2021
## Version      : 0.0
##
## Dependencies : 
## 
##| Modifications |------------------------------------------------------------
## Version   Author Date               Description
## 0.0       SMS    See header         Initial version
##
###############################################################################

#create library work        
vlib work
#map library work to work
vmap work work

#Intel simulation library
vlib altera_mf
vmap altera_mf altera_mf

# compile files
# Intel simulation files
vcom -2008 -reportprogress 300 -work altera_mf   ../sim_lib/altera_mf_components.vhd
vcom -2008 -reportprogress 300 -work altera_mf   ../sim_lib/altera_mf.vhd
# Intel IP
vcom -2008 -reportprogress 300 -work work   ../pr/ip/pll_100MHz.vhd
# i2c slave IP
vcom -2008 -reportprogress 300 -work work   ../src/i2c_slave/src/function_pkg.vhd
vcom -2008 -reportprogress 300 -work work   ../src/i2c_slave/src/i2c_slave_pkg.vhd
vcom -2008 -reportprogress 300 -work work   ../src/i2c_slave/src/i2c_register_bank.vhd
vcom -2008 -reportprogress 300 -work work   ../src/i2c_slave/src/i2c_slave_fsm.vhd
vcom -2008 -reportprogress 300 -work work   ../src/i2c_slave/src/i2c_slave.vhd
# incremental coder acquisition IP
vcom -2008 -reportprogress 300 -work work   ../src/acqu_pos/src/det_rot_ab.vhd
vcom -2008 -reportprogress 300 -work work   ../src/acqu_pos/src/ff_d.vhd
vcom -2008 -reportprogress 300 -work work   ../src/acqu_pos/src/cpt_UpDn_gen.vhd
vcom -2008 -reportprogress 300 -work work   ../src/acqu_pos/src/acqu_pos.vhd
# agribot sensor design
vcom -2008 -reportprogress 300 -work work   ../src/timer.vhd
vcom -2008 -reportprogress 300 -work work   ../src/speed_compute.vhd
vcom -2008 -reportprogress 300 -work work   ../src/data_backup.vhd
vcom -2008 -reportprogress 300 -work work   ../src/agribot_sensor_top.vhd

# tb compilation
vcom -2008 -reportprogress 300 -work work   ../src_tb/agribot_sensor_top_tb.vhd

# load simulation
vsim -voptargs="+acc" work.agribot_sensor_top_tb -t ps

# add tb signals to wave
add wave uut/*

run 700us