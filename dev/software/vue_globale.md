# Vue d'ensemble de robot_Reds



- admin

  Informations administrative des différents robots du REDS

  - agribot

    Informations relatives à agribot

    - meetings_agribot

      PV de tout les meetings ayant eu lieu pour Agribot

- dev

  Développement des robot du REDS

  - src

    Code source des robots

    - global_launcher

      Launchers principaux de chaque robot

    -	reds_pkg

      Packages modifiés ou développés par le REDS

    -	robot

      Code et fichiers scpécifique à chaque robot

      - agribot

        Code et fichiers spécifique à Agribot

        - description

          Informations pour la description d'Agribot (Gestion des frames de la base)

        - launch

          Fichier launch des différents modes d'Agribot

        - map

          Stockages des informations des cartes générées pour Agribot

        - ros_pkg

          Fichiers launch et de paramètres pour la configuration d'Agribot

        - src

          Code source et spécifique à Agribot

    -	ros_pkg

      Packages récupérés et non modifiés

  - devel

    Apparaît lorsque le projet est compilé

  - build

    Apparaît lorsque le projet est compilé
  
- doc

  Documentation de tiers 

  - agribot

    Documentation spécifique à Agribot

    - materiel

      Documentation des différents composants d'Agribot

- publi

  Documentation rédigée 

  - agribot

    Documentation pour agribot

    - demarrage_demo_agribot

      Explication pour démarrer les démos d'Agribot

    - mesures_vitesse

      Calibration des valeurs de PWM pour la vitesse réelle obtenue

    - structure_materiel_agribot

      Schéma du robot

    - sw_overview

      Informations générales sur le système et les différentes démos 

  - tutoriels

    Tutoriels généraux pour comprendre les bases de ROS et du système mis en place