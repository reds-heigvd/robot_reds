#!/usr/bin/python

import rospy
from termcolor import colored

from std_msgs.msg import Bool
from std_msgs.msg import Int16


class bcolors:
    HEADER    = '\033[95m'
    OKBLUE    = '\033[94m'
    OKCYAN    = '\033[96m'
    OKGREEN   = '\033[92m'
    WARNING   = '\033[93m'
    FAIL      = '\033[91m'
    ENDC      = '\033[0m'
    BOLD      = '\033[1m'
    UNDERLINE = '\033[4m'

class helpie_demo_app():
    def __init__(self):
        rospy.loginfo("Welcome to the Agribot demo!")
        rospy.init_node('agribot_demo_app')

        self.exploration_pub      = rospy.Publisher('agribot/explore_environment',      Bool,  queue_size=10)
        self.stop_exploration_pub = rospy.Publisher('agribot/stop_explore_environment', Bool,  queue_size=10)
        self.response_color = 'green'
        #navigation_pub       = rospy.Publisher('helpie/stop_explore_environment', Bool, queue_size=10)

    def run(self):
        user_input = "None"
        self.print_demo_usage()
        try:
            while not rospy.is_shutdown():
                user_input = raw_input('input : ')
                if(user_input == 'e'):
                    self.publish_exploration()
                elif(user_input == 'se'):
                    self.publish_stop_exploration()
                elif(user_input == 'q'):
                    print colored("Quitting application...", 'yellow')
                    break
                else:
                    print("\nWrong input, read the list of possible inputs.")
                    self.print_demo_usage()
                    pass
        except KeyboardInterrupt:
            print("User killed program..")
        finally:
            print colored("Thanks for using Helpie's demo!", 'yellow')


    def print_demo_usage(self):
        color = 'blue'
        print colored("Here is the list of the possible inputs :", color)
        print colored("'e'  : start exploring environment", color)
        print colored("'se' : stop exploration and create a map", color)
        print colored("'n'  : start navigation to specific point", color)
        print colored("'h'  : robot coming back at initial point", color)
        print colored("'q'  : quit Helpie's demo", color)


    def publish_exploration(self):
        exploration_status = True
        self.exploration_pub.publish(exploration_status)
        print colored("Exploration request sent", self.response_color)

    # Publish on stop exploration topic
    def publish_stop_exploration(self):
        stop_exploration_status = True
        self.stop_exploration_pub.publish(stop_exploration_status)
        print colored("End of exploration request sent", self.response_color)


    def publish_emergency(self, room):
        self.emergency_pub.publish(room)
        print colored("Emergency request sent for room " + room, self.response_color)


def main():
    app = helpie_demo_app()
    app.run()

if __name__ == "__main__":
    main()
