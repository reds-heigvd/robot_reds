#!/usr/bin/python3
# Setup Python ----------------------------------------------- #
import pygame, sys
import subprocess
import time
import select
import os
import signal
from subprocess import Popen
import signal

# Setup pygame/window ---------------------------------------- #
mainClock = pygame.time.Clock()
from pygame.locals import *
pygame.init()
pygame.display.set_caption('game base')
screen = pygame.display.set_mode((1024, 600),0,0)

BUTTON_FONT = pygame.font.SysFont(None, 40)
TITLE_FONT = pygame.font.SysFont(None, 60)
TEXT_FONT = pygame.font.SysFont(None, 30)


HOME_BG = (80, 80, 80)
BLACK = (0,0,0)
WHITE = (255, 255, 255)


click = False
SCREEN_WIDTH = 1024

def draw_text(text, font, color, surface, y):
    text = font.render(text, True, color)
    text_rect = text.get_rect(center=(SCREEN_WIDTH/2, y))
    screen.blit(text, text_rect)

def draw_button(button, screen):
    pygame.draw.rect(screen, button['color'], button['rect'])
    screen.blit(button['text'], button['text rect'])


def create_button(x, y, w, h, text, callback, inactive_color, function, display):
    text_surf = BUTTON_FONT.render(text, True, WHITE)
    button_rect = pygame.Rect(x, y, w, h)
    text_rect = text_surf.get_rect(center=button_rect.center)
    button = {
        'function': function,
        'display': display,
        'rect': button_rect,
        'text': text_surf,
        'text rect': text_rect,
        'color': inactive_color,
        'callback': callback,
        }
    return button


def idlef():
    return 1

def create_centered_button(y, w, h, text, callback, inactive_color, function, display):
    return create_button(SCREEN_WIDTH/2 - w/2, y, w, h, text, callback, inactive_color, function, display)

def buttons_setup(button_list, screen):
    for button in button_list:
        if button['display']:
            draw_button(button, screen)

def event_treatment(event, button_list, active_color, inactive_color):
    running = True
    if event.type == pygame.MOUSEBUTTONUP:
        # 1 is the left mouse button, 2 is middle, 3 is right.
        if event.button == 1:
            for button in button_list:
                # `event.pos` is the mouse position.
                if button['rect'].collidepoint(event.pos):
                    if button['function'] == 'back':
                        running = False
                    else:
                        button['callback']()

    elif event.type == pygame.MOUSEMOTION:
        for button in button_list:
            if button['rect'].collidepoint(event.pos):
                button['color'] = active_color
            else:
                button['color'] = inactive_color
    return running


def main_menu():
    active_color = (230, 150, 60)
    inactive_color = (170, 110, 40)
    running = True
    button1 = create_centered_button(150, 300, 80, 'Manual', manual_menu, inactive_color, 'action', True)
    button2 = create_centered_button(300, 300, 80, 'SLAM navigation', idlef, inactive_color, 'action', True)
    button4 = create_centered_button(450, 300, 80, 'Map localisation', idlef, inactive_color, 'action', True)
    button3 = create_button(900, 480, 80, 80, 'Quit', idlef, inactive_color, 'back', True)
    button_list = [button1, button2, button3, button4]
    while running == True:
        screen.fill(HOME_BG)
        draw_text('Agribot demo', TITLE_FONT, WHITE, screen, 100)
        buttons_setup(button_list, screen)
        pygame.display.update()
        event = pygame.event.wait()
        running = event_treatment(event, button_list, active_color, inactive_color)
        mainClock.tick(50)
    pygame.quit()
    sys.exit()

def manual_menu():
    def init_ds4_controller():
        active_color = (200, 0, 0)
        inactive_color = (150, 0, 0)
        button1 = create_centered_button(150, 300, 80, 'Init DS4', idlef, inactive_color, 'action', True)
        button2 = create_centered_button(450, 300, 80, 'Stop', idlef, inactive_color, 'back', True)
        button_list = [button1, button2]
        running_init = True
        process = subprocess.Popen(['ds4drv&'], stdout=subprocess.PIPE, universal_newlines=True)
        while running_init:
            screen.fill(HOME_BG)
            draw_text('Agribot demo', TITLE_FONT, WHITE, screen, 100)
            draw_text('Press Share button, then Play button.', TEXT_FONT, WHITE, screen, button_list[0]['rect'].y + 120)
            buttons_setup(button_list, screen)
            pygame.display.update()
            output = process.stdout.readline()
            print(output.strip())
            # Do something else
            return_code = process.poll()
            if return_code is not None:
                print('RETURN CODE', return_code)
                # Process has finished, read rest of the output
                for output in process.stdout.readlines():
                    print(output.strip())
                break
            print(output)
            event = pygame.event.get()
            if event:
                running_init = event_treatment(event[0], button_list, active_color, inactive_color)
            mainClock.tick(60)



    active_color = (200, 0, 0)
    inactive_color = (150, 0, 0)
    running = True
    button1 = create_centered_button(150, 300, 80, 'Init DS4', init_ds4_controller, inactive_color, 'action', True)
    button2 = create_centered_button(300, 300, 80, 'Init demo', idlef, inactive_color, 'action', False)
    button3 = create_button(900, 480, 80, 80, 'Back', idlef, inactive_color, 'back', True)
    button4 = create_centered_button(400, 300, 80, 'Stop', idlef, inactive_color, 'back', False)
    button_list = [button1, button2, button3, button4]
    holy_bolly = False

    while running == True:
        screen.fill(HOME_BG)
        draw_text('Agribot demo', TITLE_FONT, WHITE, screen, 100)
        buttons_setup(button_list, screen)
        pygame.display.update()

        event = pygame.event.wait()
        running = event_treatment(event, button_list, active_color, inactive_color)
        mainClock.tick(50)

main_menu()

