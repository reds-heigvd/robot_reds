import roslaunch
import rospy

rospy.init_node('en_Mapping', anonymous=True)
uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
rospy.loginfo("started")
cli_args = ["/home/ubuntu/robot_reds/dev/robot.launch",'model:=agribot', 'mode_defined:=nav_rf20']
roslaunch_args = cli_args[1:]
roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]

parent = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)

parent.start()

rospy.sleep(10)

parent.shutdown()


