# Setup Python ----------------------------------------------- #
import pygame, sys
import subprocess
import time
import select
import os
import signal
from subprocess import Popen
import signal
import roslaunch
import rospy

# Setup pygame/window ---------------------------------------- #
mainClock = pygame.time.Clock()
from pygame.locals import *
pygame.init()
pygame.display.set_caption('game base')
screen = pygame.display.set_mode((1024, 600),0,0)

BUTTON_FONT = pygame.font.SysFont(None, 40)
TITLE_FONT = pygame.font.SysFont(None, 60)
TEXT_FONT = pygame.font.SysFont(None, 30)


HOME_BG = (80, 80, 80)
BLACK = (0,0,0)
WHITE = (255, 255, 255)


click = False
SCREEN_WIDTH = 1024

def draw_text(text, font, color, surface, y):
    text = font.render(text, True, color)
    text_rect = text.get_rect(center=(SCREEN_WIDTH/2, y))
    screen.blit(text, text_rect)

def draw_button(button, screen):
    pygame.draw.rect(screen, button['color'], button['rect'])
    screen.blit(button['text'], button['text rect'])


def create_button(x, y, w, h, text, callback, inactive_color, function, display):
    text_surf = BUTTON_FONT.render(text, True, WHITE)
    button_rect = pygame.Rect(x, y, w, h)
    text_rect = text_surf.get_rect(center=button_rect.center)
    button = {
        'function': function,
        'display': display,
        'rect': button_rect,
        'text': text_surf,
        'text rect': text_rect,
        'color': inactive_color,
        'callback': callback,
        }
    return button


def idlef():
    return 1

def create_centered_button(y, w, h, text, callback, inactive_color, function, display):
    return create_button(SCREEN_WIDTH/2 - w/2, y, w, h, text, callback, inactive_color, function, display)

def buttons_setup(button_list, screen):
    for button in button_list:
        if button['display']:
            draw_button(button, screen)

def event_treatment(event, button_list, active_color, inactive_color):
    running = True
    if event.type == pygame.MOUSEBUTTONUP:
        # 1 is the left mouse button, 2 is middle, 3 is right.
        if event.button == 1:
            for button in button_list:
                # `event.pos` is the mouse position.
                if button['rect'].collidepoint(event.pos):
                    if button['function'] == 'back':
                        running = False
                    else:
                        button['callback']()

    elif event.type == pygame.MOUSEMOTION:
        for button in button_list:
            if button['rect'].collidepoint(event.pos):
                button['color'] = active_color
            else:
                button['color'] = inactive_color
    return running


def main_menu():
    active_color = (230, 150, 60)
    inactive_color = (170, 110, 40)
    running = True
    button1 = create_centered_button(150, 300, 80, 'Manual', manual_menu, inactive_color, 'action', True)
    button2 = create_centered_button(300, 300, 80, 'Navigation', navigation_menu, inactive_color, 'action', True)
    button4 = create_centered_button(450, 300, 80, 'Explorer', idlef, inactive_color, 'action', True)
    button3 = create_button(900, 480, 80, 80, 'Quit', idlef, inactive_color, 'back', True)
    button_list = [button1, button2, button3, button4]
    while running == True:
        screen.fill(HOME_BG)
        draw_text('Agribot demo', TITLE_FONT, WHITE, screen, 100)
        buttons_setup(button_list, screen)
        pygame.display.update()
        event = pygame.event.wait()
        running = event_treatment(event, button_list, active_color, inactive_color)
        mainClock.tick(50)
    pygame.quit()
    sys.exit()


def command_execute(cmd):
    popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
    for stdout_line in iter(popen.stdout.readline, ""):
        yield stdout_line
    print("OSKOUUUUR")
    popen.stdout.close()
    return_code = popen.wait()


def launch_demo(path, model, sensors):
    active_color = (200, 0, 0)
    inactive_color = (150, 0, 0)
    button1 = create_button(900, 480, 80, 80, 'Back', idlef, inactive_color, 'back', True)
    button_list = [button1]
    screen.fill(HOME_BG)
    running_init = True

    rospy.init_node('en_Mapping', anonymous=True)
    uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    rospy.loginfo("started")
    cli_args = [path,'model:='+model, 'mode_defined:='+sensors]
    roslaunch_args = cli_args[1:]
    roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]

    parent = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)

    parent.start()

    while running == True:
        screen.fill(HOME_BG)
        draw_text('Demo running : ' + sensors, TITLE_FONT, WHITE, screen, 100)
        buttons_setup(button_list, screen)
        pygame.display.update()
        event = pygame.event.wait()
        running = event_treatment(event, button_list, active_color, inactive_color)
        mainClock.tick(50)

    parent.shutdown()



def manual_menu():
    def init_ds4_controller():
        active_color = (200, 0, 0)
        inactive_color = (150, 0, 0)
        button1 = create_centered_button(150, 300, 80, 'Init DS4', idlef, inactive_color, 'action', True)
        button2 = create_centered_button(450, 300, 80, 'Stop', idlef, inactive_color, 'back', True)
        button_list = [button1, button2]
        running_init = True
        screen.fill(HOME_BG)
        draw_text('Agribot demo', TITLE_FONT, WHITE, screen, 100)
        draw_text('Press Share button, then Play button.', TEXT_FONT, WHITE, screen, button_list[0]['rect'].y + 120)
        buttons_setup(button_list, screen)
        pygame.display.update()

        for path in command_execute(["ds4drv"]):
            print(path)
            if path.find("Scanning") > 0:
                draw_text("Scanning, press : share first, then PS", TEXT_FONT, WHITE, screen, button_list[0]['rect'].y + 150)
                pygame.display.update()

            if path.find("Found device") > 0:
                break
            #mainClock.tick(60)

    active_color = (200, 0, 0)
    inactive_color = (150, 0, 0)
    running = True
    button1 = create_centered_button(150, 300, 80, 'Init DS4', init_ds4_controller, inactive_color, 'action', True)
    button2 = create_centered_button(300, 300, 80, 'Init demo', idlef, inactive_color, 'action', False)
    button3 = create_button(900, 480, 80, 80, 'Back', idlef, inactive_color, 'back', True)
    button4 = create_centered_button(400, 300, 80, 'Stop', idlef, inactive_color, 'back', False)
    button_list = [button1, button2, button3, button4]
    holy_bolly = False

    while running == True:
        screen.fill(HOME_BG)
        draw_text('Agribot demo', TITLE_FONT, WHITE, screen, 100)
        buttons_setup(button_list, screen)
        pygame.display.update()

        event = pygame.event.wait()
        running = event_treatment(event, button_list, active_color, inactive_color)
        mainClock.tick(50)


def navigation_menu():
    def nav_rf20():
        launch_demo("/home/ubuntu/robot_reds/dev/robot.launch", "agribot", "rf20")

    active_color = (200, 0, 0)
    inactive_color = (150, 0, 0)
    running = True
    blen = 300
    col = 180
    button1 = create_button(500 - col - blen/2, 150, blen, 80, 'rf20', nav_rf20, inactive_color, 'action', True)
    button2 = create_button(500 - col - blen/2, 300, blen, 80, 'rf20_imu', idlef, inactive_color, 'action', True)
    button3 = create_button(500 - col - blen/2, 450, blen, 80, 'rf20_t265', idlef, inactive_color, 'action', True)
    button4 = create_button(500 + col - blen/2, 150, blen, 80, 't265', idlef, inactive_color, 'action', True)
    button5 = create_button(500 + col - blen/2, 300, blen, 80, 't265_rbtloc', idlef, inactive_color, 'action', True)
    button6 = create_button(500 + col - blen/2, 450, blen, 80, 'Back', idlef, inactive_color, 'action', True)
    button7 = create_button(900, 480, 80, 80, 'Back', idlef, inactive_color, 'back', True)

    button_list = [button1, button2, button3, button4, button5, button7]
    holy_bolly = False

    while running == True:
        screen.fill(HOME_BG)
        draw_text('Navigation', TITLE_FONT, WHITE, screen, 100)
        buttons_setup(button_list, screen)
        pygame.display.update()

        event = pygame.event.wait()
        running = event_treatment(event, button_list, active_color, inactive_color)
        mainClock.tick(50)


main_menu()

