----------------------------------------------------------------------------------------

HEIG-VD, Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
REDS Institute, Reconfigurable Embedded Digital Systems

----------------------------------------------------------------------------------------------------------

------------

file : README.md
Author : A.Gabriel Catel Torres
Date : 25.04.2021

Context : -

--------------------

Description : developpement software des différents robots et leur démos

_____________



## Structure du dépôt



```
​```
dev
│   README.md
│   launch_demo
│
└───external_ws             // Informations administratives
|   |
│   └───src                 // Contient les packages pour les démos des robots
|   |    |   
|   |    └───agribot        // 
|   |    |   
|   |    └───helpie        //
|   | 
|   |
│   |
└───robot_ws
|   |
│   └───hardware            // Dev hardware pour les différents robots
|   |    |   
|   |    └───agribot        // Contient les informations de la CPLD d'agribot
|   |
│   └───software            // Regroupe le code pour les différents robots
|   |    |   
|   |    └───external_ws    // Worskpace catkin pour le lancement des démos (machine host)
|   |    |   
|   |    └───gui            // GUI pour le touchscreen
|   |    |   
|   |    └───robot_ws       // Workspace catkin contenant le code pour les robots
|   |    |   
|   |    | README.md
|   |    | launch_demo.md

│   
└───gui                     
|   |
│   └───agribot             // Doc des différents composants HW d'agribot
|   |
│   └───helpie              // Doc des différents composants HW d'agribot
│   
└───publi                   // Documentation rédigée 
|   |
│   └───agribot
|   |
│   └───helpie
|   |
│   └───tutoriels           // Tutoriels permettant d'apréhender l'environnemente et ROS

​```
```