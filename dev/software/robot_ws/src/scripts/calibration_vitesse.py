#!/usr/bin/python

"""
Class for low level control of our car. It assumes ros-12cpwmboard has been
installed
"""
import rospy
import RPi.GPIO as GPIO
from i2cpwm_board.msg import Servo, ServoArray
from geometry_msgs.msg import Twist
from std_msgs.msg import Bool
from std_msgs.msg import Int32
from std_msgs.msg import Int32MultiArray
import time

val_servos = 1000

class ServoConvert():
    def __init__(self, id=1, init_val=0, range=4095, direction=1):
        self.value      = 0.0
        self.value_out  = init_val
        self.init_val    = init_val
        self._range     = range
        self._dir       = direction
        self.id         = id

    def get_value_out(self, value_in):
        self.value      = value_in
        self.value_out  = abs(value_in) * speed_limit_value
        return(self.value_out)

class DkLowLevelCtrl():
    def __init__(self):
        rospy.loginfo("Setting Up the Node...")

        rospy.init_node('dk_script')

        self.actuators = {}
        self.actuators['left_fw']    = ServoConvert(id=4)
        self.actuators['left_bw']   = ServoConvert(id=2)
        self.actuators['right_fw']  = ServoConvert(id=3)
        self.actuators['right_bw']  = ServoConvert(id=1)
        self.step = up_down_step / speed_limit_value
        self.max_speed = vehicle_max_speed
        rospy.loginfo("> Actuators corrrectly initialized")

        self._servo_msg       = ServoArray()
        for i in range(4): self._servo_msg.servos.append(Servo())
        #--- Create the servo array publisher
        self.ros_pub_servo_array    = rospy.Publisher("/servos_absolute", ServoArray, queue_size=1)
        rospy.loginfo("> Publisher corrrectly initialized")
        rospy.loginfo("Initialization complete")

    def send_servo_msg(self):
        for actuator_name, servo_obj in self.actuators.iteritems():
            self._servo_msg.servos[servo_obj.id-1].servo = servo_obj.id
            self._servo_msg.servos[servo_obj.id-1].value = servo_obj.value_out
            #rospy.loginfo("Sending to %s command %d"%(actuator_name, servo_obj.value_out))
        self.ros_pub_servo_array.publish(self._servo_msg)

    def run(self):
        self.actuators['left_fw'].get_value_out(val_servos)
        self.actuators['left_bw'].get_value_out(val_servos)
        self.actuators['right_fw'].get_value_out(-val_servos)
        self.actuators['right_bw'].get_value_out(-val_servos)
        send_servo_msg(self)
        sleep(3)
        self.actuators['left_fw'].get_value_out(0)
        self.actuators['left_bw'].get_value_out(0)
        self.actuators['right_fw'].get_value_out(0)
        self.actuators['right_bw'].get_value_out(0)
        send_servo_msg(self)


        
if __name__ == "__main__":
    dk_llc     = DkLowLevelCtrl()
    dk_llc.run()
