#!/usr/bin/python

import rospy
import actionlib
import roslaunch
import time
import agribot.msg

import agribot_pkg_manager as Agribot
from enum import IntEnum
from actionlib_msgs.msg import GoalStatus
from std_msgs.msg import Bool

class Agribot_exploration_server(object):
    _feedback = agribot.msg.ExplorationAction()
    _result = agribot.msg.ExplorationResult()

    def __init__(self):
        rospy.init_node("exploration_server")

        self._action_name = 'exploring_environment'
        self.server = actionlib.SimpleActionServer(self._action_name, agribot.msg.ExplorationAction, execute_cb=self.execute_cb, auto_start = False)
        self.server.start()
        self.exploration_ongoing = False
        self.exploration_killed = True
        self.timer_save_map = 3
        self.server_up = True
        self.r = rospy.Rate(1)

    def run(self):
        rospy.loginfo("Exploration server is UP...")
        while not rospy.is_shutdown():
            if(self.exploration_ongoing):
                self.exploration_management()
            self.r.sleep()
        rospy.loginfo("Exploration server is DOWN...")


    def exploration_management(self):
        # Launching exploration
        Agribot.slam(True)
        Agribot.exploration(True)
        # Loop at 1Hz until exploration is over
        while(self.exploration_ongoing and not rospy.is_shutdown()):
            self.r.sleep()
        Agribot.exploration(False)
        rospy.loginfo("Exploration is over, saving map...")
        # Saving the map
        Agribot.save_map(True)
        time.sleep(self.timer_save_map)
        Agribot.slam(False)
        rospy.loginfo("Map saved, ending exploration")
        # Killing exploration process
        self.exploration_killed = True


    def execute_cb(self, goal):
        success = True
        self.exploration_killed = False
        self.exploration_ongoing = True

        client = actionlib.SimpleActionClient('stop_exploration', agribot.msg.StopExplorationAction)
        client.wait_for_server()
        goal = agribot.msg.ExplorationGoal(save=True)
        client.send_goal(goal)
        client.wait_for_result()

        self.exploration_ongoing = False

        while(not self.exploration_killed) and not rospy.is_shutdown():
            self.r.sleep()

        if(success):
            self.server.set_succeeded(self._result)


if __name__ == '__main__':
    server = Agribot_exploration_server()
    server.run()
