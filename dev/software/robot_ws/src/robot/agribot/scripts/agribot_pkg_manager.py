#!/usr/bin/python
import rospy
from nav_msgs.msg import Odometry
from move_base_msgs.msg import MoveBaseActionGoal
from python_libs.ros_launcher import Ros_launcher

base_path = "/home/ubuntu/robot_reds/dev/software/robot_ws/src"
agribot_path = "/robot/agribot/launch/demo"

odometry = None

base_launcher = Ros_launcher(base_path + agribot_path + "/demo_move_base.launch")

servers_launcher = Ros_launcher(base_path + agribot_path + "/demo_servers.launch")

exploration_launcher = Ros_launcher(base_path + agribot_path + "/demo_exploration.launch")
exploration_client = None
exploration_required = False

slam_launcher = Ros_launcher(base_path + agribot_path + "/demo_slam.launch")

save_map_launcher = Ros_launcher(base_path + agribot_path + "/demo_save_map.launch")

navigation_launcher = Ros_launcher(base_path + agribot_path + "/demo_navigation.launch")
navigation_goals = []
navigation_client = None


def base(activity):
    if(activity):
        base_launcher.start()
    elif(not activity):
        base_launcher.shutdown()

def servers(activity):
    if(activity):
        servers_launcher.start()
    elif(not activity):
        servers_launcher.shutdown()

def exploration(activity):
    if(activity):
        exploration_launcher.start()
    elif(not activity):
        exploration_launcher.shutdown()

def slam(activity):
    if(activity):
        slam_launcher.start()
    elif(not activity):
        slam_launcher.shutdown()

def save_map(activity):
    if(activity):
        save_map_launcher.start()
    elif(not activity):
        save_map_launcher.shutdown()

def navigation(activity):
    if(activity):
        navigation_launcher.start()
    elif(not activity):
        navigation_launcher.shutdown()
