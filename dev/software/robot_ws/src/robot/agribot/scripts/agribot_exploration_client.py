#!/usr/bin/python

import rospy
import actionlib
import agribot.msg

import agribot_pkg_manager as Agribot
from actionlib_msgs.msg import GoalStatus
from move_base_msgs.msg import MoveBaseActionGoal

class Agribot_exploration_client(object):

    def __init__(self):
        self.client = actionlib.SimpleActionClient('exploring_environment', agribot.msg.ExplorationAction)
        self.goal = None

    def run(self):
        self.client.wait_for_server()
        self.goal = agribot.msg.ExplorationGoal(save=True)
        self.client.send_goal(self.goal)
        self.client.wait_for_result()
