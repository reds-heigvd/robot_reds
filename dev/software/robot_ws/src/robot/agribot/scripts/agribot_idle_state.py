#!/usr/bin/python

import rospy
import actionlib
import time

from enum import IntEnum
from actionlib_msgs.msg import GoalStatus
from std_msgs.msg import Bool
from std_msgs.msg import Int16
from nav_msgs.msg import Odometry
from std_msgs.msg import String
import agribot_pkg_manager as Agribot
from move_base_msgs.msg import MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped
from agribot_exploration_client import Agribot_exploration_client
# from agribot_navigation_client import Agribot_navigation_client


class Agribot_idle(object):

    def __init__(self):
        rospy.loginfo("Node agribot_idle_state created")
        rospy.init_node('agribot_idle_state')

        rospy.Subscriber("agribot/explore_environment", Bool, self.explore_callback)
        rospy.Subscriber('odom', Odometry, self.odometry_cb)
        rospy.Subscriber('agribot/navigation_goal', MoveBaseGoal, self.navigation_goal_cb)

        self.pub_estimated_pose = rospy.Publisher('initialpose', PoseWithCovarianceStamped, queue_size = 10)

        self.emergency_busy = False


    def run(self):
        rospy.loginfo("Waiting for any action from the user...")
        r = rospy.Rate(1)
        Agribot.base(True)
        Agribot.servers(True)

        # Main loop of the program
        while not rospy.is_shutdown():
            # Priority to emergencies over exploration
            # rospy.loginfo("LOOOOOOOOOPING")
            #
            # if (Helpie.navigation_goals) :
            #     self.navigation_process(Helpie.navigation_goals[0])
            #     Helpie.navigation_goals.pop(0)
            if (Agribot.exploration_required):
                self.exploration_process()
            r.sleep()

    def get_robot_pose(self):
        estimated_pose = PoseWithCovarianceStamped()
        estimated_pose.pose = Helpie.odometry.pose
        estimated_pose.header = Helpie.odometry.header
        estimated_pose.header.frame_id = "map"
        return estimated_pose

    def exploration_process(self):
        Agribot.exploration_client = Agribot_exploration_client()
        rospy.loginfo("Processing environment exploration!")
        Agribot.exploration_client.run()
        rospy.loginfo("Exploration is over !")
        Agribot.exploration_required = False

    def navigation_process(self, goal):
        Agribot.navigation_client = Helpie_navigation_client(goal)
        rospy.loginfo("Processing navigation !")
        Agribot.navigation_client.run()
        rospy.loginfo("Navigation is over !")

    def odometry_cb(self, odometry):
        Agribot.odometry = odometry

    def navigation_goal_cb(self, goal):
        Agribot.navigation_goals.append(goal)

    def explore_callback(self, exploration):
        Agribot.exploration_required = exploration.data


def main():
    idle = Agribot_idle()
    idle.run()

if __name__ == "__main__":
    main()
