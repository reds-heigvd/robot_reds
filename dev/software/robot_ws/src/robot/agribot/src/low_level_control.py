#!/usr/bin/python

"""
Class for low level control of our car. It assumes ros-12cpwmboard has been
installed
"""
import rospy
import RPi.GPIO as GPIO
from i2cpwm_board.msg import Servo, ServoArray
from geometry_msgs.msg import Twist
from std_msgs.msg import Bool
from std_msgs.msg import Int32
from std_msgs.msg import Int32MultiArray
import time

speed_limit_value = rospy.get_param('agribot_speed_multiplier', 1)
up_down_step = rospy.get_param('agribot_step_value', 50)
vehicle_max_speed = rospy.get_param('agribot_max_pwm', 4095)

class ServoConvert():
    def __init__(self, id=1, init_val=0, range=4095, direction=1):
        self.value      = 0.0
        self.value_out  = init_val
        self.init_val    = init_val
        self._range     = range
        self._dir       = direction
        self.id         = id

    def get_value_out(self, value_in):
        self.value      = value_in
        self.value_out  = abs(value_in) * speed_limit_value
        return(self.value_out)

class DkLowLevelCtrl():
    def __init__(self):
        GPIO.setmode (GPIO.BOARD)
        GPIO.setup (29,GPIO.OUT)
        GPIO.setup (31,GPIO.OUT)
        GPIO.output(29, False)
        GPIO.output(31, True)
        rospy.loginfo("Setting Up the Node...")
        rospy.init_node('dk_llc')
        self.actuators = {}
        self.actuators['left_fw']    = ServoConvert(id=4)
        self.actuators['left_bw']   = ServoConvert(id=2)
        self.actuators['right_fw']  = ServoConvert(id=3)
        self.actuators['right_bw']  = ServoConvert(id=1)
        self.step = up_down_step / speed_limit_value
        self.max_speed = vehicle_max_speed
        rospy.loginfo("> Actuators corrrectly initialized")
        self._servo_msg       = ServoArray()
        for i in range(4): self._servo_msg.servos.append(Servo())
        #--- Create the servo array publisher
        self.ros_pub_servo_array    = rospy.Publisher("/servos_absolute", ServoArray, queue_size=1)
        rospy.loginfo("> Publisher corrrectly initialized")
        #--- Create the Subscriber to Twist commands
        self.ros_sub_lwheel          = rospy.Subscriber("/wheels_velo", Int32MultiArray, self.set_wheels_velocity)
        rospy.loginfo("Initialization complete")


    def adjust_wheels_velocity(self, new_val, prev_val):
        if new_val < prev_val and abs(prev_val - new_val) > self.step:
            new_val = prev_val - self.step if prev_val - self.step >= 0 else 0
        elif new_val > prev_val and abs(new_val - prev_val) > self.step:
            new_val = prev_val + self.step
        return new_val - self.max_speed


    ########## WHEELS CONTROL ##########
    def set_wheels_velocity(self, message):
        left_val = self.adjust_wheels_velocity(message.data[0] + self.max_speed, self.actuators['left_fw'].value + self.max_speed)
        if left_val < 0:
            GPIO.output(31, False)
        elif left_val > 0:
            GPIO.output(31, True)
        self.actuators['left_fw'].get_value_out(left_val)
        self.actuators['left_bw'].get_value_out(left_val)

        right_val = self.adjust_wheels_velocity(message.data[1] + self.max_speed, self.actuators['right_fw'].value + self.max_speed)
        if right_val < 0:
            GPIO.output(29, True)
        elif right_val > 0:
            GPIO.output(29, False)
        self.actuators['right_fw'].get_value_out(right_val)
        self.actuators['right_bw'].get_value_out(right_val)
        self.send_servo_msg()


    def send_servo_msg(self):
        for actuator_name, servo_obj in self.actuators.iteritems():
            self._servo_msg.servos[servo_obj.id-1].servo = servo_obj.id
            self._servo_msg.servos[servo_obj.id-1].value = servo_obj.value_out
            #rospy.loginfo("Sending to %s command %d"%(actuator_name, servo_obj.value_out))
        self.ros_pub_servo_array.publish(self._servo_msg)

    def run(self):
        #--- Set the control rate
        rate = rospy.Rate(20)
        rospy.loginfo("speed multiplier = %f"%(speed_limit_value))
        rospy.loginfo("step value = %d"%(up_down_step))
        rospy.loginfo("max PWM = %d"%(vehicle_max_speed))
        while not rospy.is_shutdown():
            rate.sleep()
            #self.send_servo_msg()
        GPIO.cleanup()

if __name__ == "__main__":
    dk_llc     = DkLowLevelCtrl()
    dk_llc.run()
