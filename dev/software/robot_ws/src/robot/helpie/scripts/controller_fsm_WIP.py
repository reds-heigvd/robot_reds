#!/usr/bin/python

import rospy
import actionlib
import roslaunch

# Include the generated message
import helpie.msg
import thermal_pkg.msg
from actionlib_msgs.msg import GoalStatus

import os
import enum
import subprocess
import time

class Research(enum.Enum):
    body_research = 1
    nose_research = 2
    breath_research = 3


class Demo_helpie :
    def __init__(self):
        rospy.init_node('controller_fsm')
        rospy.loginfo("Node Created\n\n")

        # Exploration
        self.exploration_uuid = None
        self.launch_exploration = None
        self.exp_running = False
        # Map saver
        self.map_saver_uuid = None
        self.launch_map_saver = None
        self.msr_running = False
        # Navigation
        self.navigation_uuid = None
        self.launch_navigation = None
        self.nav_running = False
        # Navigation
        self.navigation_uuid = None
        self.launch_navigation = None
        self.nav_running = False

    def demo_run(self):
        choice = 'a'
        user_message = ("In case of emergency, type e\nTo quit, type q\n")

        #First of all, the user needs to specify if using an existing map or exploration is required
        conf_input = input("Chose the mode :\n1. Generate a new map\n2. Use existing map\n")
        while(1) :
            if(conf_input == 'q') :
                rospy.loginfo("Thanks for using the unified demo. You are awesome!")
                return 0
            if(conf_input == '1' or conf_input == '2') :
                break
            rospy.loginfo("\nNot a valid choice.\n")
            conf_input = input("Chose the mode :\n1. Generate a new map\n2. Use existing map\n")

        if (conf_input == '1'):
            self.map_exploration()

        # Starting what is needed for the demo
        self.navigation_activity(True)

        # Managing the emergencies
        while choice != 'q':
            # Simulating emergencies
            choice = input(user_message)
            if choice == 'e':
                status = emergency_detected()
                if not status:
                    rospy.logerr("An error occurred during the emergency handling")
            elif choice == 'q':
                break

        self.navigation_activity(False)


    def map_exploration(self):
        print("Exploring environment...\n")
        print("Enter s to stop exploration and save the map.\n")
        self.exploration_activity(True)
        explore_input = 'a'
        while explore_input != 's' :
            explore_input = input("Input > ")

        self.map_saver_activity(True)
        print("Exploration done, saving map...\n")
        time.sleep(5)
        self.exploration_activity(False)


    def launch_file_gen(self, running, uuid, launch, activity, launch_path):
        if(running == True and activity == False):
            uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(uuid)
            launch = roslaunch.parent.ROSLaunchParent(uuid, launch_path)
            launch.start()
            activity = True
        elif(running == False and activity == True):
            launch.shutdown()
            activity = False
        return uuid, launch, activity


    def exploration_activity(self, running):
        path = ["/home/husarion/robot_reds/software/dev/src/robot/helpie/launch/demo_exploration.launch"]
        self.exploration_uuid, self.launch_exploration, self.exp_running = self.launch_file_gen(running, self.exploration_uuid,
                                self.launch_exploration, self.exp_running, path)


    def map_saver_activity(self, running):
        path = ["/home/husarion/robot_reds/software/dev/src/robot/helpie/launch/demo_save_map.launch"]
        self.map_saver_uuid, self.launch_map_saver, self.msr_running = self.launch_file_gen(running, self.map_saver_uuid,
                                self.launch_map_saver, self.msr_running, path)



    def navigation_activity(self, running):
        path = ["/home/husarion/robot_reds/software/dev/src/robot/helpie/launch/demo_navigation.launch"]
        self.navigation_uuid, self.launch_navigation, self.nav_running = self.launch_file_gen(running, self.navigation_uuid,
                                self.launch_navigation, self.nav_running, path)


    def displacement_to_room(self, room_number):
        """ Move to the room where the user has detected an emergency """
        rospy.loginfo("Displacement to the room " + str(room_number))
        try:
            client = actionlib.SimpleActionClient('GotoAction', helpie.msg.GotoAction)
            client.wait_for_server()
            goal = helpie.msg.GotoGoal(roomNb=room_number)

            client.send_goal(goal)
            client.wait_for_result()
            return (client.get_state() == GoalStatus.SUCCEEDED)
        except rospy.ROSInterruptException:
            return False

    def manual_displacement(self):
        """ The user guides the robot up to the room where an emergency has been
        detected.  """
        rospy.loginfo("Manual displacement of the robot using RVIZ")
        input("Place the robot in the room and press ENTER to continue\n")
        return True

    def room_exploration(self, room_number):
        """ Explore a room in the goal to find a body on the floor. """
        rospy.loginfo("Room exploration")
        try:
            client = actionlib.SimpleActionClient('BodySearchAction',
                helpie.msg.BodySearchAction)
            client.wait_for_server()
            goal = helpie.msg.BodySearchGoal(roomNb=room_number)
            client.send_goal(goal)
            client.wait_for_result()

            result = client.get_result()

            return ((client.get_state() == GoalStatus.SUCCEEDED) and (result.body_found))
        except rospy.ROSInterruptException:
            return False

    def nose_research(self):
        """ Search for the face/nose of the person on the floor """
        # TODO Only detect the face and not the nose.
        rospy.loginfo("Research face/nose")
        try:
            client = actionlib.SimpleActionClient('FaceSearchAction',
                helpie.msg.FaceSearchAction)
            client.wait_for_server()
            goal = helpie.msg.FaceSearchGoal(start=True)

            client.send_goal(goal)
            client.wait_for_result()
            result = client.get_result()
            return ((client.get_state() == GoalStatus.SUCCEEDED) and (result.face_detected))

        except rospy.ROSInterruptException:
            return False

    def breath_monitoring(self):
        """ Monitor the breath of a person on the floor """
        rospy.loginfo("Monitor breath")
        try:
            client = actionlib.SimpleActionClient('BreathAction',
                thermal_pkg.msg.BreathAction)

            goal = thermal_pkg.msg.BreathGoal(start=True)

            client.send_goal(goal)
            client.wait_for_result()
            return (client.get_state() == GoalStatus.SUCCEEDED)

        except rospy.ROSInterruptException:
            return False

    def emergency_detected(self):
        """ The user has indicated that an emergency has been detected. The
        robot is therefore in emergency mode """
        room_number = -1
        emergency_message = "Enter the room number of the emergency\n"
        nose_message = ("To continue to search for a nose, type y\n"
                        "To search for a body again, type b\n"
                        "To abort the emergency, type n\n")

        # Ask for the room number
        try:
            room_number = int(input(emergency_message))
        except ValueError:
            rospy.logerr("Invalid room number")
            return False

        # Move to the room
        status = displacement_to_room(room_number)
        if not status:
            # If the robot does not manage to go to the room, the user can
            # manually move the robot up to the room
            manual_displacement()

        research = Research.body_research
        emergency_end = False
        breath_count = 0
        while (not emergency_end):
            # Research a body lying on the floor
            if research == Research.body_research:
                status = room_exploration(room_number)
                if not status:
                    rospy.logwarn("No body found yet.")
                    continue_body = input("Continue to search? (y/n\n)")
                    if (continue_body != 'y'):
                        emergency_end = True
                else:
                    research = Research.nose_research

            # Research the face/nose of the person on the floor
            elif research == Research.nose_research:
                status = nose_research()
                if not status:
                    rospy.logwarn("No nose found yet.")
                    continue_nose = input(nose_message)
                    if continue_nose == 'b':
                        research = Research.body_research
                    elif continue_nose == 'n':
                        emergency_end = True
                else:
                    research = Research.breath_research

            # Monitor breath signal
            elif research == Research.breath_research:
                status = breath_monitoring()
                breath_count += 1
                # If monitoring the breath is failing more than three times
                # we try to find the nose again
                if not status and breath_count == 3:
                    breath_count = 0
                    rospy.logwarn("No breath signal detected")
                    research = Research.nose_research
                elif status:
                    breath_count = 0

        rospy.loginfo("Return home")
        displacement_to_room(-1)
        return True
        _generate_uuid(None, False)
            roslaunch.configure_logging(uuid)
            launch = roslaunch.parent.ROSLaunchParent(uuid, launch_path)
            launch.start()
            activity = True
        elif(running == False and activity == True):
            launch.shutdown()
            activity = False
        retus

def main():
    demo = Demo_helpie()
    demo.demo_run()
    print("Goodbye\n")


if __name__ == "__main__":
    main()
