#!/usr/bin/python

import rospy
import actionlib
import helpie.msg
import random

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped
import helpie_pkg_manager as Helpie
from actionlib_msgs.msg import GoalStatus
from actionlib_msgs.msg import GoalID

from std_msgs.msg import Bool
import tf
import geometry_msgs

class Helpie_room_exploration(object):
    def __init__(self, room):
        self.room = room
        self.client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
        self.goal = None
        rospy.Subscriber("helpie/body_found", Bool, self.body_found_callback)
        rospy.Subscriber("helpie/stop_room_exploration", Bool, self.stop_room_exploration_callback)

        self.pub_cancel_navigation = rospy.Publisher('move_base/cancel', GoalID, queue_size = 1)
        self.body_found = False
        self.stop_exploration = False

    def run(self):
        self.client.wait_for_server()
        while(not self.body_found or self.stop_exploration):
            self.goal = MoveBaseGoal()
            self.goal.target_pose.header.frame_id = "map"
            self.goal.target_pose.header.stamp = rospy.Time.now()

            self.goal.target_pose.pose.position.x = random.uniform(Helpie.rooms[self.room][0][0], Helpie.rooms[self.room][0][1])
            self.goal.target_pose.pose.position.y = random.uniform(Helpie.rooms[self.room][1][0], Helpie.rooms[self.room][1][1])
            quaternion = tf.transformations.quaternion_from_euler(0, 0, random.uniform(0, 3.14))
            self.goal.target_pose.pose.orientation.x = quaternion[0]
            self.goal.target_pose.pose.orientation.y = quaternion[1]
            self.goal.target_pose.pose.orientation.z = quaternion[2]
            self.goal.target_pose.pose.orientation.w = quaternion[3]

            print self.goal

            self.client.send_goal(self.goal)
            self.client.wait_for_result()

        rospy.loginfo("End of body research.")



    def body_found_callback(self, message):
        rospy.loginfo("Body found !")
        self.body_found = True
        cancel_msg = GoalID()
        self.pub_cancel_navigation.publish(cancel_msg)

    def stop_room_exploration_callback(self, message):
        rospy.loginfo("Stop room exploration.")
        self.stop_room_exploration = message.data
