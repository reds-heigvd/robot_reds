#!/usr/bin/python

import rospy
import actionlib
import roslaunch
import time
import helpie.msg

#from python_libs.ros_launcher import Ros_launcher
from enum import IntEnum
from actionlib_msgs.msg import GoalStatus
from std_msgs.msg import Bool


class Helpie_stop_exploration_server(object):
    _feedback = helpie.msg.StopExplorationAction()
    _result = helpie.msg.StopExplorationResult()

    def __init__(self):
        rospy.init_node("stop_exploration_server")
        rospy.Subscriber("helpie/stop_explore_environment", Bool, self.stop_explore_callback)

        self._action_name = 'stop_exploration'
        self.server = actionlib.SimpleActionServer(self._action_name, helpie.msg.StopExplorationAction, execute_cb=self.execute_cb, auto_start = False)
        self.server.start()
        self.exploration_over = True
        self.r = rospy.Rate(1)

    def run(self):
        rospy.loginfo("Stop exploration server is UP...")
        while not rospy.is_shutdown():
            self.r.sleep()
        rospy.loginfo("Stop Exploration server is DOWN...")


    def execute_cb(self, goal):
        rospy.loginfo("Checking frontiers or user input to end exploration")
        success = True
        # Set to false and waiting for algotithm or user Hard stop
        self.exploration_over = False
        # Loop until exploration needs to stop
        while(not self.exploration_over):
            # TODO : checking frontier algorithm
            self.r.sleep()

        rospy.loginfo("Exploration is over !")

        if(success):
            self.server.set_succeeded(self._result)
            rospy.loginfo("Hard stop from user...")


    def stop_explore_callback(self, state):
        self.exploration_over = state.data


if __name__ == '__main__':
    server = Helpie_stop_exploration_server()
    server.run()
