#!/usr/bin/python

import rospy
import actionlib
import helpie.msg

import helpie_pkg_manager as Helpie
from actionlib_msgs.msg import GoalStatus
from move_base_msgs.msg import MoveBaseActionGoal

class Helpie_exploration_client(object):

    def __init__(self):
        self.client = actionlib.SimpleActionClient('exploring_environment', helpie.msg.ExplorationAction)
        self.goal = None

    def run(self):
        self.client.wait_for_server()
        self.goal = helpie.msg.ExplorationGoal(save=True)
        self.client.send_goal(self.goal)
        self.client.wait_for_result()
