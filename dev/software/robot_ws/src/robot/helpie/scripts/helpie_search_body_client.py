#!/usr/bin/python

import rospy
import actionlib
import helpie.msg

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped
import helpie_pkg_manager as Helpie
from actionlib_msgs.msg import GoalStatus

class Helpie_navigation_client(object):

    def __init__(self, goal):
        self.client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
        #self.pub = rospy.Publisher('initialpose', PoseWithCovarianceStamped, queue_size = 10)
        self.goal = goal

    def run(self):
        Helpie.navigation(True)
        self.client.wait_for_server()
        # Give AMCL an estimated initial pose from the robot odometry
        estimated_pose = PoseWithCovarianceStamped()
        estimated_pose.pose = Helpie.odometry.pose
        estimated_pose.header = Helpie.odometry.header
        estimated_pose.header.frame_id = "map"
        #self.pub.publish(estimated_pose)
        self.goal.target_pose.header.stamp = rospy.Time.now()
        self.client.send_goal(self.goal)
        self.client.wait_for_result()
