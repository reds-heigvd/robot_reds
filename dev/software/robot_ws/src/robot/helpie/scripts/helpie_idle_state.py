#!/usr/bin/python

import rospy
import actionlib
import time
import helpie.msg
import random
import tf

from enum import IntEnum
from actionlib_msgs.msg import GoalStatus
from std_msgs.msg import Bool
from std_msgs.msg import Int16
from helpie.msg import Emergency
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseWithCovarianceStamped
import helpie_pkg_manager as Helpie
from move_base_msgs.msg import MoveBaseGoal
from helpie_exploration_client import Helpie_exploration_client
from helpie_navigation_client import Helpie_navigation_client
from helpie_room_exploration import Helpie_room_exploration



class Helpie_idle(object):

    def __init__(self):
        rospy.loginfo("Node helpie_idle_state created")
        rospy.init_node('helpie_idle_state')

        rospy.Subscriber("helpie/emergency", Int16, self.emergency_callback)
        rospy.Subscriber("helpie/explore_environment", Bool, self.explore_callback)
        rospy.Subscriber('odom', Odometry, self.odometry_cb)
        rospy.Subscriber('helpie/navigation_goal', MoveBaseGoal, self.navigation_goal_cb)

        self.pub_estimated_pose = rospy.Publisher('initialpose', PoseWithCovarianceStamped, queue_size = 10)

        self.emergency_busy = False


    def run(self):
        rospy.loginfo("Waiting for a new event (emergency / exploration)...")
        r = rospy.Rate(1)
        Helpie.base(True)
        Helpie.servers(True)

        # Main loop of the program
        while not rospy.is_shutdown():
            # Priority to emergencies over exploration
            if(Helpie.emergencies):
                self.emergency_process(Helpie.emergencies[0])
                Helpie.emergencies.pop(0)
                Helpie.nb_emergencies_processed += 1
            elif(Helpie.navigation_goals) :
                self.navigation_process(Helpie.navigation_goals[0])
                Helpie.navigation_goals.pop(0)
            elif(Helpie.exploration_required):
                self.exploration_process()
            r.sleep()

    def emergency_process(self, room):
        Helpie.navigation(True)
        rospy.loginfo("Processing a new emergency!")
        rospy.loginfo("Navigating to room %d", room)

        # Give estimated pose to AMCL
        self.pub_estimated_pose.publish(self.get_robot_pose())

        # Step 1 : go to the room where the emergency has been detected
        self.navigation_process(self.pose_from_room(room))

        # Step 2 : find the body in the room
        body_found = self.search_body(room)
        if(body_found):
            rospy.loginfo("Body found, trying to find the nose...")
        else:
            rospy.loginfo("Body not found, aborting emergency...")

        rospy.loginfo("End of the emergency")

        Helpie.navigation(False)


    def get_robot_pose(self):
        estimated_pose = PoseWithCovarianceStamped()
        estimated_pose.pose = Helpie.odometry.pose
        estimated_pose.header = Helpie.odometry.header
        estimated_pose.header.frame_id = "map"
        return estimated_pose

    def search_body(self, room):
        #Helpie.body_detection(True)
        Helpie.room_exploration = Helpie_room_exploration(room)
        rospy.loginfo("Searching body...")
        Helpie.room_exploration.run()
        rospy.loginfo("Stop searching body.")
        return Helpie.room_exploration.body_found

    def pose_from_room(self, room):
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = "map"
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose.position.x = random.uniform(Helpie.rooms[room][0][0], Helpie.rooms[room][0][1])
        goal.target_pose.pose.position.y = random.uniform(Helpie.rooms[room][1][0], Helpie.rooms[room][1][1])
        quaternion = tf.transformations.quaternion_from_euler(0, 0, random.uniform(0, 3.14))
        goal.target_pose.pose.orientation.x = quaternion[0]
        goal.target_pose.pose.orientation.y = quaternion[1]
        goal.target_pose.pose.orientation.z = quaternion[2]
        goal.target_pose.pose.orientation.w = quaternion[3]
        print goal
        return goal

    def exploration_process(self):
        Helpie.exploration_client = Helpie_exploration_client()
        rospy.loginfo("Processing environment exploration!")
        Helpie.exploration_client.run()
        rospy.loginfo("Exploration is over !")
        Helpie.exploration_required = False

    def navigation_process(self, goal):
        Helpie.navigation_client = Helpie_navigation_client(goal)
        rospy.loginfo("Processing navigation !")
        Helpie.navigation_client.run()
        rospy.loginfo("Navigation is over !")

    def odometry_cb(self, odometry):
        Helpie.odometry = odometry

    def navigation_goal_cb(self, goal):
        Helpie.navigation_goals.append(goal)

    def explore_callback(self, exploration):
        Helpie.exploration_required = exploration.data

    def emergency_callback(self, emergency):
        rospy.loginfo("New emergency detected!")
        Helpie.emergencies.append(emergency.data)

def main():
    idle = Helpie_idle()
    idle.run()

if __name__ == "__main__":
    main()
