#!/usr/bin/env python

"""
Author: Antonio Cusanelli 
Brief:  programm that takes the frames from a webcam and plubishes
        them on a ROS topic, so that we can access to the frames from 
        other ROS nodes 
"""

import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

cap = cv2.VideoCapture(0)
cap.set(3,640)
cap.set(4,480)

def main() :
    rospy.init_node('image_pub', anonymous=False)
    bridge = CvBridge()
    image_pub = rospy.Publisher("/image_top",Image, queue_size=10)  
        
    while not rospy.is_shutdown():
        # Read the frame
        _, img = cap.read()
        image_pub.publish(bridge.cv2_to_imgmsg(img, "bgr8"))   
       
if __name__ == '__main__' :
    main()
    