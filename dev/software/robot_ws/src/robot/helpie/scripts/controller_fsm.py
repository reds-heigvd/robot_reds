#!/usr/bin/python

import rospy
import actionlib
import roslaunch

# Include the generated message
import helpie.msg
import thermal_pkg.msg
from actionlib_msgs.msg import GoalStatus

import os
import enum
import subprocess
import time

class Research(enum.Enum):
    body_research = 1
    nose_research = 2
    breath_research = 3

def initialisation():
    rospy.init_node('controller_fsm')
    return True

def close_program():
    return True


def environment_exploration():
    print("Exploring environment...\n")
    print("Enter s to stop exploration and save the map.\n")

    print("Exploring environment...\n")
    print("Enter s to stop exploration and save the map.\n")

    exploration_uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    roslaunch.configure_logging(exploration_uuid)
    launch_exploration = roslaunch.parent.ROSLaunchParent(exploration_uuid,
                    ["/home/husarion/robot_reds/software/dev/src/robot/helpie/launch/demo_exploration.launch"])
    launch_exploration.start()
    rospy.loginfo("started")

    explore_input = 'a'

    while explore_input != 's' :
        explore_input = input("Input > ")
        if explore_input == "s" :
            break

    save_map_uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    roslaunch.configure_logging(save_map_uuid)
    launch_save_map = roslaunch.parent.ROSLaunchParent(save_map_uuid,
                    ["/home/husarion/robot_reds/software/dev/src/robot/helpie/launch/demo_save_map.launch"])
    launch_save_map.start()
    print("Exploration done, saving map...\n")
    # Sleep to save the map
    time.sleep(5)
    launch_save_map.shutdown()
    launch_exploration.shutdown()



def amcl_navigation_start():
    global amcl_navigation
    amcl_navigation = roslaunch.rlutil.get_or_generate_uuid(None, False)
    roslaunch.configure_logging(navigation_uuid)
    amcl_navigation = roslaunch.parent.ROSLaunchParent(exploration_uuid,
                    ["/home/husarion/robot_reds/software/dev/src/robot/helpie/launch/demo_amcl_navigation.launch"])
    amcl_navigation.start()
    rospy.loginfo("Exploration started")

def amcl_navigation_stop():
    amcl_navigation.shutdown()


def displacement_to_room(room_number):
    """ Move to the room where the user has detected an emergency """
    rospy.loginfo("Displacement to the room " + str(room_number))
    try:
        client = actionlib.SimpleActionClient('GotoAction', helpie.msg.GotoAction)
        client.wait_for_server()
        goal = helpie.msg.GotoGoal(roomNb=room_number)

        client.send_goal(goal)
        client.wait_for_result()
        return (client.get_state() == GoalStatus.SUCCEEDED)
    except rospy.ROSInterruptException:
        return False

def manual_displacement():
    """ The user guides the robot up to the room where an emergency has been
    detected.  """
    rospy.loginfo("Manual displacement of the robot using RVIZ")
    input("Place the robot in the room and press ENTER to continue\n")
    return True

def room_exploration(room_number):
    """ Explore a room in the goal to find a body on the floor. """
    rospy.loginfo("Room exploration")
    try:
        client = actionlib.SimpleActionClient('BodySearchAction',
            helpie.msg.BodySearchAction)
        client.wait_for_server()
        goal = helpie.msg.BodySearchGoal(roomNb=room_number)
        client.send_goal(goal)
        client.wait_for_result()

        result = client.get_result()

        return ((client.get_state() == GoalStatus.SUCCEEDED) and (result.body_found))
    except rospy.ROSInterruptException:
        return False

def nose_research():
    """ Search for the face/nose of the person on the floor """
    # TODO Only detect the face and not the nose.
    rospy.loginfo("Research face/nose")
    try:
        client = actionlib.SimpleActionClient('FaceSearchAction',
            helpie.msg.FaceSearchAction)
        client.wait_for_server()
        goal = helpie.msg.FaceSearchGoal(start=True)

        client.send_goal(goal)
        client.wait_for_result()
        result = client.get_result()
        return ((client.get_state() == GoalStatus.SUCCEEDED) and (result.face_detected))

    except rospy.ROSInterruptException:
        return False

def breath_monitoring():
    """ Monitor the breath of a person on the floor """
    rospy.loginfo("Monitor breath")
    try:
        client = actionlib.SimpleActionClient('BreathAction',
            thermal_pkg.msg.BreathAction)

        goal = thermal_pkg.msg.BreathGoal(start=True)

        client.send_goal(goal)
        client.wait_for_result()
        return (client.get_state() == GoalStatus.SUCCEEDED)

    except rospy.ROSInterruptException:
        return False

def emergency_detected():
    """ The user has indicated that an emergency has been detected. The
    robot is therefore in emergency mode """
    room_number = -1
    emergency_message = "Enter the room number of the emergency\n"
    nose_message = ("To continue to search for a nose, type y\n"
                    "To search for a body again, type b\n"
                    "To abort the emergency, type n\n")

    # Ask for the room number
    try:
        room_number = int(input(emergency_message))
    except ValueError:
        rospy.logerr("Invalid room number")
        return False

    # Move to the room
    status = displacement_to_room(room_number)
    if not status:
        # If the robot does not manage to go to the room, the user can
        # manually move the robot up to the room
        manual_displacement()

    research = Research.body_research
    emergency_end = False
    breath_count = 0
    while (not emergency_end):
        # Research a body lying on the floor
        if research == Research.body_research:
            status = room_exploration(room_number)
            if not status:
                rospy.logwarn("No body found yet.")
                continue_body = input("Continue to search? (y/n\n)")
                if (continue_body != 'y'):
                    emergency_end = True
            else:
                research = Research.nose_research

        # Research the face/nose of the person on the floor
        elif research == Research.nose_research:
            status = nose_research()
            if not status:
                rospy.logwarn("No nose found yet.")
                continue_nose = input(nose_message)
                if continue_nose == 'b':
                    research = Research.body_research
                elif continue_nose == 'n':
                    emergency_end = True
            else:
                research = Research.breath_research

        # Monitor breath signal
        elif research == Research.breath_research:
            status = breath_monitoring()
            breath_count += 1
            # If monitoring the breath is failing more than three times
            # we try to find the nose again
            if not status and breath_count == 3:
                breath_count = 0
                rospy.logwarn("No breath signal detected")
                research = Research.nose_research
            elif status:
                breath_count = 0

    rospy.loginfo("Return home")
    displacement_to_room(-1)

    return True



def main():
    rospy.loginfo("Welcome to Helpie's unified demo!\n\n")

    status = initialisation()
    if (not status):
        rospy.logerr("Initialisation failed")
        return False

    choice = 'a'

    user_message = ("In case of emergency, type e\n"
                    "To quit, type q\n")

    try:
        conf_input = input("Chose the mode :\n1. Generate a new map\n2. Use existing map\n")
        while(1) :
            if(conf_input == 'q') :
                rospy.loginfo("Thanks for using the unified demo. You are awesome!")
                return 0
            if(conf_input == '1' or conf_input == '2') :
                break
            rospy.loginfo("\nNot a valid choice.\n")
            conf_input = input("Chose the mode :\n1. Generate a new map\n2. Use existing map\n")

        if (conf_input == '1'):
            environment_exploration()
        amcl_navigation_start()

        # print choice message
        while choice != 'q':

            # Wait for user input
            choice = input(user_message)
            if choice == 'e':
                # Goto Emergency Mode
                status = emergency_detected()
                if not status:
                    rospy.logerr("An error occurred during the emergency handling")
            elif choice == 'q':
                break

    finally:
        # catch keyboard interrupt and close program properly
        rospy.loginfo("Killing everything...\n")
        amcl_navigation_stop()
        status = close_program()
        sleep(5)
        rospy.loginfo("Thanks for using the unified demo. You are awesome!")

if __name__ == "__main__":
    main()
