#!/usr/bin/python
import rospy
from nav_msgs.msg import Odometry
from move_base_msgs.msg import MoveBaseActionGoal
from python_libs.ros_launcher import Ros_launcher

base_path = "/home/husarion/robot_reds/dev/software/robot_ws/src"
helpie_path = "/robot/helpie/launch/demo"

odometry = Odometry()
nb_emergencies_processed = 0
emergencies = []

base_launcher = Ros_launcher(base_path + helpie_path + "/demo_move_base.launch")

servers_launcher = Ros_launcher(base_path + helpie_path + "/demo_servers.launch")

#servers_launcher = Ros_launcher(base_path + helpie_path + "/demo_body_detection.launch")

#exploration_launcher = Ros_launcher(base_path + helpie_path + "/demo_exploration.launch")
exploration_client = None
exploration_required = False

slam_launcher = Ros_launcher(base_path + helpie_path + "/demo_slam.launch")

save_map_launcher = Ros_launcher(base_path + helpie_path + "/demo_save_map.launch")

navigation_launcher = Ros_launcher(base_path + helpie_path + "/demo_navigation.launch")
navigation_goals = []
navigation_client = None

room_exploration = None
#room_exploration_launcher = Ros_launcher(base_path + helpie_path + "/room_exploration.launch")
#room_exploration = None
# Consider map size = [-5, -5], [-5, 5], [5, -5], [5, 5]
#-----------------------|----------------------|
#|                      |                      |
#|          R1          |          R2          |
#|                      |                      |
#|                                             |
#|------------------         ------------------|
#|                                             |
#|                      |                      |
#|          R3          |          R4          |
#|                      |                      |
#|----------------------|----------------------|
#
# This shape is for tests purposes

room1 = [[-5,  0], [ 0,  5]]
room2 = [[ 0,  5], [ 0,  5]]
room3 = [[-5,  0], [-5,  0]]
room4 = [[ 0,  5], [-5,  0]]
rooms = [room1, room2, room3, room4]

def base(activity):
    if(activity):
        base_launcher.start()
    elif(not activity):
        base_launcher.shutdown()

def servers(activity):
    if(activity):
        servers_launcher.start()
    elif(not activity):
        servers_launcher.shutdown()

def exploration(activity):
    if(activity):
        exploration_launcher.start()
    elif(not activity):
        exploration_launcher.shutdown()

def slam(activity):
    if(activity):
        slam_launcher.start()
    elif(not activity):
        slam_launcher.shutdown()

def save_map(activity):
    if(activity):
        save_map_launcher.start()
    elif(not activity):
        save_map_launcher.shutdown()

def navigation(activity):
    if(activity):
        navigation_launcher.start()
    elif(not activity):
        navigation_launcher.shutdown()

def body_detection(activity):
    if(activity):
        navigation_launcher.start()
    elif(not activity):
        navigation_launcher.shutdown()
