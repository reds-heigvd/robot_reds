#!/usr/bin/python

import rospy
import actionlib
import helpie.msg

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped
import helpie_pkg_manager as Helpie
from actionlib_msgs.msg import GoalStatus

class Helpie_navigation_client(object):

    def __init__(self, goal):
        self.client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
        self.goal = goal

    def run(self):
        self.client.wait_for_server()    
        self.goal.target_pose.header.stamp = rospy.Time.now()
        self.client.send_goal(self.goal)
        self.client.wait_for_result()
