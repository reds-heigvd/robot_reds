#!/usr/bin/env python

"""
Antonio Cusanelli
Based on the OpenCV tutorials, human detection using haar cascade modeles 
Date: Dec 2019
"""
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

"""
haarcascade_eye_tree_eyeglasses.xml   haarcascade_mcs_leftear.xml
haarcascade_eye.xml                   haarcascade_mcs_lefteye.xml
haarcascade_frontalface_alt2.xml      haarcascade_mcs_mouth.xml
haarcascade_frontalface_alt_tree.xml  haarcascade_mcs_nose.xml
haarcascade_frontalface_alt.xml       haarcascade_mcs_rightear.xml
haarcascade_frontalface_default.xml   haarcascade_mcs_righteye.xml
haarcascade_fullbody.xml              haarcascade_mcs_upperbody.xml
haarcascade_lefteye_2splits.xml       haarcascade_profileface.xml
haarcascade_lowerbody.xml             haarcascade_righteye_2splits.xml
haarcascade_mcs_eyepair_big.xml       haarcascade_smile.xml
haarcascade_mcs_eyepair_small.xml     haarcascade_upperbody.xml
"""

# Load the cascade
frontalFace_cascade = cv2.CascadeClassifier('/usr/local/share/opencv4/haarcascades/haarcascade_frontalface_default.xml')
upperBody_cascade = cv2.CascadeClassifier('/usr/local/share/opencv4/haarcascades/haarcascade_upperbody.xml')
profileFace_cascade = cv2.CascadeClassifier('/usr/local/share/opencv4/haarcascades/haarcascade_profileface.xml')

#Source : https://stackoverflow.com/questions/9041681/opencv-python-rotate-image-by-x-degrees-around-specific-point
def rotateImage(image, angleInDegrees):
    h, w = image.shape[:2]
    img_c = (w / 2, h / 2)

    rot = cv2.getRotationMatrix2D(img_c, angleInDegrees, 1)

    rad = math.radians(angleInDegrees)
    sin = math.sin(rad)
    cos = math.cos(rad)
    b_w = int((h * abs(sin)) + (w * abs(cos)))
    b_h = int((h * abs(cos)) + (w * abs(sin)))

    rot[0, 2] += ((b_w / 2) - img_c[0])
    rot[1, 2] += ((b_h / 2) - img_c[1])

    outImg = cv2.warpAffine(image, rot, (b_w, b_h), flags=cv2.INTER_LINEAR)
    return outImg

def callback(data) : 
    bridge = CvBridge()
    try:
      img = bridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError as e:
      print(e)
      
    img_out = img.copy()
    img =  cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Detect the faces
    frontalFaces = frontalFace_cascade.detectMultiScale(img, 
                                        scaleFactor=1.3,
                                        minNeighbors=6,
                                        minSize=(100,100),
					flags=cv2.CASCADE_SCALE_IMAGE
                                        )
   
    # Draw the rectangle around each face
    for (x, y, w, h) in frontalFaces:
        cv2.rectangle(img_out, (x, y), (x+w, y+h), (255, 0, 0), 2)
    
    upperBody = upperBody_cascade.detectMultiScale(img, 
                                        scaleFactor=1.3,
                                        minNeighbors=2,
                                        minSize=(100,100),
					flags=cv2.CASCADE_SCALE_IMAGE
                                        )
    # Draw the rectangle around each face
    for (x, y, w, h) in upperBody:
        cv2.rectangle(img_out, (x, y), (x+w, y+h), (150, 0, 0), 2)
        
    profileFaces = profileFace_cascade.detectMultiScale(img, 
                                        scaleFactor=1.3,
                                        minNeighbors=5,
                                        minSize=(100,100),
					flags=cv2.CASCADE_SCALE_IMAGE
                                        )
    # Draw the rectangle around each face
    for (x, y, w, h) in profileFaces:
        cv2.rectangle(img_out, (x, y), (x+w, y+h), (0, 0, 0), 2)

    # Display
    cv2.imshow('img', img_out)
    # Stop if escape key is pressed
    
    cv2.waitKey(3)
   

def main() :
    rospy.init_node('face_detection_opencv', anonymous=False)
    image_sub = rospy.Subscriber("/image_top", Image, callback)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()
       
if __name__ == '__main__' :
    main()