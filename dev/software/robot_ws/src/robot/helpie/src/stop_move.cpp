/*
 * stop_move.cpp
 *
 *  Created on: Oct, 2019
 *      Author: Antonio Cusanelli (based on the tutorials (ROS))
 */

#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <frontier_exploration/ExploreTaskAction.h>
#include <tf/transform_datatypes.h>
#include <iostream>
#include <unistd.h>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;


void cancel_exploration_action(ros::NodeHandle node)
{
    ros::Publisher explore_canceller = node.advertise<actionlib_msgs::GoalID>("/explore_server/cancel", 1);
    actionlib_msgs::GoalID cancel_exploration;
    cancel_exploration.id = "";
    cancel_exploration.stamp = ros::Time::now();
    explore_canceller.publish(cancel_exploration);
    
    ros::Publisher goal_pub = node.advertise<actionlib_msgs::GoalID>("/move_base/cancel", 1);
    actionlib_msgs::GoalID move_base_goal;
    move_base_goal.id = "";
    move_base_goal.stamp = ros::Time::now();
    goal_pub.publish(move_base_goal);
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "stop_node");
    ros::NodeHandle node("~");

    // create the action client	
    MoveBaseClient ac("move_base", true);
    actionlib::SimpleActionClient<frontier_exploration::ExploreTaskAction> exploreClient("explore_server", true);


    for(int i = 0; i < 4; i++){ //the number 4 is arbitrary, it is just to make sure we cancell all movement goals  
        cancel_exploration_action(node);
        exploreClient.cancelAllGoals();
        ac.cancelAllGoals();
        ros::Duration(0.2).sleep();
    } 
    
    return 0;
}





