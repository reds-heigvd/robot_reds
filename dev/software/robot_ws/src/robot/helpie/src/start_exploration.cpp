/*
 * start_exploration.cpp
 *
 *  Created on: Oct, 2019
 *      Author: Antonio Cusanelli (based on the tutorials (Husarion, ROS))
 *      brief : Program that Creates and send an exploration task to the explore action server
 */
#include <stdlib.h>
#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <helpie/ExplorationAction.h>
#include <actionlib/client/simple_action_client.h>
#include <frontier_exploration/ExploreTaskAction.h>
#include <iostream>

class ExplorationAction
{
private:
    //the robot will explore inside the chosen squared area
    frontier_exploration::ExploreTaskGoal createExplorationGoal()
    {
      frontier_exploration::ExploreTaskGoal goal;

      geometry_msgs::PointStamped center;
      center.header.frame_id = "map";
      center.point.x = 1.0;
      center.point.y = 0;
      center.point.z = 0;

      goal.explore_center = center;

      geometry_msgs::PolygonStamped square;
      square.header.frame_id = "map";
      std::vector<geometry_msgs::Point32> square_points;
      geometry_msgs::Point32 point_a;
      point_a.x = 10;
      point_a.y = 10;
      point_a.z = 0;
      geometry_msgs::Point32 point_b;
      point_b.x = 10;
      point_b.y = -10;
      point_b.z = 0;
      geometry_msgs::Point32 point_c;
      point_c.x = -1;
      point_c.y = -10;
      point_c.z = 0;
      geometry_msgs::Point32 point_d;
      point_d.x = -1;
      point_d.y = 10;
      point_d.z = 0;

      square_points.push_back(point_a);
      square_points.push_back(point_b);
      square_points.push_back(point_c);
      square_points.push_back(point_d);

      square.polygon.points = square_points;

      goal.explore_boundary = square;

      return goal;
    }

protected:
    ros::NodeHandle _nodeHandle;
    actionlib::SimpleActionServer<helpie::ExplorationAction> _actionServer;
    std::string _actionName;
    helpie::ExplorationFeedback _feedback;
    helpie::ExplorationResult _result;

public:
    ExplorationAction(std::string name):
        _actionServer(_nodeHandle, name, boost::bind(&ExplorationAction::executeCB, this, _1), false),
        _actionName(name)
    {
       _actionServer.start();
    }

    ~ExplorationAction(void)
    {
    }

    void executeCB(const helpie::ExplorationGoalConstPtr &goal)
    {
        ros::Publisher explore_canceler;
        explore_canceler = _nodeHandle.advertise<actionlib_msgs::GoalID>("/explore_server/cancel", 1);

        ROS_INFO("Create action client");
        actionlib::SimpleActionClient<frontier_exploration::ExploreTaskAction> exploreClient("explore_server", true);

        ROS_INFO("Waiting for server...");
        exploreClient.waitForServer();
        ROS_INFO("Sending goal");
        exploreClient.sendGoal(createExplorationGoal());
        ROS_INFO("Exploration triggered");

        exploreClient.waitForResult();
        ROS_INFO("Exploration stopped");
        if (goal->save) {
            /* Save the map */
            system("rosrun map_server map_saver -f ~/new_map");
        }
        _result.success = true;
        _actionServer.setSucceeded(_result);
    }
};

int main(int argc, char **argv)
{
  ROS_INFO("Exploration Trigger");
  ros::init(argc, argv, "explore_trigger");
  ExplorationAction action("ExplorationAction");

  ros::spin();
  return 0;
}
