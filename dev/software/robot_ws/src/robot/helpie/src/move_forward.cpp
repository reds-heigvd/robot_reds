/*
 * move_forward.cpp
 *
 *  Created on: Dec, 2019
 *      Author: Antonio Cusanelli
 *      Brief : program used to test the "moveDistanceFromRobot" function
 *              The robot movement must make a square or something close
 */

#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>

#include "goal_from_robot_pos.hpp"

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

bool pose_is_used = false;
geometry_msgs::Pose current_pose;
bool finish = false;

void poseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg){
    if(!pose_is_used)
        current_pose = msg->pose.pose;
    //ROS_INFO("Position-> x: [%f], y: [%f], z: [%f]", odom->pose.pose.position.x, odom->pose.pose.position.y, odom->pose.pose.position.z); // for debugging
}

void callbackThread(){
    ros::NodeHandlePtr node = boost::make_shared<ros::NodeHandle>();
    ros::Subscriber sub = node->subscribe("/amcl_pose", 100, poseCallback);
    ros::Rate loop_rate(10);
    while (ros::ok() && !finish){
        ros::spinOnce();
        loop_rate.sleep();
    }
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "move_node");
    ros::NodeHandle node("~");


    geometry_msgs::Pose pose;
    move_base_msgs::MoveBaseGoal goal;
    float angle = 0.0, new_angle = 0.0;

    // spawn another thread
    boost::thread thread_b(callbackThread);

    // create the action client
    MoveBaseClient ac("move_base", true);


    std::cout << "Press Enter to start the demo \"move\"" << std::endl;
    std::cin.ignore();

    // Wait 60 seconds for the action server to become available
    ROS_INFO("Waiting for the move_base action server");
    if(!ac.waitForServer(ros::Duration(60))){
        ROS_INFO("the Move_base action server is not running");
        return 0;
    }
    ROS_INFO("Connected to move base server");



    pose_is_used = true;
    angle = getDegreesFromQuaternion(current_pose);
    new_angle = angle + 180.0; // inverse orientation
    pose = current_pose;
    ROS_INFO("Sending robot to: x = %f, y = %f, angle = %f", current_pose.position.x, current_pose.position.y, angle);
    pose_is_used = false;

    goal = moveDistanceFromRobot(pose, 1, new_angle);

    // Send the goal command
    ac.sendGoal(goal);

    // Wait for the action to return
    ac.waitForResult();

    if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        ROS_INFO("You have reached the goal!");
    else
        ROS_INFO("The base failed for some reason");


    pose_is_used = true;
    angle = getDegreesFromQuaternion(current_pose);
    pose = current_pose;
    pose_is_used = false;

    goal = moveDistanceFromRobot(pose, 1, angle, false);

    // Send the goal command
    ac.sendGoal(goal);

    // Wait for the action to return
    ac.waitForResult();

    if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        ROS_INFO("You have reached the goal!");
    else
        ROS_INFO("The base failed for some reason");


    pose_is_used = true;
    angle = getDegreesFromQuaternion(current_pose);
    pose = current_pose;
    new_angle = angle - 90.0;
    pose_is_used = false;

    goal = moveDistanceFromRobot(pose, 1, new_angle);

    // Send the goal command
    ac.sendGoal(goal);

    // Wait for the action to return
    ac.waitForResult();

    if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        ROS_INFO("You have reached the goal!");
    else
        ROS_INFO("The base failed for some reason");


    pose_is_used = true;
    angle = getDegreesFromQuaternion(current_pose);
    new_angle = angle - 90.0; // inverse orientation
    pose = current_pose;
    pose_is_used = false;

    goal = moveDistanceFromRobot(pose, 1, new_angle);


    // Send the goal command
    ac.sendGoal(goal);

    // Wait for the action to return
    ac.waitForResult();

    if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
	ROS_INFO("You have reached the goal!");
    else
	ROS_INFO("The base failed for some reason");

    finish = true;
    thread_b.join();

    return 0;
}
