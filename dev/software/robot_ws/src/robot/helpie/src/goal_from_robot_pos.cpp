/*
 * goal_from_robot_pos.cpp
 *
 *  Created on: Dec, 2019
 *      Author: Antonio Cusanelli
 *      Brief : function that creates goal for a distance and direction relative to the robot.
 */
#include "goal_from_robot_pos.hpp"

float getDegreesFromQuaternion(const geometry_msgs::Pose& pose){
    tf::Pose tf_pose;
    tf::poseMsgToTF(pose, tf_pose);

    return tf::getYaw(tf_pose.getRotation()) * 180 / M_PI; // RAD to degrees
}

//creates goal for a distance and direction relative to the robot.
move_base_msgs::MoveBaseGoal moveDistanceFromRobot(const geometry_msgs::Pose& current_pose,
                                                  float meters,
                                                  float angle_fut,
                                                  bool forward){

    double x, y;
    float angle;

    angle = getDegreesFromQuaternion(current_pose);
    ROS_INFO("angle = %f", angle);
    x = current_pose.position.x;
    y = current_pose.position.y;

    angle = forward ? angle : angle + 90; // adjust if we want a pose on the sides
    //creation of the coordinates of the goals
    //they depends on the orientation of the robot on the map
    x += meters * cos(angle * (M_PI/180));//geometric laws for rectangle triangle (cos, sin func take radians params)
    y += meters * sin(angle * (M_PI/180));


    // Send a goal to move_base
    move_base_msgs::MoveBaseGoal goal;
    goal.target_pose.header.frame_id = "map";
    goal.target_pose.header.stamp = ros::Time::now();

    goal.target_pose.pose.position.x = x;
    goal.target_pose.pose.position.y = y;

    // Convert the Euler angle to quaternion
    double radians = angle_fut * (M_PI/180);
    tf::Quaternion quaternion;
    quaternion = tf::createQuaternionFromYaw(radians);
    geometry_msgs::Quaternion qMsg;
    tf::quaternionTFToMsg(quaternion, qMsg);

    goal.target_pose.pose.orientation = qMsg;

    ROS_INFO("Sending robot to: x = %f, y = %f, angle = %f", x, y, angle_fut);

    return goal;
}

/* Function that creates a move_base goal msg */
 move_base_msgs::MoveBaseGoal createGoal(double x, double y, double angle)
{
    move_base_msgs::MoveBaseGoal goal;
    goal.target_pose.header.frame_id = "map";
    goal.target_pose.header.stamp = ros::Time::now();

    goal.target_pose.pose.position.x = x;
    goal.target_pose.pose.position.y = y;

    // Convert the Euler angle to quaternion
    double radians = angle * (M_PI / 180);
    tf::Quaternion quaternion;
    quaternion = tf::createQuaternionFromYaw(radians);
    geometry_msgs::Quaternion qMsg;
    tf::quaternionTFToMsg(quaternion, qMsg);

    goal.target_pose.pose.orientation = qMsg;
    return goal;
}


