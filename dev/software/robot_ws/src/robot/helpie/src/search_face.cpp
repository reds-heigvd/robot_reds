/*
 * search_face.cpp
 *
 *  Created on: Dec, 2019
 *  Author: Antonio Cusanelli and Elieva Pignat
 *  Brief : Search the face of a human on the ground. The robot goes right and
 *       left to scan the temperature of the detected object and the moves to
 *       the warmest part supposely the head.
 */

#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>
#include <helpie/FaceSearchAction.h>
#include <tf/transform_datatypes.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <iostream>

#include "goal_from_robot_pos.hpp"

#define HUMAN_SCORE 300.0

class FaceSearchAction
{
protected:
    ros::NodeHandle _nodeHandle;
    actionlib::SimpleActionServer<helpie::FaceSearchAction> _actionServer;
    std::string _actionName;
    helpie::FaceSearchFeedback _feedback;
    helpie::FaceSearchResult _result;

public:
    static double human_det_score;
    static bool human;
    static geometry_msgs::Pose current_pose;

    /* update of the robot's position */
    static void poseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
    {
        FaceSearchAction::current_pose = msg->pose.pose;
    }

    /*
     * The thermal package informs if sth has the same temperature of a human
     * body
     */
    static void humanDetCallback(const std_msgs::Float32::ConstPtr& msg) {
        FaceSearchAction::human_det_score = msg->data;
        FaceSearchAction::human = false;
        if(FaceSearchAction::human_det_score > HUMAN_SCORE) {
            FaceSearchAction::human = true;
        }
    }

    FaceSearchAction(std::string name):
        _actionServer(_nodeHandle, name, boost::bind(&FaceSearchAction::executeCB, this, _1), false),
        _actionName(name)
    {
        _actionServer.start();
    }

    ~FaceSearchAction(void)
    {
    }

    ros::NodeHandle getNodeHandle()
    {
        return _nodeHandle;
    }

    void executeCB(const helpie::FaceSearchGoalConstPtr &goal)
    {
        double x, y, angle;
        ros::Publisher velocity;
        geometry_msgs::Twist cmd_vel;
        move_base_msgs::MoveBaseGoal move_goal;

        /*
         * Create a client that will send the goals to the navigation action
         * server
         */
        MoveBaseClient ac("move_base", true);
        /* Wait 60 seconds for the action server to become available */
        ROS_INFO("Waiting for the move_base action server");
        ac.waitForServer(ros::Duration(60));

        ROS_INFO("Connected to move base server");

        /*
         * if sth (human) is detected : (scanning) robot turns right as long as
         * the thermal sensors is detecting human temperature then it turns left
         * to "scan" the other side. then it goes to the side with the highest
         * temperature (it is potentially the head, in fact the upper body is
         * often the warmest part.
         */
        if (!human) {
            _result.face_detected = false;
            _actionServer.setAborted(_result);
            return;
        } else {
            double max_score = 0.0;
            double right_angle_limit, left_angle_limit, middle_angle;
            double angle_at_max_score;
            double meters = 0.0;
            bool go_right = false;

            /*
             * angular velocity (in radians/second) :
             * negative values  ==> turns right, positives values ==> turns left
             */

            velocity = _nodeHandle.advertise<geometry_msgs::Twist>("/cmd_vel", 100);
            ROS_INFO("GO right");

            /* Going right */
            cmd_vel.angular.z = -0.6;
            velocity.publish(cmd_vel);
            ros::Duration(1.0).sleep();
            velocity.publish(cmd_vel);
            /* wait until the scanning ends. */
            while (human_det_score > 50) {
                if (human_det_score > max_score) {
                    max_score = human_det_score;
                    angle_at_max_score = getDegreesFromQuaternion(current_pose);
                    go_right = true;
                    ROS_INFO("new angle max : %f", angle_at_max_score);
                }
            }

            cmd_vel.angular.z = 0.0; // stop
            velocity.publish(cmd_vel);
            right_angle_limit = getDegreesFromQuaternion(current_pose);

            ROS_INFO("GO left");
            /* Going left */

            cmd_vel.angular.z = 0.6; // angular velocity (in radians/second)
            velocity.publish(cmd_vel);
            ros::Duration(1.0).sleep();
            while (human_det_score > 50) { // wait until the scanning ends
                if (human_det_score > max_score) {
                    max_score = human_det_score;
                    angle_at_max_score = getDegreesFromQuaternion(current_pose);
                    go_right = false;
                    ROS_INFO("new angle max : %f", angle_at_max_score);
                }
            }
            cmd_vel.angular.z = 0.0; // stop
            velocity.publish(cmd_vel);
            left_angle_limit = getDegreesFromQuaternion(current_pose);


            //find middle angle and go to the warmest part
            middle_angle = (left_angle_limit - right_angle_limit) / 2 +
                right_angle_limit;

            ROS_INFO("GO max");
            cmd_vel.angular.z = -0.3; // angular velocity (in radians/second)
            velocity.publish(cmd_vel);

            /* the robot runs parrallel to the person */
            while (abs(getDegreesFromQuaternion(current_pose) -
                angle_at_max_score) > 5.0 /*degrees*/);
            cmd_vel.angular.z = 0.0; // stop
            velocity.publish(cmd_vel);

            if (abs(angle_at_max_score - middle_angle) > 60) {
                meters = 0.9;
            } else if (abs(angle_at_max_score - middle_angle) > 5) {
                meters = 0.7;
            } else {
                meters = 0.55;
            }

            if (go_right)
                meters *= -1.0; //inversion

            if (meters > 0.0) {
                ROS_INFO("GO head");
                ac.waitForServer(ros::Duration(60));
                move_goal = moveDistanceFromRobot(current_pose, meters,
                    middle_angle, true);
                ac.sendGoal(move_goal);
                ac.waitForResult();
                if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
                    ROS_INFO("You have reached the goal!");
                } else {
                    ROS_INFO("The base failed for some reason");
                    meters *= 0.5;
                    move_goal = moveDistanceFromRobot(current_pose, meters,
                        middle_angle, true);
                    ac.sendGoal(move_goal);
                    ac.waitForResult();
                    if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
                        ROS_INFO("You have reached the goal!");
                    } else {
                        ROS_INFO("The base failed for some reason");
                        _result.face_detected = false;
                        _actionServer.setAborted(_result);
                       return;
                    }
                }
            }
            _result.face_detected = true;
            _actionServer.setSucceeded(_result);
        }
    }

};

double FaceSearchAction::human_det_score;
bool FaceSearchAction::human;
geometry_msgs::Pose FaceSearchAction::current_pose;

int main(int argc, char** argv) {
    ROS_INFO("Face search node");
    ros::init(argc, argv, "face_search_node");

    FaceSearchAction action("FaceSearchAction");
    ros::Subscriber sub = action.getNodeHandle().subscribe("/amcl_pose", 100,
        FaceSearchAction::poseCallback);
    ros::Subscriber humanDetSub = action.getNodeHandle().subscribe("/human_det",
        100, FaceSearchAction::humanDetCallback);

    ros::spin();

    return 0;
}
