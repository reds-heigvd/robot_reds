/*
 * ReturnHome.cpp
 *
 *  Created on: Oct 7, 2019
 *      Author: Antonio Cusanelli (based on the code available roiyeho)
 *      brief : tell the robot to go to the initial position (base)
 */

#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
//#include <frontier_exploration/ExploreTaskAction.h>
#include <tf/transform_datatypes.h>
#include <iostream>

#include "goal_from_robot_pos.hpp"

//typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;


int main(int argc, char** argv) {
    ros::init(argc, argv, "send_goals_node");
    ros::NodeHandle node("~");

    double x = 0.0, y = 0.0, theta = 0.0; //origin coordinates


    // create the action client
    MoveBaseClient ac("move_base", true);


    std::cout << "Press Enter to start the demo \"return home\"" << std::endl;
    std::cin.ignore();

    // Wait 60 seconds for the action server to become available
    ROS_INFO("Waiting for the move_base action server");
    if(!ac.waitForServer(ros::Duration(60))){
        ROS_INFO("the Move_base action server is not running");
        return 0;
    }

    ROS_INFO("Connected to move base server");

    // Send a goal to move_base
    move_base_msgs::MoveBaseGoal goal;
    goal.target_pose.header.frame_id = "map";
    goal.target_pose.header.stamp = ros::Time::now();

    goal.target_pose.pose.position.x = x;
    goal.target_pose.pose.position.y = y;

    // Convert the Euler angle to quaternion
    double radians = theta * (M_PI/180);
    tf::Quaternion quaternion;
    quaternion = tf::createQuaternionFromYaw(radians);
    geometry_msgs::Quaternion qMsg;
    tf::quaternionTFToMsg(quaternion, qMsg);

    goal.target_pose.pose.orientation = qMsg;

    // Send the goal command
    ROS_INFO("Sending robot to: x = %f, y = %f, theta = %f", x, y, theta);
    ac.sendGoal(goal);

    // Wait for the action to return
    ac.waitForResult();

    ac.sendGoal(goal);
    ac.waitForResult();

    if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
	ROS_INFO("You have reached the goal!");
    else
	ROS_INFO("The base failed for some reason");

    return 0;
}
