/*
* File : room.cpp
* author : Antonio Cusanelli
* date : 14.10.2019
* brief : definition of the functions of the class "Room" which contains information
*         about the path that the robot must folow. these informations are used to
*         send goals to the move_base server.
*/

#include "room.hpp"

std::ostream& operator << (std::ostream& os, const Room& r) {
    os << "Room nb " << r.id << " at the position [x=" << r.position.x << ", y=" << r.position.y << ", angle=" << r.position.angle << "] : " << std::endl;
    for (size_t i = 0; i < r.posToExplore.size(); i++){
        os << "\tpos nb " << i << ": [x=" << r.posToExplore.at(i).x << ", y=" << r.posToExplore.at(i).y << ", angle=" << r.posToExplore.at(i).angle << "]" << std::endl;
    }
    return os;
}

Pos Room::getRoomPosition () const{
  return position;
}

short Room::getID () const{
  return id;
}

void Room::addPosToExplore(Pos new_pos){
  posToExplore.push_back(new_pos);
}

std::vector<Pos> Room::getPosToExplore() const{
  std::vector<Pos> out(posToExplore.size());
  std::copy(posToExplore.begin(), posToExplore.end(), out.begin());
  return out;
}

//file parser --> the program creates a vector of "Room" with the information
//found in the file.
bool createHouse (std::vector<Room>& rooms, std::string filename){
  std::ifstream fileOfPos;
  bool fileOk = false;

  fileOfPos.open(filename.c_str());

  if(fileOfPos.is_open()){
    fileOk = true;
    std::string line;
    short roomNb = 0;
    bool first = true;
    Room *room;

    while(std::getline(fileOfPos, line)){
      if(line.compare("--") == 0){ //new room
        first = true;
        rooms.push_back(*room);
        delete room;
      }else{ //creates room goals list

        std::stringstream ss(line);
        while(ss.good())
        {
          Pos p;
          std::string substr;
          short i = 0;
          while(std::getline( ss, substr, ',' )){
              double value = atof(substr.c_str());
              if(i==0)
                  p.x = value;
              if(i==1)
                  p.y = value;
              if(i==2)
                  p.angle = value;
              i++;
              //std::cout << pos << '|';
          }
          if(first){
            room = new Room(roomNb, p);
            first = false;
            roomNb++;
          }else{
            room->addPosToExplore(p);
          }
        }
      }
    }
    rooms.push_back(*room);
    delete room;
  }
  fileOfPos.close();
  return fileOk;
}
