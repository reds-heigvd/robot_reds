/*
 * goto_location.cpp
 *
 *  Created on: Dec, 2019
 *  Author: Antonio Cusanelli and Elieva Pignat
 *  Brief : Goto to a location on demand. The room number must be specified.
 *     The room location is specified in a file given in parameter.
 *     If the room -1 is specified, the robot return to the location (0,0)
 *     which is considered as its home.
 */

#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>
#include <helpie/GotoAction.h>
#include <tf/transform_datatypes.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <iostream>

#include "room.hpp"
#include "goal_from_robot_pos.hpp"

#define HUMAN_SCORE 300.0

MoveBaseClient ac("move_base", true);

class GotoAction
{
protected:
    ros::NodeHandle _nodeHandle;
    actionlib::SimpleActionServer<helpie::GotoAction> _actionServer;
    std::string _actionName;
    helpie::GotoFeedback _feedback;
    helpie::GotoResult _result;
    //void *ac;
    //MoveBaseClient ac;

public:
    std::vector<Room> rooms;
    static geometry_msgs::Pose current_pose;

    GotoAction(std::string name):
    _actionServer(_nodeHandle, name, boost::bind(&GotoAction::executeCB, this, _1), false),
    _actionName(name)
    {
        /*
         * create a client that will send the goals to the navigation action
         * server
         */
         //MoveBaseClient tmp("move_base", true);
         //ac = &tmp;
        _actionServer.start();
    }

    ~GotoAction(void)
    {
    }

    void executeCB(const helpie::GotoGoalConstPtr &goal)
    {
        MoveBaseClient ac("move_base", true);
        int roomNb;
        double x, y, angle;
        std::vector<Pos> positions;
        ros::Publisher velocity;
        geometry_msgs::Twist cmd_vel;
        move_base_msgs::MoveBaseGoal move_goal;
        std::string file;

        _nodeHandle.getParam("filename_rooms", file);
        if (!createHouse(rooms, file) || rooms.empty()) {
            std::cout << "Room file (" << file <<
                ") doesn't exist or doesn't contain any room configs"
                << std::endl;
            _result.success = false;
            _actionServer.setAborted(_result);
            return;
        }

        /* print out the information of each room */
        for (size_t i = 0; i < rooms.size(); i++) {
            std::cout << rooms.at(i) << std::endl;
        }


        roomNb = goal->roomNb;
        if (roomNb == -1) {
            ROS_INFO("Return home");
            x = 0;
            y = 0;
            angle = 0;
        } else if (roomNb >= rooms.size()) {
            ROS_INFO("Room number not valid");
            _result.success = false;
            _actionServer.setAborted(_result);
            return;
        } else {
            /* Get the room location from the room file */
            positions = rooms.at(roomNb).getPosToExplore();

            /* Initial position of the room */
            x = rooms.at(roomNb).getRoomPosition().x;
            y = rooms.at(roomNb).getRoomPosition().y;
            angle = rooms.at(roomNb).getRoomPosition().angle;
        }

        /* Wait 60 seconds for the action server to become available */
        ROS_INFO("Waiting for the move_base action server");
        ac.waitForServer(ros::Duration(60));

        ROS_INFO("Connected to move base server");

        move_goal = createGoal(x, y, angle);

        /* Send the goal command */
        ROS_INFO("Sending robot to: x = %f, y = %f, theta = %f", x, y, angle);
        ac.sendGoal(move_goal);

        /*
         * Wait for the action to return (goal reached or canceled or sth
         * went wrong).
         */
        ac.waitForResult();

        if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
            ROS_INFO("You have reached the goal!");
            _result.success = true;
            _actionServer.setSucceeded(_result);
        } else {
            ROS_INFO("The base failed for some reason");
            _result.success = false;
            _actionServer.setAborted(_result);
        }
   }

};

int main(int argc, char** argv) {
    ROS_INFO("Goto location node");
    ros::init(argc, argv, "goto_location_node");

    GotoAction action("GotoAction");

    ros::spin();

    return 0;
}
