/*
* File : fake_human_det.cpp
* Author : Antonio Cusanelli 
*
* Brief : Program that simulates scores for the human detection
*         Used for testing and debugging
*         To understand the scores value please go see the "thermal_human_det.cpp" file
*         in the thermal_pkg package 
* 
*/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <string>

#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <boost/thread/thread.hpp>

double human_score = 0.0; 

void inputHandle(){
    while (ros::ok()){
            std::string s; 
            std::cin >> s; 
            if(s.compare("a") == 0)
                human_score = 0;
            else if(s.compare("s") == 0)
                human_score = 100;
            else if(s.compare("d") == 0)
                human_score = 500;
            else if(s.compare("f") == 0)
                human_score = 1000;
            
    }
}
int main(int argc, char **argv) {
        ros::init(argc, argv, "fake_human_det");
	ros::NodeHandle n("~");
        ros::Publisher pub = n.advertise<std_msgs::Float32>("/human_det", 100);
        std_msgs::Float32 msg;
        ros::Rate r(5);
        boost::thread thread_b(inputHandle);
        
        std::cout << "Fake human detection score publication on the /human_det topic (for testing, debugging purpose)" << std::endl
                  << "Press 'a' (+ enter) to publish a score of 0, 's' to publish 100, 'd' to publish 300 and 'f' to publish 1000." << std::endl;
                  
        while (ros::ok()){            
            msg.data = human_score;
            pub.publish(msg);
            r.sleep();
        }
        
        thread_b.join();
        return 0; 
}
