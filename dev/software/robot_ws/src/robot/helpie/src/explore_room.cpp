/*
 * ExploringRoom.cpp
 *
 *  Created on: Dec, 2019
 *      Author: Antonio Cusanelli
 *      Brief : To explore an entire room without missing some corners,
 *              we tought about a list of goals that the robot must reach.
 *              The exploration can be stopped at any moment if the thermal
 *              camera sensed something whose temperature is near close to
 *              the human temperature.
 *
 *              Once something is detecting, the robot goes right and left to "scan"
 *              the temperature of the detected object and then moves to the warmest
 *              part.
 */

#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>
#include <helpie/BodySearchAction.h>
#include <tf/transform_datatypes.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <iostream>

#include "room.hpp"
#include "goal_from_robot_pos.hpp"

#define HUMAN_SCORE 300.0

class BodySearchAction
{
protected:
    ros::NodeHandle _nodeHandle;

    actionlib::SimpleActionServer<helpie::BodySearchAction> _actionServer;
    std::string _actionName;
    helpie::BodySearchFeedback _feedback;
    helpie::BodySearchResult _result;

public:
    std::vector<Room> rooms;
    static double human_det_score;
    static bool human;
    static geometry_msgs::Pose current_pose;

    //update of the robot's position
    static void poseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg) {
        BodySearchAction::current_pose = msg->pose.pose;
    }

    // thermal package informs if sth has the same temperature of a human body
    static void humanDetCallback(const std_msgs::Float32::ConstPtr& msg) {
        BodySearchAction::human_det_score = msg->data;
        BodySearchAction::human = false;
        if(BodySearchAction::human_det_score > HUMAN_SCORE) {
           // ROS_INFO("human_det callBack : [%f]", human_det_score);
            BodySearchAction::human = true;
        }
    }

    BodySearchAction(std::string name):
        _actionServer(_nodeHandle, name, boost::bind(&BodySearchAction::executeCB, this, _1), false),
        _actionName(name)
    {
        _actionServer.start();
    }

    ~BodySearchAction(void)
    {
    }

    void executeCB(const helpie::BodySearchGoalConstPtr &goal)
    {
        int roomNb;
        double x, y, angle;
        std::vector<Pos> positions;
        ros::Publisher velocity;
        geometry_msgs::Twist cmd_vel;
        move_base_msgs::MoveBaseGoal move_goal;
        std::string file;

        std::cout << "_nodeHandle: " <<static_cast<void const *>(&_nodeHandle) << std::endl;

       _nodeHandle.getParam("filename_rooms", file); //param setted in the launch file
        if (!createHouse(rooms, file) || rooms.empty()) {
            std::cout << "Room file (" << file <<
                ") doesn't exist or doesn't contain any room configs" << std::endl;
            _result.success = false;
            _actionServer.setAborted(_result);
            return;
        }

        //print out the information of each room
        for (size_t i = 0; i < rooms.size(); i++) {
            std::cout << rooms.at(i) << std::endl;
        }
        ros::Subscriber sub = _nodeHandle.subscribe("/amcl_pose", 100,
            poseCallback);
        ros::Subscriber humanDetSub = _nodeHandle.subscribe("/human_det",
            100, humanDetCallback);


        // create a client that will send the goals to the navigation action
        // server
        MoveBaseClient ac("move_base", true);

        roomNb = goal->roomNb;
        if (roomNb < 0 || roomNb >= rooms.size()) {
            ROS_INFO("Room number not valid");
            _result.success = false;
            _actionServer.setAborted(_result);
            return;
        }

        // Wait 60 seconds for the action server to become available
        ROS_INFO("Waiting for the move_base action server");
        ac.waitForServer(ros::Duration(60));

        ROS_INFO("Connected to move base server");

        // get the position of each goal to reach of the selected room
        positions = rooms.at(roomNb).getPosToExplore();

        // Initial position of the room
        x = rooms.at(roomNb).getRoomPosition().x;
        y = rooms.at(roomNb).getRoomPosition().y;
        angle = rooms.at(roomNb).getRoomPosition().angle;

        // patroling task, --> the robot will go to each goal of the selected
        // room until sth is detected
        size_t i = 0;
        do {
            move_goal = createGoal(x, y, angle);

            // Send the goal command
            ROS_INFO("Sending robot to: x = %f, y = %f, theta = %f", x, y, angle);
            ac.sendGoal(move_goal);

            // Wait for the action to return (goal reached or canceled or sth
            // went wrong). Every 0.5 s it checks if the callback of the human
            // det topic has received sth
            while(! ac.waitForResult(ros::Duration(0,500)) ){
                if(human){
                    ac.cancelAllGoals();
                    break;
                }
            }

            if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
                ROS_INFO("You have reached the goal!");
            } else {
                ROS_INFO("The base failed for some reason");
            }

            if (human) {
                break;
            }

            if (i < positions.size()) {
                x = positions.at(i).x;
                y = positions.at(i).y;
                angle = positions.at(i).angle;
            }

            i++;
        } while (i <= positions.size());

        /*
         * if sth (human) is detected : (scanning) robot turns right as long as
         * the thermal sensors is detecting human temperature then it turns left
         * to "scan" the other side. then it goes to the side with the highest
         * temperature (it is potentially the head, in fact the upper body is
         * often the warmest.
         */
        if (human) {
            double max_score = 0.0, right_angle_limit, left_angle_limit, middle_angle, angle_at_max_score;
            double meters = 0.0;
            bool go_right = false;

            velocity = _nodeHandle.advertise<geometry_msgs::Twist>("/cmd_vel", 100);
            ROS_INFO("GO right");

            // RIGHT
            // angular velocity (in radians/second) :
            // negative values  ==> turns right, positives values ==> turns left
            cmd_vel.angular.z = -0.6;
            velocity.publish(cmd_vel);
            ros::Duration(1.0).sleep();
            velocity.publish(cmd_vel);
            // wait until the scanning ends.
            while (human_det_score > 50) {
                if (human_det_score > max_score) {
                    max_score = human_det_score;
                    angle_at_max_score = getDegreesFromQuaternion(current_pose);
                    go_right = true;
                    ROS_INFO("new angle max : %f", angle_at_max_score);
                }
            }
            cmd_vel.angular.z = 0.0; // stop
            velocity.publish(cmd_vel);
            right_angle_limit = getDegreesFromQuaternion(current_pose);

            ROS_INFO("GO left");
            // LEFT
            cmd_vel.angular.z = 0.6; // angular velocity (in radians/second)
            velocity.publish(cmd_vel);
            ros::Duration(1.0).sleep();
            while (human_det_score > 50) { // wait until the scanning ends
                if (human_det_score > max_score) {
                    max_score = human_det_score;
                    angle_at_max_score = getDegreesFromQuaternion(current_pose);
                    go_right = false;
                    ROS_INFO("new angle max : %f", angle_at_max_score);
                }
            }
            cmd_vel.angular.z = 0.0; // stop
            velocity.publish(cmd_vel);
            left_angle_limit = getDegreesFromQuaternion(current_pose);


            //find middle angle and go to the warmest part
            middle_angle = (left_angle_limit - right_angle_limit) / 2 +
                right_angle_limit;

            ROS_INFO("GO max");
            cmd_vel.angular.z = -0.3; // angular velocity (in radians/second)
            velocity.publish(cmd_vel);

            // the robot will parrallel to the person
            while (abs(getDegreesFromQuaternion(current_pose) -
                angle_at_max_score) > 5.0 /*degrees*/);
            cmd_vel.angular.z = 0.0; // stop
            velocity.publish(cmd_vel);

            if (abs(angle_at_max_score - middle_angle) > 60) {
                meters = 0.9;
            } else if (abs(angle_at_max_score - middle_angle) > 5) {
                meters = 0.7;
            } else {
                meters = 0.55;
            }

            if (go_right)
                meters *= -1.0; //inversion

            if (meters > 0.0) {
                ROS_INFO("GO head");
                ac.waitForServer(ros::Duration(60));
                move_goal = moveDistanceFromRobot(current_pose, meters,
                    middle_angle, true);
                ac.sendGoal(move_goal);
                ac.waitForResult();
                if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
                    ROS_INFO("You have reached the goal!");
                } else {
                    ROS_INFO("The base failed for some reason");
                    meters *= 0.5;
                    move_goal = moveDistanceFromRobot(current_pose, meters,
                        middle_angle, true);
                    ac.sendGoal(move_goal);
                    ac.waitForResult();
                    if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
                        ROS_INFO("You have reached the goal!");
                    } else {
                        ROS_INFO("The base failed for some reason");
                    }
                }
            }
        }
        _result.success = true;
        _actionServer.setSucceeded(_result);
    }

};

double BodySearchAction::human_det_score;
bool BodySearchAction::human;
geometry_msgs::Pose BodySearchAction::current_pose;

int main(int argc, char** argv) {
    ROS_INFO("Explore room");
    ros::init(argc, argv, "explore_room_node");

    BodySearchAction action("BodySearchAction");

    ros::spin();

    return 0;
}
