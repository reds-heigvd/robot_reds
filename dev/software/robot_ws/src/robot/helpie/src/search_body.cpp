/*
 * body_search.cpp
 *
 *  Created on: Dec, 2019
 *  Author: Antonio Cusanelli and Elieva Pignat
 *  Brief: Explore an entire room by following a list of goal that the robot
 *      must reach. The exploration ends if an human is detected on the ground
 *      or if the exploration finished without finding  anybody. The room number
 *      to explore is provided in the goal of the server action and the room
 *      goals are read from the room file.
 */

#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>
#include <helpie/BodySearchAction.h>
#include <tf/transform_datatypes.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <iostream>

#include "room.hpp"
#include "goal_from_robot_pos.hpp"

#define HUMAN_SCORE 300.0

class BodySearchAction
{
protected:
    ros::NodeHandle _nodeHandle;

    actionlib::SimpleActionServer<helpie::BodySearchAction> _actionServer;
    std::string _actionName;
    helpie::BodySearchFeedback _feedback;
    helpie::BodySearchResult _result;

public:
    std::vector<Room> rooms;
    static double human_det_score;
    static bool human;

    // thermal package informs if sth has the same temperature of a human body
    static void humanDetCallback(const std_msgs::Float32::ConstPtr& msg)
    {
        BodySearchAction::human_det_score = msg->data;
        BodySearchAction::human = false;
        if(BodySearchAction::human_det_score > HUMAN_SCORE) {
            BodySearchAction::human = true;
        }
    }

    BodySearchAction(std::string name): _actionServer(_nodeHandle, name,
        boost::bind(&BodySearchAction::executeCB, this, _1), false),
        _actionName(name)
    {
        _actionServer.start();
    }

    ~BodySearchAction(void)
    {
    }

    ros::NodeHandle getNodeHandle()
    {
        return _nodeHandle;
    }

    void executeCB(const helpie::BodySearchGoalConstPtr &goal)
    {
        int roomNb;
        double x, y, angle;
        std::vector<Pos> positions;
        std::string file;
        move_base_msgs::MoveBaseGoal move_goal;

        /* param setted in the launch file */
        _nodeHandle.getParam("filename_rooms", file);
        if (!createHouse(rooms, file) || rooms.empty()) {
            std::cout << "Room file (" << file <<
                ") doesn't exist or doesn't contain any room configs"
                << std::endl;
            _result.body_found = false;
            _actionServer.setAborted(_result);
            return;
        }

        roomNb = goal->roomNb;
        if (roomNb < 0 || roomNb >= rooms.size()) {
            ROS_INFO("Room number not valid");
            _result.body_found = false;
            _actionServer.setAborted(_result);
            return;
        }

        /* print out the information of each room */
        for (size_t i = 0; i < rooms.size(); i++) {
            std::cout << rooms.at(i) << std::endl;
        }

        /*
         * create a client that will send the goals to the navigation action
         * server
        */
        MoveBaseClient ac("move_base", true);

        /* Waits for the action server to become available */
        ROS_INFO("Waiting for the move_base action server");
        ac.waitForServer();

        ROS_INFO("Connected to move base server");

        /* get the position of each goal to reach of the selected room */
        positions = rooms.at(roomNb).getPosToExplore();

        /*
         * patroling task, --> the robot will go to each goal of the selected
         * room until sth is detected
         */
        size_t i = 0;
        while (i < positions.size()) {
            x = positions.at(i).x;
            y = positions.at(i).y;
            angle = positions.at(i).angle;

            move_goal = createGoal(x, y, angle);

            /* Send the goal command */
            ROS_INFO("Sending robot to: x = %f, y = %f, theta = %f", x, y,
                angle);
            ac.sendGoal(move_goal);

            /*
             * Wait for the action to return (goal reached or canceled or sth
             * went wrong). Every 0.5 s it checks if the callback of the human
             * det topic has received sth
             */
            while(! ac.waitForResult(ros::Duration(0, 500)) ){
                if (human) {
                    ac.cancelAllGoals();
                    _result.body_found = true;
                    _actionServer.setSucceeded(_result);
                    return;
                }
            }

            if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
                ROS_INFO("You have reached the goal!");
            } else {
                ROS_INFO("The base failed for some reason");
            }

            i++;
        }

        _result.body_found = false;
        _actionServer.setSucceeded(_result);
    }

};

double BodySearchAction::human_det_score;
bool BodySearchAction::human;

int main(int argc, char** argv) {
    ROS_INFO("Body search node");
    ros::init(argc, argv, "body_search_node");

    BodySearchAction action("BodySearchAction");
    ros::Subscriber humanDetSub = action.getNodeHandle().subscribe("/human_det",
        100, BodySearchAction::humanDetCallback);


    ros::spin();

    return 0;
}
