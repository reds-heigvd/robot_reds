/*
* File : room.h
* author : Antonio Cusanelli
* date : 14.10.2019
* brief : declaration of the class "Room" which contains information about the path
*         that the robot must folow. these informations are used to send goals to
*         the move_base server.
*/

#ifndef __ROOM_H__
#define __ROOM_H__

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <stdlib.h>     /* atof */

typedef struct Position {
    double x; // in meters
    double y; // in meters
    double angle; // in degrees
} Pos;

class Room
{
  friend std::ostream& operator << (std::ostream& os, const Room& r);

public:
  Room(short id, Pos position) : id(id), position(position) {}
  ~Room(){}

  //void setRoomPosition (Pos position);
  Pos getRoomPosition () const;
  //void setID (short id);
  short getID () const;
  void addPosToExplore(Pos new_pos);

  std::vector<Pos> getPosToExplore() const;

private:
  short id; //the number of the room
  Pos position;
  std::vector<Pos> posToExplore;

};

bool createHouse (std::vector<Room>& rooms, std::string filename);

#endif /* __ROOM_H__ */
