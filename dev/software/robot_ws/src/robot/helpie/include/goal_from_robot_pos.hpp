/*
* File : goal_from_robot_pos.hpp
* author : Antonio Cusanelli
* date : 14.10.2019
* brief :
*
*/

#ifndef __GOAL_FROM_ROBOT_POS_HPP__
#define __GOAL_FROM_ROBOT_POS_HPP__

#include <actionlib/client/simple_action_client.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <std_msgs/Float32.h>
#include <cmath>


typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
float getDegreesFromQuaternion(const geometry_msgs::Pose& pose);

move_base_msgs::MoveBaseGoal moveDistanceFromRobot(const geometry_msgs::Pose& current_pose,
                                                  float meters,
                                                  float angle_fut,
                                                  bool forward = true);

move_base_msgs::MoveBaseGoal createGoal(double x, double y, double angle);


#endif /* __GOAL_FROM_ROBOT_POS_HPP_ */
