## Structure

- [robot_name]
  - description         -- urdf (robot description) and launch(s) of the description
  - params                -- 1parameters needed to launch the robot
  - ros_packages      -- launch and parameters to launch some ros packages for this specific robot
  - src                         -- specific code for the robot