#!/usr/bin/python

import rospy
import actionlib
import roslaunch

from enum import Enum
from enum import IntEnum

import os
import enum
import subprocess
import time

class Research(enum.Enum):
    body_research = 1
    nose_research = 2
    breath_research = 3

class Launch_ids(IntEnum):
    BASE_ROBOT = 0
    EXPLORATION = 1
    MAP_SAVER = 2
    NAVIGATION = 3


class Helpie_manager :
    def __init__(self):
        #rospy.init_node('helpie_manager')
        rospy.loginfo("Node Created\n\n")

        self.uuids = []
        self.launchs = []
        self.activity = []
        self.test = []

        for i in range(len(Launch_ids)):
            self.uuids.append(None)
            self.launchs.append(None)
            self.activity.append(False)
            self.test.append(i)

    def run(self):
        bibux = Launch_ids.EXPLORATION
        for i in Launch_ids:
            print(self.test[bibux])

        return 0


    def launch_file_gen(self, running, uuid, launch, activity, launch_path):
        if(running == True and activity == False):
            uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(uuid)
            launch = roslaunch.parent.ROSLaunchParent(uuid, launch_path)
            launch.start()
            activity = True
        elif(running == False and activity == True):
            launch.shutdown()
            activity = False
        return uuid, launch, activity


    def exploration_activity(self, running):
        index = Launch_ids.EXPLORATION
        path = ["/home/husarion/robot_reds/software/dev/src/robot/helpie/launch/demo_exploration.launch"]
        self.uuids[index], self.launchs[index], self.activity[index] = self.launch_file_gen(running, self.uuids[index],
                                self.launchs[index], self.activity[index], path)


    def map_saver_activity(self, running):
        index = Launch_ids.MAP_SAVER
        path = ["/home/husarion/robot_reds/software/dev/src/robot/helpie/launch/demo_save_map.launch"]
        elf.uuids[index], self.launchs[index], self.activity[index] = self.launch_file_gen(running, self.uuids[index],
                                self.launchs[index], self.activity[index], path)



    def navigation_activity(self, running):
        index = Launch_ids.NAVIGATION
        path = ["/home/husarion/robot_reds/software/dev/src/robot/helpie/launch/demo_navigation.launch"]
        elf.uuids[index], self.launchs[index], self.activity[index] = self.launch_file_gen(running, self.uuids[index],
                                self.launchs[index], self.activity[index], path)



def main():
    manager = Helpie_manager()
    manager.run()

if __name__ == "__main__":
    main()
