/*
* File : thermal_human_det.cpp
* Author : Antonio Cusanelli 
*
* Brief : program that calculates a score based on the proxomity between the skin temperature range
*         and the current thermal sensor values. If a score is good enough a string msg is published
*         to inform other nodes.
*/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>

#include <ros/ros.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Float32.h"

#define COL_ROW_SIZE 32
using namespace cv;

class HumanDetWithThermal {
private:
    ros::Subscriber sub;
    ros::Publisher pub;
    Mat mtx, src;

public:
    HumanDetWithThermal(ros::NodeHandle n);
    ~HumanDetWithThermal() {}
    void tempCallback(const std_msgs::Float64MultiArray::ConstPtr& array);
};

HumanDetWithThermal::HumanDetWithThermal(ros::NodeHandle n) : mtx(COL_ROW_SIZE, COL_ROW_SIZE, CV_8UC1, Scalar::all(0)) {
    sub = n.subscribe("/teraranger_evo_thermal/raw_temp_array", 10, &HumanDetWithThermal::tempCallback, this);
    pub = n.advertise<std_msgs::Float32>("/human_det", 100);
}

void HumanDetWithThermal::tempCallback(const std_msgs::Float64MultiArray::ConstPtr& array){
	double current_score = 0.0; 

        for(int i = 0; i < COL_ROW_SIZE; i++){
            for(int j = 0; j < COL_ROW_SIZE; j++){		
                if(array->data[j + i * 32] > 32.0 && array->data[j + i * 32] < 38.0){
			mtx.at<uchar>(i,j) = 255;
			current_score += 4.5;
                }else if(array->data[j + i * 32] > 29.0 && array->data[j + i * 32] < 32.1){
			mtx.at<uchar>(i,j) = 150;
			current_score += 2.0;
                }else{
			mtx.at<uchar>(i,j) = 0;
                        //current_score += 0.3;
            	}
            }
        }


        std_msgs::Float32 msg;
        msg.data = current_score;
        pub.publish(msg);

        resize(mtx, src, Size(), 7, 7);
        imshow( "thermal_raw", src );

        std::cout << "score : " << current_score << std::endl;
        waitKey(3);
	return;
}

int main(int argc, char **argv) {
	namedWindow("thermal_raw");
        
        ros::init(argc, argv, "thermal_human_det_node");
	ros::NodeHandle n("~");
        HumanDetWithThermal h(n);
        ros::spin();
        destroyWindow("thermal_raw");
}
