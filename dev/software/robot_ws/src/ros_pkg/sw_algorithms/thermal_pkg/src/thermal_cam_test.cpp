/*
* File : thermal_cam_test.cpp
* Author : Antonio Cusanelli 
* based on the tutorial : https://docs.opencv.org/2.4/doc/tutorials/imgproc/erosion_dilatation/erosion_dilatation.html
* 
*/
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect.hpp>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include "std_msgs/Float64MultiArray.h"

#define COL_ROW_SIZE 32
using namespace cv;

int erosion_elem = 0;
int erosion_size = 0;
int dilation_elem = 0;
int dilation_size = 0;
int const max_elem = 2;
int const max_kernel_size = 21;

/** Function Headers */
void Erosion( int, void* );
void Dilation( int, void* );

Mat src, erosion_dst, dilation_dst, element_erode, element_delate;
Mat mtx(COL_ROW_SIZE, COL_ROW_SIZE, CV_8UC1, Scalar::all(0));


// functions that create a pixel value from a temperature value
int f(int temperature){
	float tmp; 
        tmp = 1 + exp(-(temperature - 32)); //32 = const, represent mean temp of face skin surface
	return int(255/tmp); 
}

int f2(int temperature){
	if (temperature < 12 || temperature > 44)
		return 0;
	return int((temperature - 20) * 10);
}


void imageCallback(const sensor_msgs::ImageConstPtr &image) {

	cv_bridge::CvImagePtr cv_ptr;
	try {
 		cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::TYPE_8UC3);
	}catch(cv_bridge::Exception& e){
		ROS_ERROR("cv bridge ex : %s", e.what()); 
		return;
	}
	imshow("Image Window", cv_ptr->image);
	waitKey(3);
}

void tempCallback(const std_msgs::Float64MultiArray::ConstPtr& array)
{
        for(int i = 0; i < COL_ROW_SIZE; i++){
            for(int j = 0; j < COL_ROW_SIZE; j++){
		//mtx.at<uchar>(i,j) = f(array->data[j + i * 32]); //transform function 
		//mtx.at<uchar>(i,j) = f2(array->data[j + i * 32]);
                if(array->data[j + i * 32] > 33.0 && array->data[j + i * 32] < 42.0){
                    mtx.at<uchar>(i,j) = 255;
                }else if(array->data[j + i * 32] > 29.0 && array->data[j + i * 32] < 33.1){
                    mtx.at<uchar>(i,j) = 150;
                }else{
                    mtx.at<uchar>(i,j) = 0;
            	}
            }
        }
        
        resize(mtx, src, Size(), 7, 7);
        
        /// Apply the erosion operation
        erode( src, erosion_dst, element_erode );
        imshow( "Erosion Demo", erosion_dst );
        
        /// Apply the dilation operation
        dilate( erosion_dst, dilation_dst, element_delate );
        imshow( "Dilation Demo", dilation_dst );
        
        //imshow("Image Window", src);
        waitKey(3);
	return;
}

int main(int argc, char **argv) {
	namedWindow("Image Window");
    
        namedWindow( "Erosion Demo", CV_WINDOW_AUTOSIZE );
        namedWindow( "Dilation Demo", CV_WINDOW_AUTOSIZE );
        cvMoveWindow( "Dilation Demo", src.cols, 0 );

        /// Create Erosion Trackbar
        createTrackbar( "Element:\n 0: Rect \n 1: Cross \n 2: Ellipse", "Erosion Demo",
                        &erosion_elem, max_elem,
                        Erosion );

        createTrackbar( "Kernel size:\n 2n +1", "Erosion Demo",
                        &erosion_size, max_kernel_size,
                        Erosion );

        /// Create Dilation Trackbar
        createTrackbar( "Element:\n 0: Rect \n 1: Cross \n 2: Ellipse", "Dilation Demo",
                        &dilation_elem, max_elem,
                        Dilation );

        createTrackbar( "Kernel size:\n 2n +1", "Dilation Demo",
                        &dilation_size, max_kernel_size,
                        Dilation );

        /// Default start
        Erosion( 0, 0 );
        Dilation( 0, 0 );
        
	ros::init(argc, argv, "thermal_test_node");
	ros::NodeHandle n("~");
	ros::Subscriber sub = n.subscribe("/teraranger_evo_thermal/rgb_image", 10, imageCallback);
	ros::Subscriber sub2 = n.subscribe("/teraranger_evo_thermal/raw_temp_array", 10, tempCallback);
	ros::Rate loop_rate(50);
	while (ros::ok()) {
		ros::spinOnce();
		loop_rate.sleep();
	}
	destroyWindow("Image Window");
	destroyWindow("Erosion Demo");
	destroyWindow("Dilation Demo");
}

/**  @function Erosion  */
void Erosion( int, void* )
{
  int erosion_type;
  if( erosion_elem == 0 ){ erosion_type = MORPH_RECT; }
  else if( erosion_elem == 1 ){ erosion_type = MORPH_CROSS; }
  else if( erosion_elem == 2) { erosion_type = MORPH_ELLIPSE; }

  element_erode = getStructuringElement( erosion_type,
                                       Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                       Point( erosion_size, erosion_size ) );
}

/** @function Dilation */
void Dilation( int, void* )
{
  int dilation_type;
  if( dilation_elem == 0 ){ dilation_type = MORPH_RECT; }
  else if( dilation_elem == 1 ){ dilation_type = MORPH_CROSS; }
  else if( dilation_elem == 2) { dilation_type = MORPH_ELLIPSE; }

  element_delate = getStructuringElement( dilation_type,
                                       Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                       Point( dilation_size, dilation_size ) );
  
}
