/*
* File : thermal_show.cpp
* Author : Antonio Cusanelli 
* Brief: show thermal RGB image 
* 
*/
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect.hpp>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include "std_msgs/Float64MultiArray.h"

#define COL_ROW_SIZE 32

using namespace cv;

void imageCallback(const sensor_msgs::ImageConstPtr &image) {

	cv_bridge::CvImagePtr cv_ptr;
	try {
 		cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::TYPE_8UC3);
	}catch(cv_bridge::Exception& e){
		ROS_ERROR("cv bridge ex : %s", e.what()); 
		return;
	}
        imshow("Thermal Image", cv_ptr->image);
	waitKey(3);
}

int main(int argc, char **argv) {
        namedWindow("Thermal Image");
        
	ros::init(argc, argv, "thermal_show_node");
	ros::NodeHandle n("~");
	ros::Subscriber sub = n.subscribe("/teraranger_evo_thermal/rgb_image", 10, imageCallback);
	ros::Rate loop_rate(50);
	while (ros::ok()) {
		ros::spinOnce();
		loop_rate.sleep();
	}
        destroyWindow("Thermal Image");
}
