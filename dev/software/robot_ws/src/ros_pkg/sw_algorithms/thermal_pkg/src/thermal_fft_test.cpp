/*
* File : thermal_fft_test.cpp
* Author : Antonio Cusanelli 
* based on the tutorial : https://docs.opencv.org/2.4/doc/tutorials/imgproc/erosion_dilatation/erosion_dilatation.html
* 
* /!\/!\ this FFT method is not used, it is left here only for information, curiosity purpose /!\/!\
* 
*/
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <fstream> 
#include <cmath>
#include <complex>
#include <iomanip>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect.hpp>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include "std_msgs/Float64MultiArray.h"

#define COL_ROW_SIZE 32
using namespace cv;

int erosion_elem = 0;
int erosion_size = 0;
int dilation_elem = 0;
int dilation_size = 0;
int const max_elem = 2;
int const max_kernel_size = 21;
int prev_temp = 0; 

/** Function Headers */
void Erosion( int, void* );
void Dilation( int, void* );

std::vector<double> temperature;

Mat src, erosion_dst, dilation_dst, element_erode, element_delate;
Mat mtx(COL_ROW_SIZE, COL_ROW_SIZE, CV_8UC1, Scalar::all(0));


//FFT + hanning window:
//	reference : https://openclassrooms.com/forum/sujet/implementation-de-lalgorithme-fft-en-c
constexpr double PI = 3.141592653589793;
 
std::complex<double> w(int nk, int N) {
    return cos(2*PI*nk/N) - (std::complex<double>(0,1))*sin(2*PI*nk/N);
}
 
unsigned int reverseNum(unsigned int num, unsigned int pos) {
    unsigned int result = 0;
 
    for (unsigned int i(0); i < pos; i++) {
        if((num & (1 << i)))
           result |= 1 << ((pos - 1) - i);
    }
    return result;
}
 
void fft(std::vector<std::complex<double>>& signal, unsigned int end_, unsigned int start = 0) {
    if(end_ - start > 1) {
        unsigned int len = end_ - start;
 
        for(unsigned int i(0); i < len / 2; i++) {
            std::complex<double> temp(signal[start+i]);
            signal[start+i] += signal[start+i+(len/2)];
            signal[start+i+(len/2)] = (temp - signal[start+i+(len/2)])*w(i,len);
        }
 
        fft(signal,start+len/2,start);
        fft(signal,end_,start+len/2);
    }
    else if(end_ == signal.size()) {
        std::vector<std::complex<double>> temp(signal);
        unsigned int power(ceil(log(end_)/log(2)));
 
        for(unsigned int i(0); i < end_;i++) {
            signal[(i+end_/2)%end_] = temp[reverseNum(i,power)];
        }
    }
}


void imageCallback(const sensor_msgs::ImageConstPtr &image) {

	cv_bridge::CvImagePtr cv_ptr;
	try {
 		cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::TYPE_8UC3);
	}catch(cv_bridge::Exception& e){
		ROS_ERROR("cv bridge ex : %s", e.what()); 
		return;
	}
	imshow("Image Window", cv_ptr->image);
	waitKey(3);
}

int f(int temperature){
	float tmp; 
	tmp = 1 + exp(-(temperature - 32)); //32 = const, represent mean temp of face skin surface 
	return int(255/tmp); 
}

int f2(int temperature){
	if (temperature < 12 || temperature > 44)
		return 0;
	return int((temperature - 20) * 10);
}

void tempCallback(const std_msgs::Float64MultiArray::ConstPtr& array)
{
	int nb = 0; 
	float temp_mean = 0.0;       
        for(int i = 0; i < COL_ROW_SIZE; i++){
            for(int j = 0; j < COL_ROW_SIZE; j++){
                mtx.at<uchar>(i,j) = f(array->data[j + i * 32]);
		if(i >= 13 && i <= 17 && j >= 12 && j <= 18){
			mtx.at<uchar>(i,j) = 0;
			temp_mean += array->data[j + i * 32]; 
			nb++;
		}
            }
        }
	temp_mean /= nb;
	temperature.push_back(temp_mean); 
	
    
        resize(mtx, src, Size(), 7, 7);
        
        /// Apply the erosion operation
        erode( src, erosion_dst, element_erode );
        imshow( "Erosion Demo", erosion_dst );
        
        /// Apply the dilation operation
        dilate( erosion_dst, dilation_dst, element_delate );
        imshow( "Dilation Demo", dilation_dst );
        
        //imshow("Image Window", src);
        waitKey(3);
	return;
}

int main(int argc, char **argv) {
	namedWindow("Image Window");
    
        namedWindow( "Erosion Demo", CV_WINDOW_AUTOSIZE );
        namedWindow( "Dilation Demo", CV_WINDOW_AUTOSIZE );
        cvMoveWindow( "Dilation Demo", src.cols, 0 );
	/*
        /// Create Erosion Trackbar
        createTrackbar( "Element:\n 0: Rect \n 1: Cross \n 2: Ellipse", "Erosion Demo",
                        &erosion_elem, max_elem,
                        Erosion );

        createTrackbar( "Kernel size:\n 2n +1", "Erosion Demo",
                        &erosion_size, max_kernel_size,
                        Erosion );

        /// Create Dilation Trackbar
        createTrackbar( "Element:\n 0: Rect \n 1: Cross \n 2: Ellipse", "Dilation Demo",
                        &dilation_elem, max_elem,
                        Dilation );

        createTrackbar( "Kernel size:\n 2n +1", "Dilation Demo",
                        &dilation_size, max_kernel_size,
                        Dilation );
	*/ 
        /// Default start
        Erosion( 0, 0 );
        Dilation( 0, 0 );
        
	ros::init(argc, argv, "thermal_test_node_2");
	ros::NodeHandle n("~");
	ros::Subscriber sub = n.subscribe("/teraranger_evo_thermal/rgb_image", 10, imageCallback);
	ros::Subscriber sub2 = n.subscribe("/teraranger_evo_thermal/raw_temp_array", 10, tempCallback);
	ros::Rate loop_rate(50);
	while (ros::ok()) {
		ros::spinOnce();
		loop_rate.sleep();
	}
	
	std::vector<std::complex<double>> spectrum(temperature.size());
        std::ofstream myfile("~/temp_array.csv", std::ofstream::out);
        std::ofstream myfile_2("~/temp_fft_array.csv", std::ofstream::out);
	double temp_min = *std::min_element(temperature.begin(), temperature.end());
 	double temp_max = *std::min_element(temperature.begin(), temperature.end());
	for (int n=0; n<temperature.size(); n++)
	{
		double temp = temperature.at(n);
		if(n > 2 && n < temperature.size() - 3){
			temp = (temp + temperature.at(n-3) + temperature.at(n-2) + temperature.at(n-1) + temperature.at(n+1) + temperature.at(n+2) + temperature.at(n+3))/7; 
		}
		myfile << temp << ',';
		spectrum[n] = std::complex<double>(temp, 0.0);
	}
	myfile.close();
		

	fft(spectrum, spectrum.size()); 
	for (int n=0; n<spectrum.size(); n++)
	{
			myfile_2 << std::abs(spectrum.at(n)/std::complex<double>(spectrum.size(), 0.0)) << std::endl;
	}
	myfile_2.close();
 	
	destroyWindow("Image Window");
	destroyWindow("Erosion Demo");
	destroyWindow("Dilation Demo");
}

/**  @function Erosion  */
void Erosion( int, void* )
{
  int erosion_type;
  if( erosion_elem == 0 ){ erosion_type = MORPH_RECT; }
  else if( erosion_elem == 1 ){ erosion_type = MORPH_CROSS; }
  else if( erosion_elem == 2) { erosion_type = MORPH_ELLIPSE; }

  element_erode = getStructuringElement( erosion_type,
                                       Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                       Point( erosion_size, erosion_size ) );
}

/** @function Dilation */
void Dilation( int, void* )
{
  int dilation_type;
  if( dilation_elem == 0 ){ dilation_type = MORPH_RECT; }
  else if( dilation_elem == 1 ){ dilation_type = MORPH_CROSS; }
  else if( dilation_elem == 2) { dilation_type = MORPH_ELLIPSE; }

  element_delate = getStructuringElement( dilation_type,
                                       Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                       Point( dilation_size, dilation_size ) );
  
}
