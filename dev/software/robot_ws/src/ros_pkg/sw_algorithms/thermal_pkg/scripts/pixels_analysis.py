#!/usr/bin/env python
#Reference : https://plot.ly/matplotlib/fft/ 

import matplotlib.pyplot as plt
import numpy as np
import rospy, math, cv2, sys
from std_msgs.msg import Float64MultiArray
from scipy.signal import savgol_filter, butter, lfilter

temp_variation = np.zeros(350)
N = 0 

def annot_max(x,y, ax=None): 
    xmax = x[np.argmax(y)]
    ymax = y.max()
    text= "x={:.3f}, y={:.3f}".format(xmax, ymax)
    if not ax:
        ax=plt.gca()
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    arrowprops=dict(arrowstyle="->",connectionstyle="angle,angleA=0,angleB=60")
    kw = dict(xycoords='data',textcoords="axes fraction",
              arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
    ax.annotate(text, xy=(xmax, ymax), xytext=(0.94,0.96), **kw)

def breath_signal_det(y) : 
	sqi = 0
	Fs = 12.0  # sampling rate (10 hz) 
	Ts = 1.0/Fs # sampling interval
	 
	y = np.array(y, dtype=float) 


	n = len(y)
	t = np.arange(0,n*Ts,Ts) # time vector


	k = np.arange(n)
	T = n/Fs
	frq = k/T 
	frq = frq[range(n/2)]
	#y_shifted = np.fft.fftshift(y)
	y_ = y * np.hanning(n)  
	Y = np.fft.fft(y_)/n 
	Y = Y[range(n/2)]

	#limit the freq value near the breath rate (~0.25 Hz, 15 BPM) 
	Y_final = []
	frq_final = [] 
	i = 0

	for f in frq : 
		if ((f >= 0.167) and (f <= 0.667)):
			Y_final.append(Y[i]) 
			frq_final.append(frq[i])
		i = i + 1

	Y_final = np.array(Y_final)

	for el in abs(Y_final) : 
		sqi = sqi + el*el 

	sqi = abs(Y_final).max() / math.sqrt(sqi)
	print sqi
	
	fig, ax = plt.subplots(2, 1)
	ax[0].plot(t,y)
	ax[0].set_xlabel('Time (s)')
	ax[0].set_ylabel('Amplitude (Degrees Celsius)') 
	ax[1].plot(np.array([frq_el * 60 for frq_el in frq]), abs(Y),'r') # plotting the spectrum
	#ax[1].plot(np.array( [frq_el * 60 for frq_el in frq_final]), abs(Y_final),'r') # plotting the spectrum
	ax[1].set_xlabel('BPM (RR)')
	ax[1].set_ylabel('Magnitude : |Y(freq)|')
	annot_max(np.array( [frq_el * 60 for frq_el in frq_final]), abs(Y_final), ax[1])
	plt.show()
	

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y


def temp_callback(msg): 
	global N
	global temp_variation
	temp_variation[N] = msg.data[500] - np.mean(np.array(msg.data))
	N = N + 1
	if (N == 350) :
		I = 0 
		while I < len(temp_variation): 
			if (I <  (len(temp_variation) - 4)) : 
				temp_variation[I] = ( temp_variation[I] + temp_variation[I+1] + temp_variation[I+2] + temp_variation[I+3] ) / 4
			else : 
				temp_variation[I] = ( temp_variation[I] + temp_variation[I-1] + temp_variation[I-2] ) / 3
			I = I + 1 
	
		temp_variation2 = butter_bandpass_filter(temp_variation, 0.1, 0.8, 12) # bandpass filter : 10 -> 40 BPM 
		breath_signal_det(temp_variation2)
		N = 0

if __name__ == '__main__': 
	# In ROS, nodes are uniquely named. If two nodes with the same
	# name are launched, the previous one is kicked off. The
	# anonymous=True flag means that rospy will choose a unique
	# name for our 'listener' node so that multiple listeners can
	# run simultaneously.
	rospy.init_node('pixel_analysis', anonymous=True)

	rospy.Subscriber("/teraranger_evo_thermal/raw_temp_array", Float64MultiArray, temp_callback)

	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()
