#!/usr/bin/env python
# -*- coding: utf-8 -*-


#Method Papeer Reference : Lorato, I., Bakkes, T., Stuijk, S., Meftah, M., & de Haan, G. (2019). Unobtrusive respiratory flow monitoring using a thermopile array: a feasibility study. Applied Sciences, 9(12), [2449]. DOI: 10.3390/app9122449

#the equations used to compute SNR, signal quality, filtre, ... are shown on the report 

#Author : Antonio Cusanelli 
import matplotlib.pyplot as plt
import numpy as np
import rospy, math, cv2, sys, random
from std_msgs.msg import Float64MultiArray
from scipy.signal import butter, sosfilt 
import collections

 
NB_SAMPLE = 400 # circa 30s (13 Hz)
L_CUT = 0.1 #3 BPM 
H_CUT = 0.8 #54 BPM 
nb_pixel_couple = 1024 
current_sample = 0 

temp_array = None
temp_full = False 
is_free = True 


 
#reference : https://stackoverflow.com/questions/12093594/how-to-implement-band-pass-butterworth-filter-with-scipy-signal-butter 
#Accordind to scipy's doc : " (It’s recommended to use second-order sections (sos) format when filtering, to avoid numerical error with transfer function (ba) format) " 
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = butter(order, [low, high], btype='band', output='sos')
    return sos

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass(lowcut, highcut, fs, order=order)
    y = sosfilt(sos, data)
    return y


# show the max y-value on a plot, reference : https://stackoverflow.com/questions/43374920/how-to-automatically-annotate-maximum-value-in-pyplot
def annot_max(x,y, ax=None): 
    xmax = x[np.argmax(y)]
    ymax = y.max()
    text= "x={:.3f}, y={:.3f}".format(xmax, ymax)
    if not ax:
        ax=plt.gca()
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    arrowprops=dict(arrowstyle="->",connectionstyle="angle,angleA=0,angleB=60")
    kw = dict(xycoords='data',textcoords="axes fraction",
              arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
    ax.annotate(text, xy=(xmax, ymax), xytext=(0.94,0.96), **kw)

#Reference : https://plot.ly/matplotlib/fft/ 
def breath_signal_det(y) : 
	sqi = 0.0
	
	Fs = 14.0  # sampling rate (13-14 hz) 
	Ts = 1.0/Fs # sampling interval
	 
	y = np.array(y, dtype=float) 

	n = len(y)
	t = np.arange(0,n*Ts,Ts) # time vector


	k = np.arange(n)
	T = n/Fs
	frq = k/T 
	frq = frq[range(n/2)]
	y = y * np.hanning(n)  
	Y = np.fft.fft(y)/n #compute fft + normalize 
	Y = Y[range(n/2)]

	#limit the freq value near the breath rate (10 -> 40 BPM) 
	Y_final = []
	frq_final = [] 
	i = 0
 
	for f in frq : 
            if ((f >= 0.1) and (f <= 0.7)):
                Y_final.append(Y[i]) 
                frq_final.append(frq[i])
            i = i + 1

	Y_final = np.array(Y_final)
	
	for el in abs(Y_final) : 
            sqi = sqi + el**2 

	sqi = abs(Y_final).max() / math.sqrt(sqi)
	RR = frq_final[np.argmax(abs(Y_final))] * 60

	return sqi, frq, abs(Y), y, RR

def temp_callback(msg): 
    global is_free
    global current_sample
    global temp_array
    
    if is_free:
        temp = np.array(msg.data, dtype=float) 
        temp = temp.reshape((32,32))
        #spatial_mean = np.mean(temp) 

        j = 0 
        l = 0 
        while j < temp.shape[0]: 
            i = 0 
            while i < temp.shape[1] :
                temp_array[l].appendleft(temp[j][i] - spatial_mean)
                if temp_full:
                    temp_array[l].pop()
                
                l = l + 1 
                i = i + 1
            j = j + 1

        current_sample = current_sample + 1 


plt.ion()
fig, ax = plt.subplots(2, 1, figsize=(15, 7.5))


if __name__ == '__main__' : 

    #init the temp array 
    temp_array = [0] * nb_pixel_couple
    for i in range(nb_pixel_couple):
        temp_array[i] = collections.deque([]) 
    #temp_array = np.array(temp_array)
    
    # ROS init :

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('breath_det', anonymous=True)

    raw_input("press any key to begin the extraction")
    rospy.Subscriber("/teraranger_evo_thermal/raw_temp_array", Float64MultiArray, temp_callback)

    while not rospy.is_shutdown():
        if current_sample >= NB_SAMPLE :
            temp_full = True
                
        if temp_full and current_sample >= 20 : # update the plot every 3 seconds circa
            
            is_free = False 
            
            i = 0
            best_sqi = 0.0 
            best_freq = None 
            best_signal = None 
            best_magn = None
            best_group = 0 
            best_RR = 0.0 
            best_y = None

            #selection of the best pixel
            while i < len(temp_array) :
                temp_array_filtered = np.array(temp_array[i])
            
                j = 0 
                temporal_mean = np.mean(temp_array_filtered)
                while j < len(temp_array_filtered): 
                    temp_array_filtered[j] = temp_array_filtered[j] - temporal_mean				
                    j = j + 1 
                        
                temp_array_filtered = butter_bandpass_filter(temp_array_filtered, L_CUT, H_CUT, 14.0) # bandpass filter : 10 -> 40 BPM 
                sqi, freq, magn, y, RR = breath_signal_det(temp_array_filtered)

                #look for the best signal 
                if sqi > best_sqi : 
                    best_freq = freq 
                    best_sqi = sqi
                    best_magn = magn 
                    best_signal = np.copy(temp_array[i])
                    best_group = i 
                    best_RR = RR
                    best_y = y 
                i = i + 1 
                
            is_free = True
        
            print ('best group: ' + str(best_group))
            print ('best pixel : x=' + str(best_group % 32) + ', y=' + str(best_group / 32)) 
            print ('Respiratory Rate : ' + str(best_RR) + ' BPM')
            print ('quality : ' + str(best_sqi)) 
        
            
            #Show graphics 
            ax[0].clear()
            ax[1].clear()
            ax[0].title.set_text('filtered and Hanning windowed signal')
            ax[0].set_xlabel('frames')
            ax[0].set_ylabel('Amplitude (degrees Celsius)') 

            ax[1].title.set_text('FFT')
            ax[1].set_xlabel('BPM (RR)')
            ax[1].set_ylabel('Magnitude : |Y(best_freq)|')
            ax[0].plot(best_y)
            ax[1].plot(np.array([frq_el * 60 for frq_el in best_freq]), best_magn,'r')
            annot_max(np.array([frq_el * 60 for frq_el in best_freq]), best_magn, ax[1])
            
            fig.canvas.draw()
            fig.canvas.flush_events()

            #compute the SNR 
            periodogram = best_magn**2
            peak = best_RR / 60 #convert to Hz 
            bins = 1.0 / (NB_SAMPLE/14.0) 
            signal = 0.0 
            noise = 0.0 
            i = 0
            for f in best_freq : 
                if f >= 0.1 and f <= 1.7 : 
                    if f >= (peak - bins) and f <= (peak + bins) :
                        signal = signal + periodogram[i]
                    else : 
                        noise = noise + periodogram[i]
                elif f > 1.7 :
                    break

                i = i + 1 
                
            SNR = 10 * math.log10(signal/noise) 
            print 'SNR : ' + str(SNR)  + ' dB\n' 
            
            current_sample = 0 
                        




