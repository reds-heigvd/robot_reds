#!/usr/bin/env python
# -*- coding: utf-8 -*-


#Method Reference : Lorato I., Bakkes T., Stuijk S., Meftah M. & de Haan G. (2019). Unobtrusive respiratory flow monitoring using a thermopile array: a feasibility study. Applied Sciences, 9(12), [2449]. DOI: 10.3390/app9122449

#the equations used to compute SNR, signal quality, filtre, ... are shown on the report

#Author : Antonio Cusanelli
#Date: Dec, 2019

import matplotlib.pyplot as plt
import numpy as np
import rospy, math, cv2, sys, random
from std_msgs.msg import Float64MultiArray
from scipy.signal import butter, sosfilt
import actionlib
import thermal_pkg.msg
from multiprocessing import Lock

NB_SAMPLE = 400 # circa 30s (13 Hz)
NB_SAMPLE_UPDATE = 250 # 20S
FS = 13.5 #Hz
L_FRQ = 0.1 #6 BPM
H_FRQ = 0.8 #54 BPM

mutex = Lock()

class BreathAction(object):
    _feedback = thermal_pkg.msg.BreathFeedback()
    _result = thermal_pkg.msg.BreathResult()

    nb_pixel_couple = 1024
    current_sample = 0
    temp_array = None
    selected_pixel = None
    pixel_found = False
    selected_XY = []
    SQI_track = []
    SNR_track = []
    plot_y = None
    freq = None
    magn = None

    def __init__(self, name):
        self._action_name = name
        self._action_server = actionlib.SimpleActionServer(self._action_name,
            thermal_pkg.msg.BreathAction, execute_cb=self.execute_cb,
            auto_start=False)
        self._action_server.start()

    #reference : https://stackoverflow.com/questions/12093594/how-to-implement-band-pass-butterworth-filter-with-scipy-signal-butter
    #Accordind to scipy's doc : " (It’s recommended to use second-order sections (sos) format when filtering, to avoid numerical error with transfer function (ba) format) "
    def butter_bandpass(self, lowcut, highcut, fs, order=5):
        nyq = 0.5 * fs
        low = lowcut / nyq
        high = highcut / nyq
        sos = butter(order, [low, high], btype='band', output='sos')
        return sos

    def butter_bandpass_filter(self,data, lowcut, highcut, fs, order=5):
        sos = self.butter_bandpass(lowcut, highcut, fs, order=order)
        y = sosfilt(sos, data)
        return y


    # show the max yvalue on a plot, reference : https://stackoverflow.com/questions/43374920/how-to-automatically-annotate-maximum-value-in-pyplot
    def annot_max(self, x,y, ax=None):
        xmax = x[np.argmax(y)]
        ymax = y.max()
        text= "x={:.3f}, y={:.3f}".format(xmax, ymax)
        if not ax:
            ax=plt.gca()
        bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
        arrowprops=dict(arrowstyle="->",connectionstyle="angle,angleA=0,angleB=60")
        kw = dict(xycoords='data',textcoords="axes fraction",
            arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
        ax.annotate(text, xy=(xmax, ymax), xytext=(0.94,0.96), **kw)

    #Reference : https://plot.ly/matplotlib/fft/
    def breath_signal_det(self, y) :
        global FS
        global L_FRQ
        global H_FRQ

        sqi = 0.0

        y = np.array(y, dtype=float) #temperature signal

        n = len(y) # nb of frames
        k = np.arange(n)
        T = n/FS
        frq = k/T
        frq = frq[range(n/2)]

        y = y * np.hanning(n)
        Y = np.fft.fft(y)/n #compute fft + normalize
        Y = Y[range(n/2)]

        #limit the freq value near the breath rate (10 -> 40 BPM)
        Y_final = []
        frq_final = []
        i = 0

        #selection of the desired frequency interval
        for f in frq :
            if ((f >= L_FRQ) and (f <= H_FRQ)):
                Y_final.append(Y[i])
                frq_final.append(frq[i])
            i = i + 1

        Y_final = np.array(Y_final)

        #calculation of the score
        for el in abs(Y_final) :
            sqi = sqi + el**2

        sqi = abs(Y_final).max() / math.sqrt(sqi)
        # freq to Breath Per Minute (BPM)
        RR = frq_final[np.argmax(abs(Y_final))] * 60
        return sqi, frq, abs(Y), y, RR

    def temp_callback(self, msg):
        global FS
        global L_FRQ
        global H_FRQ
        global NB_SAMPLE

        temp = np.array(msg.data, dtype=float)
        temp = temp.reshape((32,32))
        spatial_mean = np.mean(temp)

        j = 0
        l = 0
        while j < temp.shape[0]:
            i = 0
            while i < temp.shape[1] :
                if not self.pixel_found :
                    self.temp_array[l][self.current_sample] = temp[j][i] - spatial_mean
                    l = l + 1
                else :
                    if j == self.selected_XY[1] and i == self.selected_XY[0] :

                        # for the next measurement : the selected pixel and its direct neighbours are taken (average)
                        temperature = 0.0
                        y = j - 1
                        nb = 0
                        while y < j + 2 :
                            x = i - 1
                            while x < i + 2 :
                                if x >= 0 and x <= 31 and y >= 0 and y <= 31 : #must be inside the matrix
                                    temperature = temperature + temp[y][x]
                                    nb = nb + 1
                                x = x + 1
                            y = y + 1
                        temperature = (temperature / nb) - spatial_mean

                        #temperature = temp[j][i] - spatial_mean
                        self.selected_pixel = np.insert(self.selected_pixel, self.selected_pixel.size, temperature)
                        self.selected_pixel = np.delete(self.selected_pixel, 0)
                i = i + 1
            j = j + 1


        self.current_sample = self.current_sample + 1

        if self.current_sample == NB_SAMPLE and not self.pixel_found :
            i = 0
            best_sqi = 0.0
            best_freq = None
            best_signal = None
            best_magn = None
            best_group = 0
            best_RR = 0.0
            self.temp_array = np.array(self.temp_array, dtype=float)

            while i < self.temp_array.shape[0] :
                temp_array_filtered = np.copy(self.temp_array[i])

                j = 0
                temporal_mean = np.mean(temp_array_filtered)
                while j < len(temp_array_filtered):
                    temp_array_filtered[j] = temp_array_filtered[j] - temporal_mean
                    j = j + 1


                temp_array_filtered = self.butter_bandpass_filter(temp_array_filtered, L_FRQ, H_FRQ, FS) # bandpass filter : 6 -> 54 BPM
                sqi, freq, magn, y, RR = self.breath_signal_det(temp_array_filtered)
                #look for the best signal
                if sqi > best_sqi :
                    best_freq = freq
                    best_sqi = sqi
                    best_magn = magn
                    best_signal = np.copy(self.temp_array[i])
                    best_group = i
                    best_RR = RR

                i = i + 1

            if best_sqi >= 0.5 :  #if signal's quality is good enough we select the corresponvx ding pixel
                self.pixel_found = True
                self.selected_pixel = np.array(best_signal)

                self.selected_XY.append(int(best_group % 32))
                self.selected_XY.append(int(best_group / 32))

                print ('\nbest pixel : x=' + str(self.selected_XY[0]) + ', y=' + str(self.selected_XY[1]))
                print ('Respiratory Rate : ' + str(best_RR) + ' BPM')
                print ('SQI : ' + str(best_sqi))
            else :
                print ('Signal is not good enough to compute an estimation, SQI : ' + str(best_sqi))

            self.current_sample = 0

    def execute_cb(self, goal):
        global mutex

        self.temp_array = [0] * self.nb_pixel_couple
        for i in range(self.nb_pixel_couple):
            self.temp_array[i] = [0] * NB_SAMPLE


        # Start
        rospy.Subscriber("/teraranger_evo_thermal/raw_temp_array", Float64MultiArray, self.temp_callback)
        while not rospy.is_shutdown():
            if self.pixel_found :
                if self.current_sample >= 40 : # update the plot every 3 seconds circa
                    temp_array_filtered = np.copy(self.selected_pixel[-NB_SAMPLE_UPDATE:]) #the last 20s of the signal
                    #filtre
                    j = 0
                    temporal_mean = np.mean(temp_array_filtered)
                    while j < temp_array_filtered.size :
                        temp_array_filtered[j] = temp_array_filtered[j] - temporal_mean
                        j = j + 1

                    temp_array_filtered = self.butter_bandpass_filter(temp_array_filtered, L_FRQ, H_FRQ, FS) # bandpass filter : 10 -> 40 BPM
                    with mutex:
                        sqi, self.freq, self.magn, self.plot_y, RR = self.breath_signal_det(temp_array_filtered)

                    print ('\nRespiratory Rate : ' + str(RR) + ' BPM')
                    print ('quality : ' + str(sqi))

                    #compute the SNR
                    periodogram = self.magn**2
                    peak = RR / 60 #BPM to Hz
                    bins = 1 / (NB_SAMPLE_UPDATE/FS) # bin = one interval between two frequency values of the FFT
                    signal = 0.0
                    noise = 0.0
                    i = 0
                    for f in self.freq :
                        if f >= 0.1 and f <= 1.7 :
                            if f >= (peak - bins) and f <= (peak + bins) :
                                signal = signal + periodogram[i]
                            else :
                                noise = noise + periodogram[i]
                        elif f > 1.7 :
                            break

                        i = i + 1
                    SNR = 10 * math.log10(signal/noise)
                    print ('SNR : ' + str(SNR)  + ' dB\n')

                    self.SQI_track.append(sqi)
                    self.SNR_track.append(SNR)

                    #check the signal quality, if not good enough we do a selection of a new pixel
                    if len(self.SQI_track) > 4 :
                        SQI_median = np.median(np.array(self.SQI_track))
                        SNR_median = np.median(np.array(self.SNR_track))
                        print ('\nSQI median : ' + str(SQI_median))
                        print ('SNR median : ' + str(SNR_median) + '\n')

                        if SQI_median < 0.6 and SNR_median < 3.0 : #launch of a new pixel selection
                            self.pixel_found = False
                            self.selected_XY = []

                        self.SQI_track = []
                        self.SNR_track = []

                    self.current_sample = 0

        self._result.success = True
        self._action_server.set_succeeded(self._result)
        #print self.current_sample #for debugging purpose


if __name__ == '__main__' :
    #init the temp array
    # ROS init
    plt.ion()
    fig, ax = plt.subplots(2, 1, figsize=(15, 7.5))

    rospy.init_node('breath_det', anonymous=True)
    server = BreathAction('Breath')
    while not rospy.is_shutdown():
        with mutex:
            freq = server.freq
            y = server.plot_y
            magn = server.magn
        #Show graphics
        if freq is not None:
            ax[0].clear()
            ax[1].clear()
            ax[0].title.set_text('filtered and Hanning windowed signal')
            ax[0].set_xlabel('frames')
            ax[0].set_ylabel('Amplitude (degrees Celsius)')
            ax[1].title.set_text('FFT')
            ax[1].set_xlabel('BPM (RR)')
            ax[1].set_ylabel('Magnitude : |Y(freq)|')
            ax[0].plot(y)
            ax[1].plot(np.array([frq_el * 60 for frq_el in freq]), magn,'r')
            server.annot_max(np.array([frq_el * 60 for frq_el in freq]), magn, ax[1])

            fig.canvas.draw()
            fig.canvas.flush_events()

        rospy.sleep(0.1)

