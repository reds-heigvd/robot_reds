#!/usr/bin/env python
# -*- coding: utf-8 -*-
# brief : program used to test the FFT method 
#Author : Antonio Cusanelli

import matplotlib.pyplot as plt
import numpy as np
import math, cv2, sys, random
from scipy.signal import butter, sosfilt


FS = 100.0 #Hz
L_FRQ = 0.1 #6 BPM
H_FRQ = 0.8 #54 BPM


#reference : https://stackoverflow.com/questions/12093594/how-to-implement-band-pass-butterworth-filter-with-scipy-signal-butter
#Accordind to scipy's doc : " (It’s recommended to use second-order sections (sos) format when filtering, to avoid numerical error with transfer function (ba) format) "
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = butter(order, [low, high], btype='band', output='sos')
    return sos

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass(lowcut, highcut, fs, order=order)
    y = sosfilt(sos, data)
    return y


#Reference : https://plot.ly/matplotlib/fft/
def breath_signal_det(y) :
    global FS
    global L_FRQ
    global H_FRQ
    
    sqi = 0.0

    y = np.array(y, dtype=float) #temperature signal

    n = len(y) # nb of frames
    k = np.arange(n)
    T = n/FS
    frq = k/T
    frq = frq[range(n/2)]
    
    y = y * np.hanning(n)
    Y = np.fft.fft(y)/n #compute fft + normalize
    Y = Y[range(n/2)]


    return sqi, frq, np.abs(Y), y


if __name__ == '__main__' :
    freq_sampling = 5 #Hz
    amplitude = 2 # amplitude of the sine wave
    time = np.arange(100) #np.linspace(0, 10, 500, endpoint=True) # time range with total samples of 500 from 0 to 6 with time interval equals 6/500
    temp_array_filtered = amplitude * np.sin(2 * np.pi * freq_sampling * (time/FS))
 
    sqi, freq, magn, y = breath_signal_det(temp_array_filtered)
    
    plt.figure(figsize=(10,6))
    plt.plot(freq, magn)
    #plt.plot(temp_array_filtered)
    plt.show()
    
                
