#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Reference : https://plot.ly/matplotlib/fft/ 
#Method Papeer Reference : Lorato, I., Bakkes, T., Stuijk, S., Meftah, M., & de Haan, G. (2019). Unobtrusive respiratory flow monitoring using a thermopile array: a feasibility study. Applied Sciences, 9(12), [2449]. DOI: 10.3390/app9122449
#Author : Antonio Cusanelli 
import matplotlib.pyplot as plt
import numpy as np
import rospy, math, cv2, sys
from std_msgs.msg import Float64MultiArray
from scipy.signal import savgol_filter, butter, lfilter

temp_array = None
temp_array2 = None 
NB_SAMPLE = 400 # circa 30s (13 Hz) 
nb_pixel_couple = 1024 
current_sample = 0 


#reference : https://stackoverflow.com/questions/12093594/how-to-implement-band-pass-butterworth-filter-with-scipy-signal-butter 
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

# show the max y-value, reference : https://stackoverflow.com/questions/43374920/how-to-automatically-annotate-maximum-value-in-pyplot
def annot_max(x,y, ax=None): 
    xmax = x[np.argmax(y)]
    ymax = y.max()
    text= "x={:.3f}, y={:.3f}".format(xmax, ymax)
    if not ax:
        ax=plt.gca()
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    arrowprops=dict(arrowstyle="->",connectionstyle="angle,angleA=0,angleB=60")
    kw = dict(xycoords='data',textcoords="axes fraction",
              arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
    ax.annotate(text, xy=(xmax, ymax), xytext=(0.94,0.96), **kw)

def breath_signal_det(y) : 
	sqi = 0.0
	
	Fs = 13.5  # sampling rate (12 hz) 
	Ts = 1.0/Fs # sampling interval
	 
	y = np.array(y, dtype=float) 


	n = len(y)
	t = np.arange(0,n*Ts,Ts) # time vector


	k = np.arange(n)
	T = n/Fs
	frq = k/T 
	frq = frq[range(n/2)]
	#y_shifted = np.fft.fftshift(y)
	#y = y * np.hanning(n)  
	Y = np.fft.rfft(y)/n 
	Y = Y[range(n/2)]

	#limit the freq value near the breath rate (~0.25 Hz, 15 BPM) 
	Y_final = []
	frq_final = [] 
	i = 0

	for f in frq : 
		if ((f >= 0.05) and (f <= 0.667)):
			Y_final.append(Y[i]) 
			frq_final.append(frq[i])
		i = i + 1

	Y_final = np.array(Y_final)
	
	for el in abs(Y_final) : 
		sqi = sqi + el*el 

	sqi = abs(Y_final).max() / math.sqrt(sqi)

	return sqi, frq_final, abs(Y_final), y

def temp_callback(msg): 
	global current_sample
	global NB_SAMPLE
	global nb_pixel_couple
	global temp_array
	global temp_array2
	temp = np.array(msg.data, dtype=float) 
	temp = temp.reshape((32,32))
	
	i = 0
	j = 0 
	l = 0 
	while j < temp.shape[0]: 
		i = 0 
		while i < temp.shape[1] : 
			"""
			tmp = [ temp[j][i], temp[j][i+1], temp[j][i+2], temp[j][i+3]  ,
				temp[j+1][i], temp[j+1][i+1], temp[j+1][i+2], temp[j+1][i+3],
				temp[j+2][i], temp[j+2][i+1], temp[j+2][i+2], temp[j+2][i+3], 
				temp[j+3][i], temp[j+3][i+1], temp[j+3][i+2], temp[j+3][i+3] ] 
			tmp = np.array(tmp, dtype=float) 
			temp_array[l][current_sample] = np.mean(tmp) - np.mean(temp) 

		 	i = i + 4 
			
			tmp = [ temp[j][i], temp[j][i+1], 
				temp[j+1][i], temp[j+1][i+1] ] 
			tmp = np.array(tmp, dtype=float) 
			temp_array[l][current_sample] = np.mean(tmp) - np.mean(temp) 

			i = i + 2
			"""
			spatial_mean = np.mean(temp) 
			temp_array[l][current_sample] = temp[j][i] - spatial_mean
			#temp_array2[l][current_sample] = temp[j][i] - spatial_mean
			i = i + 1
			l = l + 1 

		#j = j + 4
		#j = j + 2
		j = j + 1
	
	current_sample = current_sample + 1 
	
	if current_sample == NB_SAMPLE:
		"""		
		temp_array2 = np.delete(temp_array2, 0)
		print temp_array2.shape
		temp_array2 = np.insert(temp_array2, temp_array2.size, 2.0)
		temp_array2 = temp_array2.reshape((1024, 300))
		print temp_array2.shape
		"""
		i = 0
		best_sqi = 0.0 
		best_freq = None 
		best_signal = None 
		best_magn = None
		best_group = 0 
		temp_array = np.array(temp_array, dtype=float) 

		while i < temp_array.shape[0] :
			j = 0 
			temporal_mean = np.mean(temp_array[i])
			while j < len(temp_array[i]): 
				if (j <  (len(temp_array[i]) - 4)) : 
					temp_array[i][j] = ( temp_array[i][j] + temp_array[i][j+1] + temp_array[i][j+2] + temp_array[i][j+3] ) / 4 - temporal_mean
				else : 
					temp_array[i][j] = ( temp_array[i][j] + temp_array[i][j-1] + temp_array[i][j-2] ) / 3 - temporal_mean
				j = j + 1 

			temp_array[i] = butter_bandpass_filter(temp_array[i], 0.15, 0.8, 13.5) # bandpass filter : 10 -> 40 BPM 
			sqi, freq, magn, y = breath_signal_det(temp_array[i])
			if sqi > best_sqi : 
				best_freq = freq 
				best_sqi = sqi
				best_magn = magn 
				best_signal = y
				best_group = i 
				
			i = i + 1 
		print best_sqi
		if best_sqi >= 0.75 : 
			fig, ax = plt.subplots(2, 1)
			ax[0].plot(best_signal)
			ax[0].set_xlabel('frames')
			ax[0].set_ylabel('Amplitude (degrees Celsius)') 
			ax[1].plot(np.array([frq_el * 60 for frq_el in best_freq]), best_magn,'r') # plotting the spectrum
			ax[1].set_xlabel('BPM (RR)')
			ax[1].set_ylabel('Magnitude : |Y(freq)|')
			annot_max(np.array([frq_el * 60 for frq_el in best_freq]), best_magn, ax[1])
			plt.show()
			print ('best pixel : x=' + str(int(best_group % 32)) + ', y=' + str(int(best_group / 32))) 
			print ('Respiratory Rate : ' + str(best_freq[np.argmax(best_magn)] * 60) + ' BPM')
			
		current_sample = 0 

	#print current_sample #for debugging purpose 

if __name__ == '__main__' : 

	
	temp_array = [0] * nb_pixel_couple
	for i in range(nb_pixel_couple):
	    temp_array[i] = [0] * NB_SAMPLE

	temp_array2 = np.zeros((nb_pixel_couple, NB_SAMPLE)) 

	# In ROS, nodes are uniquely named. If two nodes with the same
	# name are launched, the previous one is kicked off. The
	# anonymous=True flag means that rospy will choose a unique
	# name for our 'listener' node so that multiple listeners can
	# run simultaneously.
	rospy.init_node('breath_det', anonymous=True)

	rospy.Subscriber("/teraranger_evo_thermal/raw_temp_array", Float64MultiArray, temp_callback)

	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()
