#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <ros/ros.h>
#include <std_msgs/String.h>

extern "C"{
    #include "i2c.h"
}

#include <sstream>
#include "reds_sensor_board/Sensor_data.h"

// defines
#define BUS_ADDR        0x08
#define IADDR_BYTES     1
#define PAGE_BYTES      16

#define TRA_SIZE        4
#define FREQ            10
#define SPEED_INT_VAL   0xE8030000  // ticks per sec

// Board addresses
#define VERSION_REG     0x0
#define TEST_REG        0x1
#define COMMAND_REG     0x2
#define STATUS_REG      0x3
#define FL_WPOS_REG     0x4
#define FR_WPOS_REG     0x5
#define BL_WPOS_REG     0x6
#define BR_WPOS_REG     0x7
#define FL_WSPEED_REG   0x8
#define FR_WSPEED_REG   0x9
#define BL_WSPEED_REG   0xA
#define BR_WSPEED_REG   0xB
#define SPEED_INT_REG   0xC

// commands
#define PICTURE_CMD     0x00FF0000
#define RESET_CMD       0xFF000000


#define SwapFourBytes(data)   \
( (((data) >> 24) & 0x000000FF) | (((data) >>  8) & 0x0000FF00) | \
  (((data) <<  8) & 0x00FF0000) | (((data) << 24) & 0xFF000000) )


// define structures
typedef struct _enc_pos
{
    int front_left;
    int front_right;
    int back_left;
    int back_right;
} Enc_pos;

typedef struct _enc_speed
{
    int front_left;
    int front_right;
    int back_left;
    int back_right;
} Enc_speed;

typedef struct _board_data
{
    int board_version;
    Enc_pos *enc_pos;
    Enc_speed *enc_speed;
} Board_data;


int ret = 0; // check value
const std::string I2C_BUS = "/dev/i2c-1"; // i2c bus name

// prototypes
int init(I2CDevice *i2c_device, Board_data *board_data, std::string i2c_bus_name);
int init_board(I2CDevice *i2c_device);
int reset_board(I2CDevice *i2c_device);
void commands_callback(I2CDevice *i2c_device, const std_msgs::String::ConstPtr& msg);


int main(int argc, char **argv)
{
    I2CDevice *i2c_device; // struct for i2c communication
    ros::init(argc, argv, "sensor_board_node");
    ros::NodeHandle n;
    int buf;
    int right_enc;
    int left_enc;
    int left_speed;
    int right_speed;
    char buffer[100];
    Board_data *board_data;

    // init publishers
    ros::Publisher sensor_data_pub = n.advertise<reds_sensor_board::Sensor_data>("sensor_board/sensor_datas", 1000);

    // init subscribers
    //ros::Subscriber sub = n.subscribe("sensor_board/commands", 1000, commands_callback);

    ros::Rate loop_rate(FREQ);

    i2c_device = (I2CDevice*)calloc(1, sizeof(*i2c_device));
    board_data = (Board_data *) calloc (1, sizeof(*board_data));
    board_data->enc_pos = (Enc_pos *) calloc (1, sizeof(board_data->enc_pos));
    board_data->enc_speed = (Enc_speed *) calloc (1, sizeof(board_data->enc_speed));

    // Init strucutre for i2c communication
    if (init(i2c_device, board_data, I2C_BUS) != 0) {
        ROS_FATAL ("Init failed !");
    }

    // Init the sensor board
    if (reset_board(i2c_device) != 0) {
        ROS_FATAL ("Reset of the board failed !");
    }

    int count = 0;
    int publish;

    while (ros::ok())
    {
        publish = 1;
        //create custom message

        // reds_sensor_board::Sensor_data msg;
        reds_sensor_board::Sensor_data msg;

        // "picture" of the actual values (speed and position)
        //buf = 0xFFFF00;
        buf = PICTURE_CMD;

        if (i2c_ioctl_write(i2c_device, COMMAND_REG, &buf, TRA_SIZE) == -1) {
            ROS_FATAL ("Can't take picture...");
            publish = 0;
            //return -1;
        }

        // // reading position value of front left encoder
        // if (i2c_ioctl_read(i2c_device, FL_WPOS_REG, &buf, TRA_SIZE) != 4) {
        //     ROS_FATAL ("Problem reading position of front left encoder...");
        //     publish = 0;
        // } else {
        //   buf = SwapFourBytes(buf);
        //
        //   if(abs(abs(board_data->enc_pos->front_right) - abs(buf)) < 500){
        //     board_data->enc_pos->front_right = -buf;
        //   }
        // }
        //
        // // reading position value of front right encoder
        // if (i2c_ioctl_read(i2c_device, FR_WPOS_REG, &buf, TRA_SIZE) != 4) {
        //     ROS_FATAL ("Problem reading position of front right encoder...");
        //     publish = 0;
        // } else {
        //   buf = SwapFourBytes(buf);
        //   if(abs(abs(board_data->enc_pos->front_left) - abs(abs(buf))) < 500) {
        //     board_data->enc_pos->front_left = buf;
        //   }
        // }
        // // reading speed value of front left encoder
        // if (i2c_ioctl_read(i2c_device, FL_WSPEED_REG, &buf, TRA_SIZE) != 4) {
        //     ROS_FATAL ("Problem reading position of front left speed...");
        //     publish = 0;
        // } else {
        //     board_data->enc_speed->front_left = -SwapFourBytes(buf);
        // }
        //
        // // reading speed value of front right encoder
        // if (i2c_ioctl_read(i2c_device, FR_WSPEED_REG, &buf, TRA_SIZE) != 4) {
        //     ROS_FATAL ("Problem reading position of front right speed...");
        //     publish = 0;
        // } else {
        //     board_data->enc_speed->front_right = SwapFourBytes(buf);
        // }

        // ROS_INFO("ENC POS LEFT = %d", board_data->enc_pos->front_left);
        // ROS_INFO("ENC POS RIGHT = %d\n\n", board_data->enc_pos->front_right);

        // Reading front left and front right
        if (i2c_ioctl_read(i2c_device, FL_WPOS_REG, buffer, 32) != 32) {
            ROS_FATAL ("Problem reading the informations");
            publish = 0;
        } else {
            board_data->enc_pos->front_right   =
                    ((buffer[0] << 24) & 0xFF000000) | ((buffer[1] << 16) & 0x00FF0000) |
                    ((buffer[2] <<  8) & 0x0000FF00) | ((buffer[3] <<  0) & 0x000000FF);
            board_data->enc_pos->front_left    =
                    ((buffer[4] << 24) & 0xFF000000) | ((buffer[5] << 16) & 0x00FF0000) |
                    ((buffer[6] <<  8) & 0x0000FF00) | ((buffer[7] <<  0) & 0x000000FF);
            board_data->enc_speed->front_right =
                    ((buffer[16] << 24) & 0xFF000000) | ((buffer[17] << 16) & 0x00FF0000) |
                    ((buffer[18] <<  8) & 0x0000FF00) | ((buffer[19] <<  0) & 0x000000FF);
            board_data->enc_speed->front_left  =
                    ((buffer[20] << 24) & 0xFF000000) | ((buffer[21] << 16) & 0x00FF0000) |
                    ((buffer[22] <<  8) & 0x0000FF00) | ((buffer[23] <<  0) & 0x000000FF);

            // ROS_INFO("left  = %d  | right  = %d", -left_enc, right_enc);
            // ROS_INFO("sleft = %d  | sright = %d", -left_speed, right_speed);
        }


        if(publish) {
            msg.pos = {-board_data->enc_pos->front_right, board_data->enc_pos->front_left, 0, 0};
            msg.vel = {-board_data->enc_speed->front_right, board_data->enc_speed->front_left, 0, 0};
            // publishing information
            sensor_data_pub.publish(msg);
        }

        ros::spinOnce();

        loop_rate.sleep();
    }

    free(board_data);
    i2c_close(i2c_device->bus);

    return 0;
}


/*
**	@brief	      : init the structure used for the i2c communication
**	#i2c_bus_name :	string of i2c bus used
**	@return       : success return 0, failed -1
*/
int init(I2CDevice *i2c_device, Board_data *board_data, std::string i2c_bus_name)
{
    int bus;
    int buf;

    if ((bus = i2c_open(i2c_bus_name.c_str())) == -1) {
        ROS_FATAL ("Failed to open I2C bus %s", i2c_bus_name.c_str());
        return -1;
    }

    // set the i2c_devices values
    i2c_device->bus = bus;
    i2c_init_device(i2c_device);
    i2c_device->addr = BUS_ADDR;
    i2c_device->tenbit = 0;
    i2c_device->delay = 1;


    // init to 0 all values of encoders
    board_data->enc_pos->front_left  = 0;
    board_data->enc_pos->front_right = 0;
    board_data->enc_pos->back_left   = 0;
    board_data->enc_pos->back_right  = 0;

    board_data->enc_speed->front_left  = 0;
    board_data->enc_speed->front_right = 0;
    board_data->enc_speed->back_left   = 0;
    board_data->enc_speed->back_right  = 0;


    if (i2c_ioctl_read(i2c_device, VERSION_REG, &buf, TRA_SIZE) != 4) {
        ROS_FATAL ("Problem reading the version of the board...");
        return -1;
    }
    board_data->board_version = buf;

    return 0;
}

/*
**	@brief	      : resets all the registers of the sensor board
**	@return       : success return 0, failed -1
*/
int reset_board(I2CDevice *i2c_device)
{
    int buf;

    buf = RESET_CMD;
    // resets all values of encoders (speed and position)
    if (i2c_ioctl_write(i2c_device, COMMAND_REG, &buf, TRA_SIZE) == -1) {
        return -1;
    }

    buf = SPEED_INT_VAL;
    // "picture" of the actual values (speed and position)
    if (i2c_ioctl_write(i2c_device, SPEED_INT_REG, &buf, TRA_SIZE) == -1) {
        return -1;
    }

    buf = PICTURE_CMD;
    // "picture" of the actual values (speed and position)
    if (i2c_ioctl_write(i2c_device, COMMAND_REG, &buf, TRA_SIZE) == -1) {
        return -1;
    }

    buf = 0x0;
    // resets the test register (tests purposes)
    if (i2c_ioctl_write(i2c_device, TEST_REG, &buf, TRA_SIZE) == -1) {
        return -1;
    }

    return 0;
}


/*
**	@brief	      : resets all the registers of the sensor board
**	@return       : success return 0, failed -1
*/
void commands_callback(I2CDevice *i2c_device, const std_msgs::String::ConstPtr& msg)
{
    int *buf;
    ROS_INFO("Command sent : [%s]", msg->data.c_str());
    if (msg->data.compare("reset")) {
        if(reset_board(i2c_device) != 0) {
            ROS_FATAL ("Reset of the board failed !");
        }
    } else if (msg->data.compare("picture")) {
        *buf = 0xFF00;
        if (i2c_write(i2c_device, COMMAND_REG, buf, TRA_SIZE) != 0) {
            ROS_FATAL ("Picture failed !");
        }
    }
}
