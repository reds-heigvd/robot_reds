#ifndef AGRIBOT_HW
#define AGRIBOT_HW

#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include "reds_sensor_board/Sensor_data.h"

#include <wiringPi.h>

#define NB_WHEELS       2
#define WHEELS_RADIUS   0.065
#define MIN_MOT_VEL     0.071
#define MULT_RATIO      1960.784313725
#define PWM_ADAPT       200

#define LEFT_GPIO       22
#define RIGHT_GPIO      21



class Agribot_hw : public hardware_interface::RobotHW
{
public:
    Agribot_hw();
    static void agribot_read_callback(const reds_sensor_board::Sensor_data::ConstPtr &msg);
    void agribot_write();
    void agribot_read();
    int get_rate();

    ros::Time get_time();
    ros::Duration get_period();

private:
    ros::NodeHandle n;

    hardware_interface::JointStateInterface jnt_state_interface;
    hardware_interface::VelocityJointInterface jnt_vel_interface;
    double cmd[NB_WHEELS];
    double pos[NB_WHEELS];
    double vel[NB_WHEELS];
    double eff[NB_WHEELS];
    static reds_sensor_board::Sensor_data encoders_values;
    double wheels_radius;
    double min_motor_vel;
    int fd_gpio29;
    int fd_gpio31;
    std::string left_fw_dir;
    std::string left_bw_dir;
    std::string right_fw_dir;
    std::string right_bw_dir;
    int prev_pwm_left;
    int prev_pwm_right;
    ros::Publisher motors_pub;
    ros::Subscriber encoders_ret;
    int rate;
};


#endif /* INCLUDE_AGRIBOT_HW */
