#define PI            3.1415926
#define ENCODER_MAX   1288.848
#define ENCODER_MAX_D   644.424


#include <agribot_control/agribot_hw.h>

#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"

#include "std_msgs/Int32MultiArray.h"
#include <stdlib.h>

reds_sensor_board::Sensor_data Agribot_hw::encoders_values;

Agribot_hw::Agribot_hw()
{
    motors_pub = n.advertise<std_msgs::Int32MultiArray>("/command", 10);
    encoders_ret = n.subscribe("sensor_board/sensor_datas", 10, agribot_read_callback);

    for(int i = 0; i < NB_WHEELS; i++) {
        cmd[i] = pos[i] = vel[i] = eff[i] = 0;
    }

    // connect and register the joint state interface
    hardware_interface::JointStateHandle state_handle_a("base_to_right_wheel_fw", &pos[0], &vel[0], &eff[0]);
    jnt_state_interface.registerHandle(state_handle_a);

    hardware_interface::JointStateHandle state_handle_b("base_to_left_wheel_fw", &pos[1], &vel[1], &eff[1]);
    jnt_state_interface.registerHandle(state_handle_b);

    registerInterface(&jnt_state_interface);

    // connect and register the joint velocity interface
    hardware_interface::JointHandle vel_handle_a(jnt_state_interface.getHandle("base_to_right_wheel_fw"), &cmd[0]);
    jnt_vel_interface.registerHandle(vel_handle_a);

    hardware_interface::JointHandle vel_handle_b(jnt_state_interface.getHandle("base_to_left_wheel_fw"), &cmd[1]);
    jnt_vel_interface.registerHandle(vel_handle_b);

    registerInterface(&jnt_vel_interface);

    wheels_radius = WHEELS_RADIUS;
    min_motor_vel = MIN_MOT_VEL;

    rate = 50;

     wiringPiSetup () ;
     pinMode(LEFT_GPIO, OUTPUT);
     pinMode(RIGHT_GPIO, OUTPUT);
     digitalWrite(LEFT_GPIO, HIGH);
     digitalWrite(RIGHT_GPIO, LOW);
}



void Agribot_hw::agribot_read_callback(const reds_sensor_board::Sensor_data::ConstPtr &msg)
{
    encoders_values.pos = msg->pos;
    encoders_values.vel = msg->vel;
    // pos[0] = msg->pos[0];
    // pos[1] = msg->pos[1];
    // vel[0] = msg->vel[0];
    // vel[1] = msg->vel[1];
}

void Agribot_hw::agribot_write()
{
    int pwm_sent_left;
    int pwm_sent_right;
    int gpio_left;
    int gpio_right;
    double tmp_vel;
    std_msgs::Int32MultiArray motors;
    motors.data.clear();

    gpio_right = cmd[0] >= 0 ? 1 : -1;
    tmp_vel = abs(cmd[0]);
    pwm_sent_right = tmp_vel < min_motor_vel ?
            0 : ((((tmp_vel * wheels_radius) - min_motor_vel) * MULT_RATIO) + PWM_ADAPT) * gpio_right;

    gpio_left = cmd[1] >= 0 ? 1 : -1;
    tmp_vel = abs(cmd[1]);
    pwm_sent_left = tmp_vel < min_motor_vel ?
            0 : ((((tmp_vel * wheels_radius) - min_motor_vel) * MULT_RATIO) + PWM_ADAPT) * gpio_left;


    if(!((pwm_sent_right >= 0) ^ (prev_pwm_right < 0))) {
        if(pwm_sent_right < 0) {
            digitalWrite(RIGHT_GPIO, HIGH);
        } else {
            digitalWrite(RIGHT_GPIO, LOW);
        }
    }

    if(!((pwm_sent_left >= 0) ^ (prev_pwm_left < 0))) {
        if(pwm_sent_left < 0) {
            digitalWrite(LEFT_GPIO, LOW);
        } else {
            digitalWrite(LEFT_GPIO, HIGH);
        }
    }

    //ROS_INFO("left : %5d   |   right : %5d\n", pwm_sent_left, pwm_sent_right);
    motors.data.push_back(abs(pwm_sent_right * 2 * 2 * 2 * 2));
    motors.data.push_back(abs(pwm_sent_left * 2 * 2 * 2 * 2));
    motors.data.push_back(abs(pwm_sent_right * 2 * 2 * 2 * 2));
    motors.data.push_back(abs(pwm_sent_left * 2 * 2 * 2 * 2));

    // Need array of 16 to send, -1 won't compute the information
    // Not really elegant, this can be changed to something more scalable
    // TODO : Change the loop system
    for(int i = 0; i < 12; i++) {
      motors.data.push_back(-1);
    }

    motors_pub.publish(motors);

    prev_pwm_left = pwm_sent_left;
    prev_pwm_right = pwm_sent_right;

}

void Agribot_hw::agribot_read()
{
    pos[0] = (encoders_values.pos[0] * PI) / ENCODER_MAX_D;
    pos[1] = (encoders_values.pos[1] * PI) / ENCODER_MAX_D;
    vel[0] = (encoders_values.vel[0] * PI) / ENCODER_MAX_D;
    vel[1] = (encoders_values.vel[1] * PI) / ENCODER_MAX_D;
    //ROS_INFO("left : %5f   |   right : %5f", vel[0], vel[1]);

}


ros::Time Agribot_hw::get_time() {
    return ros::Time::now();
}

ros::Duration Agribot_hw::get_period() {
    return ros::Duration(1./50);
}

int Agribot_hw::get_rate() {
    return rate; 
}
