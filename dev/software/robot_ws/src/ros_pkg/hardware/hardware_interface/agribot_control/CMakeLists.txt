cmake_minimum_required(VERSION 3.0.2)
project(agribot_control)

## Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  hardware_interface
  controller_manager
  controller_manager_msgs
  urdf
  reds_sensor_board
  i2cpwm_board
)

## System dependencies are found with CMake's conventions
 find_package(COMPONENTS)

catkin_package(
  INCLUDE_DIRS include
  LIBRARIES agribot_control
  CATKIN_DEPENDS roscpp rospy
  DEPENDS system_lib
  controller_manager
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ library
 add_library(agribot_hw
   src/agribot_hw.cpp
 )

#add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

add_executable(agribot_hw_control_node src/agribot_hw_control.cpp)

## Rename C++ executable without prefix
## The above recommended prefix causes long target names, the following renames the
## target back to the shorter version for ease of user use
## e.g. "rosrun someones_pkg node" instead of "rosrun someones_pkg someones_pkg_node"
# set_target_properties(${PROJECT_NAME}_node PROPERTIES OUTPUT_NAME node PREFIX "")

## Add cmake target dependencies of the executable
## same as for the library above
# add_dependencies(${PROJECT_NAME}_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
 target_link_libraries(agribot_hw_control_node
   ${catkin_LIBRARIES}
   agribot_hw
   -lwiringPi
 )
