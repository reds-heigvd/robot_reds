#include <agribot_control/agribot_hw.h>
#include <controller_manager/controller_manager.h>
#include <ros/ros.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "mobile_base_1 Hardware Interface");

    Agribot_hw robot;

    controller_manager::ControllerManager cm(&robot);

    ros::Rate loop_rate(robot.get_rate());
    // NOTE: We run the ROS loop in a separate thread as external calls such
    // as service callbacks to load controllers can block the (main) control loop
    ros::AsyncSpinner spinner(3);
    spinner.start();

    while (ros::ok())
    {
        robot.agribot_read();
        cm.update(robot.get_time(), robot.get_period());
        robot.agribot_write();
        //ROS_INFO("%s", "running mamen");
        loop_rate.sleep();
    }
}
