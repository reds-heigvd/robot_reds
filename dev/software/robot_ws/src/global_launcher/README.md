Comporte les launchers des différents robots du REDS.

Ces fichiers launchs sont appelés par le fichier launch à la racine du projet :

```bash
../robot_reds/dev/robot.launch
```

Ce fichier launch récupère des variables d'environnement pour déterminer quel système il faut lancer.

- ROBOT_MODEL
- <nom_du_robot>_MODE

Les fichiers launchs comportent une documentation expliquant quels sont les modes possibles lorsqu'un robot est lancé avec un modèle donné. 

Si le mode n'est pas correct ou inexistant, un message d'erreur apparaît lors du lancement et décris comment mettre en place les variables d'environnement et quels sont les modes possibles pour un robot.

Une explication des différents modèles de robots existants et des modes possibles pour chaque robot se trouve à l'endroit du fichier "robot.launch"  précédemment évoqué.