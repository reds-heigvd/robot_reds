

# Useful document for a fresh install



## Packages to add for a fresh install

```bash
sudo apt install libsdl-image1.2-dev libsdl-dev ros-noetic-move-base-msgs ros-noetic-openslam-gmapping ros-noetic-tf2-sensor-msgs ros-noetic-geographic-msgs libi2c-dev
```



## WiringPi

To use WiringPi package, you need to clone it from the repo

```
git clone https://github.com/WiringPi/WiringPi.git
cd WiringPi
git pull origin
./build
```



## Services : 

#### Start roscore at boot : 

Create a service (filename.service) in  /etc/systemd/system/

```bash
[Unit]
Description=Lance le roscore du systeme
After=rbt_init.service

[Service]
Type=simple
ExecStart=/bin/bash /home/ubuntu/scripts/roscore_launch
User=ubuntu
Group=ubuntu

[Install]
WantedBy=multi-user.target

```



#### Execute init script (permissions for tty and more) : 

Create a service (filename.service) in  /etc/systemd/system/

```bash

[Unit]
Description=Initialisation des systèmes pour le robot
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
ExecStart=/home/ubuntu/scripts/init_system
User=ubuntu
Group=ubuntu

[Install]
WantedBy=multi-user.target
```



## Bluetooth config (DS4 controller)

Modify or create a file : /etc/rc.local (with execution permissions) and fill it with : 

```bash
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

hciattach /dev/ttyAMA0 bcm43xx 921600

exit 0
```

