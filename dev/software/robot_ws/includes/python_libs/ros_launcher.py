#!/usr/bin/python
import rospy
import roslaunch

class Ros_launcher():
    def __init__(self, path):
        self.path = path
        self.uuid = None
        self.launch = None
        self.running = False

    def start(self):
        if(not self.running):
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(self.uuid)
            self.launch = roslaunch.parent.ROSLaunchParent(self.uuid, [self.path])
            self.launch.start()
            self.running = True

    def shutdown(self):
        if(self.running):
            self.launch.shutdown()
            self.running = False
