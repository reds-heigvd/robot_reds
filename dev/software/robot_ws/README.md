Racine des sources du projet "robot_reds".



## Comment lancer un robot dans un mode spécifique (avec détails) ?

Pour lancer un robot spécifique il faut impérativement mettre en place des variables d'environnement spécifiques permettant de décrire : 

#### 1. Le modèle du robot

Le modèle correspond en général au nom du robot développé (par exemple "Agribot", "Helpie"...).

Pour mettre en place cette variable d'environnement : 

``` bash
export ROBOT_MODEL="<your_model>"
```



#### 2. Le mode de fonctionnement du robot

Différents modes sont disponibles pour tous les robots, et ces modes ne sont pas identiques d'uLe robot navigue vers un point n robot à l'autre.

La convention pour définir le mode d'un robot est d'exporter une variable d'environnement qui se compose de la mnière suivante : 

<ROBOT_MODEL>_<MODE_NAME>="<mode>"

Par exemple pour que le mode création de map manuelle d'agribot avec l'algorithme rf20 soit lancé : 

```bash
export AGRIBOT_MODE="mapping_rf20"
```



## Chemin des fichiers launch pour chaque robot

Les fichiers launch appelés se trouvent dans : 

```bash
../robot_reds/dev/src/robot/<modele_du_robot>/launch/
```

Chaque fichier launch correspond à un mode de fonctionnement. Amérioration possible : Segmenter les fichiers launch pour éviter les répétitions. Par exemple un fichier launch général avec les nodes basiques pour un robot va appeler d'autres fichiers launch ne démarrant que les nodes spécifiques au mode.



## Liste et description des modes pour chaque robot : 

#### Agribot

Exécuter cette commande dans le dossier ~/robot_reds/dev/software/robot_ws : 

```bash
 catkin_make -DCATKIN_WHITELIST_PACKAGES="reds_sensor_board;diff_dirve;gmapping;joint_state_publisher;robot_state_publisher;agribot"
```





#### Helpie 

TODO









