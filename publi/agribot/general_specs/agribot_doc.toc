\contentsline {section}{\numberline {1}Introduction}{4}{section.1}%
\contentsline {subsection}{\numberline {1.1}Cadre du projet}{4}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Analyse de la problématique}{4}{subsection.1.2}%
\contentsline {section}{\numberline {2}Structure hardware}{5}{section.2}%
\contentsline {subsection}{\numberline {2.1}Composants mécanique}{5}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Chassis et roues}{5}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}Moteurs}{7}{subsubsection.2.1.2}%
\contentsline {subsection}{\numberline {2.2}Alimentation du système}{8}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Batterie moteurs}{8}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Batterie système}{8}{subsubsection.2.2.2}%
\contentsline {subsection}{\numberline {2.3}Composants électroniques}{9}{subsection.2.3}%
\contentsline {subsubsection}{\numberline {2.3.1}Drivers moteurs - Cytron}{9}{subsubsection.2.3.1}%
\contentsline {subsubsection}{\numberline {2.3.2}i2cPWM board - ServoShield}{10}{subsubsection.2.3.2}%
\contentsline {subsubsection}{\numberline {2.3.3}REDS sensor board}{11}{subsubsection.2.3.3}%
\contentsline {subsubsection}{\numberline {2.3.4}Raspberry Pi 4}{12}{subsubsection.2.3.4}%
\contentsline {subsection}{\numberline {2.4}Capteurs}{13}{subsection.2.4}%
\contentsline {subsubsection}{\numberline {2.4.1}Scanner laser 360° - RPLidar A1}{13}{subsubsection.2.4.1}%
\contentsline {subsubsection}{\numberline {2.4.2}GNSS IMU - Xsens MTI 680G}{14}{subsubsection.2.4.2}%
\contentsline {subsubsection}{\numberline {2.4.3}Camera - T265}{15}{subsubsection.2.4.3}%
\contentsline {subsubsection}{\numberline {2.4.4}Ecran tactile}{15}{subsubsection.2.4.4}%
\contentsline {section}{\numberline {3}Structure Agribot}{16}{section.3}%
\contentsline {subsection}{\numberline {3.1}Robot final}{16}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Etage 1}{17}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Etage 2}{18}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Etage 3}{19}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Schéma bloc Agribot}{20}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Schéma du câblage}{21}{subsection.3.6}%
\contentsline {section}{\numberline {4}Software}{22}{section.4}%
\contentsline {subsection}{\numberline {4.1}Environnement - ROS}{22}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Qu'est ce que ROS ?}{22}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}Pourquoi ROS ?}{22}{subsubsection.4.1.2}%
\contentsline {subsubsection}{\numberline {4.1.3}Définitions}{22}{subsubsection.4.1.3}%
\contentsline {subsection}{\numberline {4.2}Architecture software d'Agribot}{23}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Robot REDS}{23}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Architecture software globale}{23}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Architecture base mobile}{24}{subsubsection.4.2.3}%
\contentsline {subsubsection}{\numberline {4.2.4}Architecture de la navigation}{26}{subsubsection.4.2.4}%
