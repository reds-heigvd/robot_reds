# Calibration au tachimètre - Roues libres



**<u>La calibration est faite avec la roue avant gauche.</u>**

Rayon de la roue = 61 + 4.8 = 65.8 mm

Diamètre de la roue = 2 x pi x R = 41.343359321 cm = 0.41343359321m

2 mètres en 9.53 secondes

Tableau de valeurs : 

| PWM (0 - 4000) | Rotation/minute (mesure) - RM1 | Rotation/minute (théorique) - RM2 | Vitesse théorique (m/s)  | Vitesse mesurée (m/s) |      |
| :------------: | :----------------------------: | :-------------------------------: | :----------------------- | --------------------- | :--: |
|       0        |               0                |                 0                 |                          |                       |      |
|      100       |               0                |                 0                 |                          |                       |      |
|      200       |              10.3              |               10.3                | RM1=0.071<br />RM2=0.071 | 0.053705693           |      |
|      300       |              17.8              |               17.7                | RM1=0.123<br />RM2=0.122 | 0.105485232           |      |
|      400       |              25.0              |               25.1                | RM1=0.172<br />RM2=0.173 | 0.160384924           |      |
|      500       |              32.6              |               32.5                | RM1=0.225<br />RM2=0.224 | 0.209863589           |      |
|      600       |              40.2              |               39.9                | RM1=0.277<br />RM2=0.275 | 0.252525253           |      |
|      700       |              47.0              |               47.3                | RM1=0.324<br />RM2=0.326 |                       |      |
|      800       |              54.5              |               54.7                | RM1=0.376<br />RM2=0.377 |                       |      |
|      900       |              61.7              |               62.1                | RM1=0.425<br />RM2=0.428 |                       |      |
|      1000      |              69.0              |               69.5                | RM1=0.475<br />RM2=0.479 | 0.461893764           |      |
|      1200      |              84.1              |               84.3                | RM1=0.580<br />RM2=0.581 |                       |      |
|      1400      |              98.0              |               99.1                | RM1=0.675<br />RM2=0.683 | 0.689655172           |      |
|      1600      |             113.9              |               113.9               | RM1=0.785<br />RM2=0.785 |                       |      |
|      1800      |             127.6              |               128.7               | RM1=0.879<br />RM2=0.887 |                       |      |
|      2000      |             143.5              |               143.5               | RM1=0.989<br />RM2=0.989 |                       |      |
|      2200      |             158.5              |                                   |                          |                       |      |
|      2400      |             169.9              |                                   |                          |                       |      |
|      2600      |             186.5              |                                   |                          |                       |      |
|      2800      |             198.6              |                                   |                          |                       |      |
|      3000      |             213.6              |                                   |                          |                       |      |
|      3200      |             228.6              |                                   |                          |                       |      |
|      3400      |             240.0              |                                   |                          |                       |      |
|      3600      |             261.1              |                                   |                          |                       |      |
|      3800      |             283.1s             |                                   |                          |                       |      |
|      4000      |             296.8              |                                   |                          |                       |      |
|                |                                |                                   |                          |                       |      |
|                |                                |                                   |                          |                       |      |
|                |                                |                                   |                          |                       |      |



## 2. Formules pour le calcul de la vitesse

Calcul du périmètre de la roue : 

2 * pi * R = 2 * 3.14 * 6.75 cm = 42.411500823 cm

Une fois le périmètre calculer, prendre les tours par minutes et déterminer la vitesse en m/s

1 tour = 42.412 cm parcourus.

Pour 1000 PWM par exemple, le nombre de tours par minute = 69.0

La distance parcourue en 1 minute à 1000 PWM est de : 

69 * 42.412 = 2926.428 cm/minutes

Cela correspond donc à une vitesse linéaire de  : 

 2926.428 / 60 = 48.7738 cm / secondes donc à

48.7738 / 100 = 0.48 m/s



## 3. Calcul du ratio m/s - PWM

Range vitesse m/s entre 200-2000 pwm

0.989 - 0.071 = 0.918

On considère pour le calcul que la range est de de 0 à 1800 et qu'avant l'envoi du PWM, on ajoute 200 à la valeur obtenue.

Même principe pour la vitesse en m/s. La range de base est de 0.071 - 0.989. On prendra donc la range 0 - 0.918. Il faut donc enlever 0.071 à la valeur de vitesse m/s reçue.

Calculons à présent le nombre de steps pour la range de 200 à 2000 et des palliers de 100 PWM : 

(2000 - 200) / 100 = 18

Pour chaque 100 PWM augmenté, l'augmentation en m/s est de : 

0.918 / 18 = 0.051

Le ratio de multiplication es0.6*2021.568627451t donc de : 

100 / 0.051 = 1960.784313725



Pour confirmer ce calcul, testons de retrouver la valeur de PWM d'après une vitesse en m/s du tableau.

Prenons la valeur en m/s pour 700 PWM -> 0.326

Le calcul pour obtenir les PWM est le suivant : 

(0.326 - 0.071) * 1960.784313725 + 200 = 700

La valeur de PWM est bien retrouvée, le calcul de translation de valeur de vitesse vers valeur de PWM est bon.









