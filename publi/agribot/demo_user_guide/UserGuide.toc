\contentsline {section}{\numberline {1}Introduction}{3}{section.1}%
\contentsline {section}{\numberline {2}Lancement de la démo}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1}Vérification de l'alimentation du système}{4}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Alimentation des boards des niveaux supérieurs}{4}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}Alimentation des moteurs}{5}{subsubsection.2.1.2}%
\contentsline {subsubsection}{\numberline {2.1.3}Batterie 12V connectée}{6}{subsubsection.2.1.3}%
\contentsline {subsection}{\numberline {2.2}Mise en route de la démo}{7}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Communication laptop/Agribot}{7}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Démarrage de la démo}{7}{subsubsection.2.2.2}%
\contentsline {section}{\numberline {3}Troubleshotting}{8}{section.3}%
\contentsline {subsection}{\numberline {3.1}RPlidar absent}{8}{subsection.3.1}%
