# Marche à suivre - Déploiement logiciel



## 1. Installation de l'environnement

#### 1.1 Mise en place de l'OS et wifi

- Flash la carte avec Ubuntu 18.04 LTS : https://ubuntu.com/download/raspberry-pi

- Pour faciliter les manipulations et pour pouvoir effectuer des copier/coller, dans une des partitions de la carte se trouve à la racine le fichier config.txt, ajouter la ligne suivante (permet la connexion UART) : 

  ``` bash
  # serial
  enable_uart=1
  
  # i2c
  dtparam=i2c_arm=on
  
  #touchscreen
  framebuffer_width=1024
  framebuffer_height=600
  ```

- Connexion UART :

  <img src="./img/02_UART_connection.png" alt="02_UART_connection" align="left" style="zoom:60%;" />

- Sur l'ordinateur utilisé pour mettre en place la pi : 

  ``` bash
  sudo picocom -b 115200 /dev/ttyUSB0
  ```

- Mettre la carte dans le pi et se connecter (login = ubuntu, pass = ubuntu)

- Activer le wifi : 

  - ``` bash
    ls /sys/class/net  #(détecte le nom du wifi, wlan0 par exemple)
    ```

  - Modifier le fichier qui se trouve dans le dossier /etc/netplan/<name> en ajoutant le wifi : 

    <img src="./img/01_screen_wifi_set.png" alt="01_screen_wifi_set" align="left" style="zoom:100%;" />

  - ``` bash
    sudo netplan apply
    ```

  - Vérifier si le wifi fonctionne bien avec un ping ou autre.

  - Exemple de fichier pour copier/coller
  
    ```bash
        wifis:
            wlan0:
                optional: true
                access-points:
                    "jgu-05069":
                        password: "2ma9-fsoj-cd71-hhpl"
                dhcp4: true
    ```
  
    
  
  

#### 1.2 Changer le layout du clavier

- ``` bash
  sudo nano /etc/default/keyboard
  ```

- Modifier le XKBLAYOUT avec le layout de votre choix (ch pour Suisse, fr pour français etc...)

- Redémarrer le système

  

#### 1.3 Mise en place de ROS sur la pi4

- Installation du système :

  ```bash
  # set up source.list
  sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
  
  # set up les keys
  sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
  
  # debian package à jour
  sudo apt update
  
  # installer ros sans gui. 
  # j'ai installé Melodic mais n'importe lequel fera l'affaire
  sudo apt install ros-melodic-ros-base
  ```

- Mise en place de l'environnement :

  ```bash
  echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
  source ~/.bashrc
  
  # installations utiles et pratiques
  sudo apt install python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential
  
  # initialisation de rosdep
  sudo rosdep init
  rosdep update
  ```



#### 1.3 Tests de l'installation de ROS

Il suffit pour tester d'effectuer quelques commandes basiques telles que : 

``` bash
roscore
```

Si roscore ne fonctionne pas, ajouter dans le bashrc la ligne suivante : 

```bash
export ROS_HOSTNAME=localhost
```

Si cette commande fonctionne, l'ajouter au bashrc : 

```bash
echo "export ROS_HOSTNAME=localhost" >> ~/.bashrc
```

Source le bashrc et relancer roscore.

Ne pas hésiter à tester des packages de base de ROS.



#### 1.4 Mise en place de l'i2c

- Ajouter dans le fichier config.txt la ligne suivante : 

  ``` 
  dtparam=i2c_arm=on
  ```

- Il est important d'installer les outils nécessaires pour utiliser l'i2c : 

  ```bash
  sudo apt-get install libi2c-dev
  sudo apt-get install i2c-tools   
  ```

- ![03_i2c_connect](./img/03_i2c_connect.png)

  Voici une image montrant comment câbler l'i2c sur la rpi4.

  Et voici le cablage sur la board i2cPWM : 

  ![04_i2c_onboard_PWM](./img/04_i2c_onboard_PWM.png)

- Enfin la dernière étape est de modifier les droits de l'i2c : 

  ``` bash
  sudo chmod 777 /dev/i2c-1
  ```





## 2. Set up du contrôleur (ds4)

Pour que le ds4 fonctionne avec le rpi4 : 

- Installations à faire : 

  ```bash
  sudo apt-get install python3-pip 
  sudo pip3 install ds4drv # driver pour la manette ps4 sur Linux
  ```

- Récupérer le package ROS "joy". Pour ce faire deux solutions possibles :

  - Installation via apt 

    ```bash
    sudo apt-get install ros-melodic-joy ros-melodic-diagnostic-updater ros-melodic-roslint  # remplacer Melodic par la bonne version ROS
    ```

  - Récupération du package depuis le git : https://github.com/ros-drivers/joystick_drivers et mettre le dossier "joy" dans les sources du répertoire de travail Catkin créé et enfin refaire les manipulations pour le build du projet.

  La seconde méthode est vivement conseillé car la première requiert des adaptations dans les fichiers ".launch" concernant les paths.

- Connexion du bluetooth. Pour connecter la manette à la rpi4 il faut activer le service bluetooth. Dans le cas de la rpi4, il est indispensable d'attacher un UART série à la pile bluetooth en tant qu'interface de transport HCI. La commande pour le faire : 

  ```bash
  sudo hciattach /dev/ttyAMA0 bcm43xx 921600
  ```

- Si l'outil hciattach n'est pas disponible, il faut installer bluez : 

  ```bash
  sudo apt-get install bluez
  ```

- Pour éviter tout problème, une fois l'attach effectué, un redémarrage du service bluetooth est recommendé : 

  ```bash
  sudo systemctl restart bluetooth
  ```

- A présent il est possible de lancer le driver précédemment installé (ds4drv) : 

  ```bash
  sudo ds4drv # dans un autre terminal ou avec &
  ```

- Pour lier la manette, appuyer longtemps sur le bouton "share" et le bouton "PS" simultanément. Le driver devrait au bout de quelques secondes produire un message de ce type : 

  ![05_ds4_message_success](./img/05_ds4_message_success.png)

  Si ce message apparaît, bonne nouvelle tout s'est bien passé.

- Dans /dev/input/ un nouveau fichier de type "jsX" devrait être présent. Si aucun autre contrôleur n'est connecté, alors le fichier /dev/input/js0 doit être présent. Pour y accéder depuis ROS, il est nécessaire de changer les droits : 

  ```bash
  sudo chmod 777 /dev/input/js0
  ```

- Lancer le node joy et confirmer que le système fonctionne.

Pour une question pratique, il est possible de vouloir lancer ds4drv sans être super utilisateurs (sudo). Pour ce faire : 

- Créer un fichier dans /etc/udev/rules.d/ 

  ``` bash
  sudo nano /etc/udev/rules.d/50-ds4drv.rules
  ```

- Et copier le contenu suivant dedans : 

  ``` bash
  KERNEL=="uinput", MODE="0666"
  KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="05c4", MODE="0666"
  KERNEL=="hidraw*", SUBSYSTEM=="hidraw", KERNELS=="0005:054C:05C4.*", MODE="0666"
  KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="09cc", MODE="0666"
  KERNEL=="hidraw*", SUBSYSTEM=="hidraw", KERNELS=="0005:054C:09CC.*", MODE="0666"
  ```

- Puis enfin il suffit de reload les "rules" : 

  ``` bash
  sudo udevadm control --reload-rules
  sudo udevadm trigger
  ```



## 3. Mise en place du code

- Clôner le repo à la racine et exécuter le script de mise en place : 

  ```bash
  git clone https://gitlab.com/REDS-tdb/2020_catel
  ./2020_catel/setup_environnement
  ```

- Avant de make, installer les dépendances nécessaires pour le projet : 

  ```bash
  sudo apt-get install ros-melodic-roslint ros-melodic-tf2 ros-melodic-tf2-ros ros-melodic-xacro ros-melodic-tf ros-melodic-cv-bridge ros-melodic-image-transport ros-melodic-laser-geometry ros-melodic-tf-conversions libbullet-dev ros-melodic-map-msgs python-pip ros-melodic-tf2-sensor-msgs qt4-default ros-melodic-eigen-conversions ros-melodic-geographic-msgs ros-melodic-tf2-geometry-msgs ros-melodic-openslam-gmapping ros-melodic-csm libgeographic-dev libyaml-cpp-dev libyaml-dev ros-melodic-move-base-msgs libsdl-image1.2-dev ros-melodic-pcl-ros ros-melodic-diagnostic-updater ros-melodic-kdl-parser ros-melodic-tf2-kdl
  ```

- Installation de la librairie RPi.GPIO : 

  - Installer la librairie avec apt : 

    ```bash
    sudo apt-get install python-rpi.gpio
    ```

  - Cette version n'est pas fonctionnelle avec la b+ (pas à jour pour le moment ?). Il faut donc installer la même librairie avec pip : 

    ```bash
    sudo pip install RPi.GPIO # sans le sudo si ça ne marche pas
    ```

  - Enfin il suffit de remplacer les fichiers dans la librairie installée dans /usr/lib/python2.7/dist-packages/RPi par ceux qui se trouvent dans : ~.local/lib/python2.7/site-packages/RPi

  - Cela permet d'avoir la librairie installée avec la bonne version et pas uniquement en local.

- Build le projet : 

  ```bash
  cd robot_autonome
  catkin_make
  source devel/setup.bash
  ```

- Pour vérifier qu'aucun problème n'a été rencontré, la commande suivante lance le système pour la gestion avec le contrôleur : 

  ```bash
  roslaunch donkey_car ds4_control_demo.launch
  ```

- Pour que le contrôleur fonctionne, il est important d'enregistrer la manette avec ds4drv au préalable.



## 4. Démarrage autonome

Commandes utiles pour systemd (debug) : 

- sudo systemctl start <nom>.service

- sudo systemctl stop <nom>.service

- sudo systemctl status <nom>.service

- sudo journalctl -u <nom>.service

  

#### 4.1 - Démarrer roscore automatiquement

- Utiliser systemd

- Créer un nouveau service :

  ```bash
  sudo nano /etc/systemd/system/rbt_roscore_launch.service
  ```

- Puis lancer roscore en tant qu'utilisateur (ubuntu de base). Pour cela copier dans le fichier :

  ```bash
  [Unit]
  Description=Lance le roscore du systeme
  
  [Service]
  Type=simple
  ExecStart=/home/ubuntu/scripts/roscore_launch
  User=ubuntu
  Group=ubuntu
  
  [Install]
  WantedBy=multi-user.target
  ```

- Pour vérifier au boot que roscore est bien lancé, la commande suivante permet de lister les topics :

  ```bash
  rostopic list
  ```

  Si les topics "/rosout" et "/rosout_agg" sont lancés, tout s'est bien passé. 
  Pour vérifier les logs et ce qu'il s'est passé dans le service, utiliser les commandes cités en début du 5.

  

#### 4.2 - Setup le bluetooth pour le contrôleur au boot

- Ne fonctionne pas bien avec systemd. La solution la plus pratique est d'ajouter au fichier /etc/rc.local la commande souhaitée pour set correctement le bluetooth.

- Si le fichier /etc/rc.local n'existe pas, le créer : 

  ```bash
  sudo nano /etc/rc.local
  ```

  Coller la base du rc.local : 

  ```bash
  #!/bin/sh -e
  #
  # rc.local
  #
  # This script is executed at the end of each multiuser runlevel.
  # Make sure that the script will "exit 0" on success or any other
  # value on error.
  #
  # In order to enable or disable this script just change the execution
  # bits.
  #
  # By default this script does nothing.
  
  exit 0
  ```

- Lorsque le fichier est présent (de base ou créé) ajouter la ligne suivante avant le "exit 0" : 

  ```bash
  hciattach /dev/ttyAMA0 bcm43xx 921600
  ```

  Pour vérifier que la commande est bien exécutée au boot, il faut tester si la commande suivante permet d'attacher le contrôleur au système bluetooth : 

  ```bash
  ds4drv
  ```

  Si cette commande ne produit pas d'erreur, c'est que le set up est correctement effectué.

  

#### 4.3 - Gestion GUI

- Si ce n'est pas déjà fait ajouter les lignes suivantes au config.txt : 

  ```bash
  framebuffer_width=1024
  framebuffer_height=600
  ```

- Installer la llibrairie "Pygame" de cette façon : 

  ```bash
  cd ~
  sudo apt-get install mercurial
  hg clone https://bitbucket.org/pygame/pygame
  cd pygame
  sudo apt-get install python3-dev python3-numpy libsdl-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev libportmidi-dev libavformat-dev libswscale-dev libjpeg-dev libfreetype6-dev
  python3 setup.py build
  sudo python3 setup.py install
  ```
  
  Pour voir si tout est bien installé : 

  ```bash
echo "import pygame" > test_pygame.py
  sudo chmod 777 test_pygame.py
  python3 test_pygame.py
  ```
  
  Si aucune erreur ne survient, la librairie est à présent accessible.

- Pour tester la GUI manuellement : 

  - Depuis SSH : 

    ```bash
    sudo ./gui.py
    ```

  - Depuis un terminal directement sur la Raspberry pi : 

    ``` bash
    ./gui.py
    ```

    







## Exemple de développement de l'application de base du robot (optionnel)

Pour commencer, il est nécessaire de créer le répertoire de travail : 

```bash
mkdir catkin_ws
cd catkin_ws
mkdir src
catkin_make
```

Ces commandes vont créer les dossiers "build" et "devel".



Ce répertoire aura la structure suivante :  

​             racine

   |             |           |        

build        src      devel

#### 2.1 Contrôle du module i2c PWM pour l'envoie des commandes au moteur

- ``` bash
  cd ~/catkin_ws/src
  git clone https://gitlab.com/bradanlane/ros-i2cpwmboard
  ```

```
  
- Ajouter dans /ros-i2cpwmboard/src/i2cpwm_controller.cpp, dans les includes

  ``` c
  extern "C" {
      #include <linux/i2c.h>
      #include <linux/i2c-dev.h>
      #include <i2c/smbus.h>
  }
```

  Eventuellement modifier la fréquence (60 par exemple)

- Ajouter dans /ros-i2cpwmboard/CMakelists.txt

  ``` bash
  target_link_libraries(i2cpwm_board ${catkin_LIBRARIES} -li2c)
  ```

  Ajout du link avec la librairie libi2c installée pour l'i2c.

- A la racine du dépôt : 

  ``` bash
  catkin_make 
  source devel/setup.bash # Cette commande doit être faite dans chaque nouveau terminal
  ```

- Vérifier avec rosrun que tout fonctionne correctement

  ```bash
  # premier terminal
  roscore
  
  # deuxième terminal
  rosrun i2cpwm_board i2cpwm_board
  
  # troisième terminal
  rostopic list # permet de voir si les topics ont bien été créés
  rostopic pub /servos_absolute i2cpwm_board/ServoArray # finir avec tab la commande
  ```

  





