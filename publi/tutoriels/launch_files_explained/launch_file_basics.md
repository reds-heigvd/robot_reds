# Les bases des fichiers launch

Les fichier "launch" sont des fichiers avec l'extension ".launch" qui permettent le lancement d'un ou plusieurs noeuds dans ROS ou même d'autres fichiers launchs, tout en renseignant directement dans le fichiers divers paramètres.



## 1 - Objectif des fichiers launch

- Permet de faciliter le lancement du système avec une syntaxe XML simple.
- Ces fichiers sont flexibles et permettent de rendre la structure du code modulable et moins confuse.



## 2 - Syntaxe des fichiers launch 

La base des fichiers launch est une balise XML launch : 

```xml
<launch>
  <!-- remplir avec les différents composants à démarrer -->
</launch>

```

#### a) Démarrer des nodes (noeuds)

Pour lancer un noeuds la syntaxe est la suivante : 

```xml
<launch>
  <node pkg="<pkg_name>" name="<name>" type="<node_name>" output="screen" />
</launch>
```

<u>Explications des paramètres (package tf pour l'exemple)</u>  : 

- pkg : nom du package utilisé. Par exemple "tf" est un package qui contient de nombreux nodes.
- name : nom donné pour le voir dans l'environnement (peut être n'importe quoi, mais pour le debug donner des noms cohérent...)
- type : correspond au nom du noeud réel. Par exemple dans tf un des noeuds est "static_transform_publisher" et permet de publier les transformation d'une frame par rapport à une autre qui resteront statiques l'une par rapport à l'autre en toute circonstances.

Il est possible de passer d'autres informations par exemple : 

- output="screen", qui permet d'afficher sur la console les informations de debug présentent dans le code.
- args="<args>", qui permet de passer des arguments spécifiques pour un noeud.

Il existe d'autres variables modifiables spécifiques à chaque nodes/package pour le lancement de ceux-ci. C'est la base minimale qui est présentée dans ce document.



#### b) Inclusion d'autres fichiers launch

Une des possibilité les plus pratiques pour avoir un lancement modulable est la possibilité d'appeler d'autres fichier launch depuis un fichier launch.

La syntaxe est la suivante : 

```xml
<launch>  
  <include file="$(find package)/path/to/launch_file.launch"/>
</launch>
```

<u>Explications</u> : 

- "find" permet de retrouver un package d'après le nom de celui-ci.
- Il suffit de renseigner ensuite le chemin du fichier ".launch" à partir de la racine du package.

Il est possible d'inclure de nombreux autres fichiers launchs en les listant avec plusieurs balises : 

```xml
<launch>  
  <include file="$(find package)/path/to/launch_file.launch"/>
  <include file="$(find package_2)/path/to/launch_file2.launch"/>
</launch>
```



#### c) Définir des paramètres

Il est possible de définir des paramètres directement dans les fichiers launch (ce qui est possible aussi directement depuis le code ou dans des fichiers ".yaml").

La syntaxe est la suivante : 

```xml
<launch>  
  <arg name="arg_name0" default="default_value0"/>
  <arg name="arg_name1" default="default_value1"/>
  <arg name="arg_name2" default="default_value2"/>
</launch>
```

Cela crée un argument (ou variable) qui est utilisable dans le code source ou même directement dans le fichier launch. Cela permet de mettre en place une valeur une seule fois et de l'utiliser plusieurs fois même dans le fichier launch directement de cette façon : 

```xml
<launch>  
  <node pkg="<pkg_name>" name="<name>" type="$(arg default_value0)" output="screen" />
</launch>
```

La référence à l'argument est faite dans la définition de "type" lors du lancement du node dans ce cas-ci.

Pour plus de précision sur les paramètres  et les syntaxes, se référer à : http://wiki.ros.org/roslaunch/XML

Concernant les paramètres, il est d'usage d'utiliser les fichiers ".yaml" lorsqu'il y a beaucoup de paramètres qui sont définis et qui peuvent varier. Ces fichiers sont expliqués dans le "3 -" du document.



 #### d) Le reste

Les fichiers launchs ne se limitent pas à ces quelques concepts, mais ce sont les principaux pour comprendre le principe et s'en sortir dans beaucoup de situations.

Il y a d'autres outils très intéressants suivant les cas : 

- if/unless : permet d'avoir un if/else dans un fichier xml. Pratique lorsque certaines parties ne doivent être lancées que si un certain matériel est présent/disponible par exemple. Des conditions directement spécifiées par le passage de paramètres peut aussi être fait.
- group : correspond à une sorte de conteneur. Permet d'appliquer des paramètres à un groupe de nodes spécifique défini dans le groupe.
- ...

Pour plus d'informations sur toutes les possibilitées, se référer au wiki : http://wiki.ros.org/roslaunch/XML



## 3 - Les fichiers de paramètres (.yaml)



 #### a) Syntaxe

Lorsqu'il y a beaucoup de paramètres à changer pour lancer un noeud spécifique, il est possible de le faire directement depuis le XML dans le fichier launch, mais avec une syntaxe verbeuse. 

Les fichiers .yaml peuvent être appelés depuis les fichiers launch pour mettre en place des paramètres avec une syntaxe beaucoup plus épurée : 

```yaml
frequency: 100

silent_tf_failure: false
sensor_timeout: 0.1

two_d_mode: true

transform_time_offset: 0.0
transform_timeout: 0.0

# If you're having trouble, try setting this to true, and then echo the /diagnostics_agg 
# topic to see if the node is unhappy with any settings or data.
print_diagnostics: true

deceleration_gains: [1.0, 0.0, 0.0, 0.0, 0.0, 1.0]
```

Le fichier launch va récupérer le fichier .yaml et le parser pour obtenir le nom des paramètres et leur valeur.

Les types de base sont pris en compte par les fichiers yaml pour définir les paramètres.

Il est aussi possible de renseigner des tableaux avec la syntaxe [<val>, <val>, <val>]

Plus d'informations sur : https://wiki.ros.org/rosparam



#### b) Intégration/appel des fichiers yaml dans les fichiers launch

Pour intégrer les fichiers yaml : 

```xml
<launch>
  <node pkg="<pkg>" type="<node_name>" name="<name>" clear_params="true">
    <rosparam command="load" file="$(find <pkg>)path/to/params_file.yaml" />
  </node>
</launch>
```

Voici un exemple de chargement d'un fichier de paramètres pour le lancement d'un node.

Ce qui est important à retenir c'est que la balise "<rosparam>" permet d'inclure les paramètres.

Pour plus d'informations : https://wiki.ros.org/rosparam

