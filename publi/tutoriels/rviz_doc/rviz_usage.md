# Explication et initiation à Rviz



## 1 - Lancement rviz

Pour commencer il faut que le package rviz soit installé. Si ce n'est pas le cas, l'installer via :

```bash
sudo apt-get install ros-melodic-rviz
```

Pour lancer rviz et assurer le bon fonctionnement du package : 

```bash
rosrun rviz rviz
```

Cette fenêtre apparaît : 

![01_fenetre_rviz_base](./img/01_fenetre_rviz_base.png)

C'est la base du package. A partir de cette fenêtre, toute la configuration doit être faite pour afficher les données utiles/souhaitées.



## 2 - Configuration manuelle

#### a) Configurer la base : 

Une fois le package rviz lancé avec la méthode du "1 -", il faut tout d'abord configurer l'option "fixed frame".

<img src="./img/02_fixed_frame.png" alt="02_fixed_frame" align="left" style="zoom:100%;" />

De base c'est "map" parce que c'est en général la frame de base statique d'un robot. Cependant pour divers tests, cette frame n'existera pas dans un premier temps. Il faut donc déterminer quelle est la frame la plus haute (frame parent) du tf tree (arborescence des frames). Pour plus d'explications sur les frames et le debug, aller voir le tutoriel sur les tf.

Il suffit alors de modifier "map" par la bonne frame et quelque chose devrait apparaître (dépendant du projet lancé évidemment...).

#### b) personnaliser l'affichage : 

A partir de cette étape, il suffit d'ajouter les différentes otpions souhaitées. 

Pour ce faire, clicker sur le bouton "add" en bas de la fenêtre "displays" : 

<img src="./img/03_add_button.png" alt="03_add_button" align="left" style="zoom:100%;" />  
</br>

Une fenêtre pop-up apparaît avec de nombreuses options. La plus basique est l'affichage des frames :   
</br>

<img src="./img/04_tf_display.png" alt="04_tf_display" align="left" style="zoom:100%;" />  
</br>  

Dans la fenêtre displays s'ajoute alors les TFs : 
</br>

<img src="./img/05_tf_displays.png" alt="05_tf_displays" align="left" style="zoom:100%;" />
</br>

Après avoir déroulé le menu, il est possible de gérer l'affichage souhaité pour les frames, ainsi que les frames que l'on souhaite afficher.

Il faut procéder de la même manière pour afficher les différentes informations fournies par le système mis en place.



## 3 - Rviz avec une configuration prédéfinie

#### a) Sauvegarder sa configuration

Dans la pratique Rviz n'est utilisé de la manière décrite dans le "2 -" que lorsque l'on souhaite créer une configuration spécifique que l'on souhaite réutiliser.

En effet une fois arrivé à la configuration souhaitée, il est possible de la sauvegarder dans un fichier "*.rviz" et de charger cette configuration au lancement de rviz. 

Pour ce faire, après avoir choisi les bon paramètres de displays : 

![06_save_config](./img/06_save_config.png)

Sauvegarder le fichier avec l'extension ".rviz" à l'endroit souhaité.

Le fichier créé est un fichier de paramètres lisible qui ressemble à : 

![07_file_rviz_params](./img/07_file_rviz_params.png)

Il est tout à fait possible de modifier ce fichier manuellement pour modifier les paramètres d'affichage lorsque l'on connaît un peu mieux le fonctionnement d'Rviz et la description des fichiers ".rviz".



#### b) Réutiliser le fichier rviz créé

Pour réutiliser ce fichier, le plus pratique/propre/habituel est d'utiliser un fichier de lancement launch (extension ".launch").

Un tutoriel sur les fichiers launch est disponible, s'y référer si besoin.

Le fichier launch à écrire pour lancer rviz avec la configuration décrite dans le fichier ".rviz" précédemment créé est très simple : 

```xml
<?xml version="1.0"?>
<launch>
  <node name="rviz" pkg="rviz" type="rviz" args="-d $(find your_package)/your/path.rviz" required="true" />
</launch>
```

La seule chose à modifier est le paramètre "args" qui contient le chemin vers le fichier de configuration .rviz.

















