# URF - Bases

Ce document permet de comprendre les bases de l'URDF pour l'utilisation qui en est faite dans les différents projets de "robot_reds".

## 1- Syntaxe

Voici un exemple de fichier URDF très simple mais qui couvre les notions basiques (la description est aussi disponible dans un fichier URDF présent dans le dossier) : 

```xml
<?xml version="1.0"?>
<robot name="origins">
  <link name="base_footprint"/>

  <joint name="footprint_to_base_link" type="fixed">
    <parent link="base_footprint"/>
    <child link="base_link"/>
    <origin xyz="0 0 0"/>
  </joint>
  
  <link name="base_link"/>
  <link name="laser"/>

  <joint name="base_to_scan" type="fixed">
    <parent link="base_link"/>
    <child link="laser"/>
    <origin rpy="0 0 -1.5708" xyz="0 0 0.05"/>
  </joint>  
  
  <link name="imu_link">
    <visual>
      <geometry>
        <box size="0.05 0.055 0.01"/>
      </geometry>
      <material name="green"/>
      <origin rpy="0 0 0" xyz="0 0 0"/>
    </visual>
  </link>  
  
  <joint name="base_to_imu" type="continuous">
    <parent link="base_link"/>
    <child link="imu_link"/>
    <origin xyz="0 0 0.14"/>
  </joint>  
</robot>
```

<u>**Explications**</u> : 

- La description entière du robot se trouve dans une balise <robot>.

- Un link est défini par la balise <link>. Le paramètre indispensable pour définir un "link" est le nom (name="<link_name>"). Par exemple : 

  ```xml
    <link name="base_footprint"/>
  ```

- Il est possible de spécifier de nombreux paramètres pour décrire une balise "link". Par exemple dans le fichier d'exemple, le link "imu_link" possède une description de son visuel, qui n'est utile que pour la visualisation et non dans le traitement des informations du système.

- Les "links" correspondent à des frames dans ROS.

- Les balises <joint> permettent de spécifier les "tf" (transformation frame) à publier.

- Il n'est possible d'avoir en parent/enfant de <joint> uniquement des <link> préalablement déclarés dans le fichier URDF.

- Cette organisation de <link> et de <joint> vont permettre de générer un tf tree d'après la description du robot.

- ATTENTION : la balise <origin> peut-être présente dans <link> comme dans <joint>. Cette balise permet de spécifier un décalage par rapport à l'origine. Dans <link>, le décalage sera fait d'après le point d'ancrage du link (donc aucun impact sur les TF, juste sur le visuel) tandis que dans <joint>, c'est ce décalage qui sera publié pour le TF. 

- La balise <origin> peut opérer des transformations sur xyz (position) et sur rpy (rotation).



## 2 - Utilisation du fichier URDF dans ROS

Une fois la description faite, ROS propose des packages pour l'utiliser et publier les TF définis : 

1. Robot_state_publisher : peut fonctionner seul et va publier tous les <joints> "fixed"
2. Joint_state_publisher : fonctionne en combinaison avec robot_state_publisher. Permet de publier les TF "continuous" en publiant à un frame rate spécifique les transformations sur un topic des messages "JointState" que robot_state_publisher va traiter.



- Les balises <joint> ont un type. Dans le fichier d'exemple, le type est "fixed" ou "continuous". Il existe d'autres types qui sont utiles suivant les cas de figure. Concrètement le type "fixed" permet de publier des tf statiques et "continuous" des tf qui ne sont pas statiques.



<u>**Les static tf**</u> : si un élément est considéré comme fixé par rapport à un autre, alors la publication d'un static_tf est suffisante. Par exemple le Rplidar aura une transformation qui ne changera pas par rapport à la base du robot. Il n'y a donc pas besoin de spécifier en temps réel la transformation du laser par rapport à la base puiqu'elle sera toujours la même.

<u>**Les tf**</u> : il faut tracer en temps réel (idéalement au frame rate le plus élevé possible) la transformation d'un élément par rapport à son parent. Par exemple l'extrémitée d'un bras mécanisé qui peut bouger n'aura pas la même transformation par rapport à la base à tout moment.



## 3 - Aller plus loin

Ce sont les bases qui sont décrites ici. Cela permet de comprendre un peu mieux le début des URDF.

Pour plus de précisions et un explication beaucoup plus fournie sur toutes les possibilités : http://wiki.ros.org/urdf/Tutorials







