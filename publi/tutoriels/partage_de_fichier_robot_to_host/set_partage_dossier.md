# Tutoriel pour le partage de dossier

Le but est d'avoir accès à un dossier spécifique et de pouvoir modifier les fichiers directement depuis le laptop. Celui-ci permet de modifier plus rapidement les fichiers avec l'interface graphique disponible.



#### 1. Installations 

Sur la rpi4 présente sur le robot : 

```bash
sudo apt-get install openssh-server
```

Sur la machine hôte (Acer actuellement) : 

```bash
sudo apt-get install sshfs
```



#### 2. Partage du dossier : 

Sur la machine hôte : 

```bash
sshfs username_on_server@server_ip:/location_of/movies_on_Server /mountpoint/on_host_device

## Exemple de commande sur la machine : 
sshfs ubuntu@10.192.91.234:/home/ubuntu/robot_reds /home/reds/shared_files
```

Si besoin, ajouter le laptop au "fuser group" : 

```bash
sudo useradd -G {group-name} username
```

Il est à présent possible de naviguer et modifier les fichiers du robot (rpi4) directement depuis le laptop.



#### 3. Terminer le partage : 

Sur la machine hôte

```bash
fusermount -u /mountpoint/on_host_device
```

Si comme pour moi la partie d'ajout au "fuser groupe" n'est pas claire, exécuter cette commande en sudo...