# Analyser un système logiciel ROS

Une fois le système lancé via des fichiers launchs, des nodes communiquent entre eux via des messages postés et récupérés sur des topics.

Les messages d'erreurs qui apparaissent sur la console peuvent indiquer des erreurs, mais il n'est pas toujours évident de comprendre d'où proviennent ces erreurs.

Il existe des outils pour analyser un système et pour s'assurer du bon fonctionnement (ou pas) de celui-ci.



## 1 - Analyse des topics

ROS met à disposition de nombreuses commandes très pratiques pour le debug et l'analyse des topics. Nous allons parcourir les plus importantes et les plus utiles.

La plus basique des commandes pour observer tous les topics déclarés : 

```bash
rostopic list
```

La commande va produire cette sortie : 

![04_rostopic_list_out](./img/04_rostopic_list_out.png)

Lorsque le système n'a que le master ROS, sans aucun node lancé, les topics /rosout et /rosout_agg sont présents. C'est un bon moyen pour voir si le rosmaster est bien lancé et que le système est prêt à l'emploi.

Pour voir les messages qui transitent sur un topic : 

```bash
rostopic echo /<topic_name>
```

Une fois les nodes lancés, il est possible d'afficher les messages qui transitent sur ce topic, la sortie ressemble à cela : 

![05_rostopic_echo](./img/05_rostopic_echo.png)

Les messages vont défiler en temps réel, on ne voit ici que le premier message. Si on va voir la doc des messages tf, on retrouvera les 3 parties : header, child_frame et transform.

Pour voir la fréquence de publication d'un message sur un topic utiliser la commande suivante : 

```bash
rostopic hz /<topic_name>
```

Cette commande produit en sortie : 

![06_rostopic_hz](./img/06_rostopic_hz.png)

Les informations sont claires, dans ce cas-ci les tf de base_link -> imu_link sont publiées à une fréquence de 20Hz. Cela correspond aux paramètres définis. Cependant lorsqu'un système est surchargé ou non fonctionnel, cette fréquence peut être différente.



D'autres options existent concernant la commande rostopic, libre à vous d'aller explorer plus en détail la documentation : http://wiki.ros.org/rostopic





## 2 - Analyse des frames

Si les frames ne sont pas encore un concept familier pour vous, se référer au tutoriel sur des tf (tranformation frames).

Concrètement un robot est décrit par les frames et les transformations effectuées entre elles. Il est donc impératif de s'assurer que les frames sont correctement mises en places dans le système.

Pour cela il existe plusieurs façons de les analyser : 



#### a) Analyse des messages de tf publiés sur les topics

Les tf sont des messages de type "geometry_msgs/TransformStamped" tels que : 

```bash
# This expresses a transform from coordinate frame header.frame_id
# to the coordinate frame child_frame_id
#
# This message is mostly used by the 
# tf package. 
# See its documentation for more information.

Header header
string child_frame_id # the frame id of the child frame
Transform transform
```

Pour plus d'informations sur les éléments : http://docs.ros.org/en/melodic/api/geometry_msgs/html/msg/TransformStamped.html

Ces messages sont publiés sur des topics spécifiques, en général "tf" ou "tf_static".

Pour une analyse simple, il est possible d'utiliser la commande : 

```bash
rostopic echo /tf   
# ou
rostopic echo /tf_static
# selon les besoins
```

C'est une alanyse assez basique qui donne principalement des informations concernant les transformations elles-mêmes.



#### b) Le tf tree

Le tf tree est très important, que ce soit pour une analyse personnelle ou pour demander de l'aide à la communauté.

Un outil existe pour produire un pdf permettant de voir le tf tree s'un système en cours : 

```bash
rosrun tf view_frames
```

Cette commande va analyser pendant 5 secondes les transformations qui sont faites sur les topics /tf (et dérivés) et va générer un pdf de type : 

![01_analyse_tftree_pdf](./img/01_analyse_tftree_pdf.png)

Ce tf tree est très basique et provient du fichier URDF en annexe dans le dossier. 

Ce tf tree donne des informations très importantes : 

- Broadcaster : node qui publie la transformation
- Average rate : fréquence de publication sur le topic. 10000.000 Hz indique que c'est une static_transform (donc le système n'est pas réellement surchargé par cette transformation). 
- La vue générale du tf tree est le plus important. Si les liens correspondent aux attentes, c'est un bon début.



#### c) Combiner les deux analyses

Dans la plupart des cas, il est important d'analyser les frames avec les deux points pour avoir une vue complète sur l'état dudes système et s'assurer du bon fonctionnement des transformations.

Le concept des frames et des transformation est un concept simple à comprendre, mais complexe à maitriser... Tous les outils d'analyses sont donc les bienvenus.





## 3 - Analyse des nodes

L'analyse des nodes est un aspect important pour les mêmes raisons que l'analyse des frames.

Un outil très performant existe pour donner une vue d'ensemble sur l'intéraction des nodes et des topics. Cet outil est un package ROS du nom de "rqt_graph".

C'est un outil qui offre une GUI pour observer les nodes en temps réel. Si le système est sur une rpi4 sans sortie vidéo, alors il est nécessaire d'avoir un laptop tiers qui communique avec la rpi4 pour visualiser les nodes.

Pour commencer installer le package : 

```bash
sudo apt-get install ros-melodic-rqt-graph
```

Pour utiliser le package et lancer la GUI : 

```bash
rosrun rqt_graph rqt_graph
```

Une fenêtre de ce type va apparaître : 

![01_rqt_graph_simple](./img/01_rqt_graph_simple.png)

C'est une structure vraiment basique qui montre les nodes dans des ovales : 

- joint_state_publisher
- robot_state_publisher
- rqt_gui_py_node
- rosout

Et les topics dans des rectangles : 

- joint_states
- rosout

L'interface est très intuitive donc nous n'entrerons pas plus en détails sur les différentes options.

Voici cependant un example un peu plus complexe, qui correspond à une des étapes de développement du robot agribot : 

![03_agribot_node_viz](./img/03_agribot_node_viz.png)

En déplaçans le curseur sur les différentes parties du graphique, des informations supplémentaires peuvent apparaître.

L'exploration de l'outil est une affaire personnelle !





## 4 - Informations de debug

Enfin il existe un dossier avec des informations de debug qui peuvent parfois être utiles : 

```bash
~/.ros/log/*
```

Il est possible de retrouver certaines informations en dernier recours, mais ce n'est pas forcément le debug le plus pratique.





