# ----------------------------------------------------------------------------------------
# -- HEIG-VD, Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
# -- REDS Institute, Reconfigurable Embedded Digital Systems
# ----------------------------------------------------------------------------------------
# --
# -- File     : README.txt
# -- Author   : A.Gabriel Catel Torres
# -- Date     : 07.10.2020
# --
# -- Context  : Depot projet robots du REDS
# --
# ----------------------------------------------------------------------------------------
# -- Description :
# --   Strucuture du depot du projet générique pour les robots du REDS
# -- 
# ----------------------------------------------------------------------------------------
# -- Modifications :
# -- 
# ----------------------------------------------------------------------------------------



INITIALISER L'ENVIRONNEMENT : 
1. Aller dans le dossier racine du projet (source) ../robot_reds/dev/.
2. Make le projet avec la commande suivante : catkin_make -DCMAKE_BUILD_TYPE=Release



Structure du depot:
===================

- admin             information administrative
  |


-doc                documentation de tiers
  |		


-dev      developpement realisé                 
  |- src	      code source
      |- global_launcher	Fichiers launchs général spécifiques à chaque robot
      |- reds_pkg         	Packages créés par le REDS
          |- hardware         		
          |- misc         		
          |- navigation         		
          |- sensors   
      |- robot			Code et informations spécifiques à chaque robot du REDS
          |- agribot         		Robot développé dans le cadre de l'agriculture écologique

          |- helpie         		---
    |- ros_pkg         		Packages récupérés (développés par la communauté ROS)
        |- hardware         		
        |- misc         		
        |- navigation         		
        |- sensors         		


-publi              documentation redigée 
  |


