# Liste du matériel à commander : 



| Matériel                                   | Quantité                               | Lien                                                         |
| ------------------------------------------ | -------------------------------------- | ------------------------------------------------------------ |
| Chassis, moteurs et roues                  | 1                                      | https://www.servocity.com/prowler#313=36                     |
| Batterie et chargeur                       | 3                                      | https://www.servocity.com/12v-6000mah-li-ion-power-bank-w-charger |
| Board I2C PWM                              | 1                                      | https://www.cesdeals.com/fr/product/pca9685-16-channel-12-bit-pwm-servo-driver-i2c-interface-module-for-arduino-234862?currency=CHF&gclid=CjwKCAjwkun1BRAIEiwA2mJRWR_uzFTonHcgEg0gxqY-r3NMWzkzg4laZD4ytpeznLeIH0c7sWaFmRoCGdMQAvD_BwE |
| Raspberry Pi 4                             | 1 (ou bien demander au reds une carte) | https://www.digitec.ch/fr/s1/product/raspberry-pi-4-4g-model-b-armv8-cartes-de-developpement-kits-11267867?gclid=CjwKCAjwkun1BRAIEiwA2mJRWY2TRlXcO3HFIvn5Huv6_VWI5ne_t1EU32gKHn7ah0NS-aNHUBmNPBoCpGgQAvD_BwE&gclsrc=aw.ds |
| Cartes SD 32 Go                            | 3                                      | https://www.digitec.ch/en/s1/product/sandisk-ultra-microsdhc-32gb-80mbs-cl-10-microsdhc-32gb-memory-card-10409472?gclid=CjwKCAjwkun1BRAIEiwA2mJRWXDeDP02r7zJq6prKuDtOdDyvZPwyubd4aNUp50wzthVmXEoUQ8TXRoCqI8QAvD_BwE&gclsrc=aw.ds |
| Screen                                     | 1                                      | https://www.wish.com/product/5d4d13ff7e7b3608a69d5bf7?hide_login_modal=true&from_ad=goog_shopping&_display_country_code=CH&_force_currency_code=CHF&pid=googleadwords_int&c=%7BcampaignId%7D&ad_cid=5d4d13ff7e7b3608a69d5bf7&ad_cc=CH&ad_lang=EN&ad_curr=CHF&ad_price=50.00&campaign_id=9527731137&gclid=CjwKCAjwkun1BRAIEiwA2mJRWUa9N2fvngeAXO4lvhrYlIWlPJEfwRMRSKsThEc_swDnsreuGR5ESBoC8v4QAvD_BwE&share=web |
| Motor controller                           | 4                                      | https://www.parallax.com/product/29144                       |
| Voltage converter                          | 1                                      | https://www.banggood.com/fr/DC-DC-9V12V24V36V-To-5V-5A-Step-Down-Board-Buck-Module-High-Power-Vehicle-Power-Supply-Converter-p-1211647.html?rmmds=search&cur_warehouse |
| .......................................... |                                        |                                                              |



** Si le lieu de commande n'est pas compatible avec la politique de l'HEIG me prévenir au plus vite

