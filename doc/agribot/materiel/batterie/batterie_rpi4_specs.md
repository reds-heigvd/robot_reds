## Description

FreePower Slim 10.000 est le chargeur de batterie portable  ultra-mince qui offre une charge rapide, des performances élevées et une légèreté avec une épaisseur de seulement 20 mm : le meilleur compromis  entre performance et taille. Construit avec une technologie  lithium-polymère innovante, le FreePower Slim 10 000 est une banque de  puissance ultra-mince avec un encombrement minimal, parfaite à  transporter avec vous à tout moment. Le chargeur de batterie portable  dispose également d'un système de charge rapide simultanée et d'une  protection contre la surchauffe, vous permettant de charger vos  appareils en toute sécurité.



## Spécifications

| Spécifications principales |                      |
| -------------------------- | -------------------- |
| Technologie de charge      | Charge rapide        |
| Ports USB                  | 1 x USB, 1 x USB-C-C |
| Poids                      | 173 g                |

| Informations générales                                       |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Fabricant                                                    | [Cellularline](https://www.digitec.ch/fr/brand/cellularline-15751) |
| Type de produit                                              | [Chargeurs de secours](https://www.digitec.ch/fr/s1/producttype/chargeurs-de-secours-2040)i |
| Numéro d'article                                             | 8970589                                                      |
| Réf. du fabricant                                            | FREEPSLIM10000K                                              |
| Classement des ventes dans Type de produit Chargeurs de secours | 33 de 1101                                                   |

| Couleur           |      |
| ----------------- | ---- |
| Groupe de couleur | Noir |
| Couleur exacte    | Noir |

| Connexions |                      |
| ---------- | -------------------- |
| Ports USB  | 1 x USB, 1 x USB-C-C |

| Alimentationi                    |               |
| -------------------------------- | ------------- |
| Production de courant électrique | 2.10 A        |
| Technologie de charge            | Charge rapide |
| Puissance nominale               | 10 W          |

| Propriétés des piles / de la batterie |           |
| ------------------------------------- | --------- |
| Type de pile / batterie               | LiPoi     |
| Capacité                              | 10000 mAh |

| Contenu de la livraison |                 |
| ----------------------- | --------------- |
| Contenu de la livraison | Câble Micro USB |

| Dimensions du produit |        |
| --------------------- | ------ |
| Longueur              | 110 mm |
| Largeur               | 42 mm  |
| Hauteur               | 20 mm  |
| Poids                 | 173 g  |

| Dimensions de l’emballage |       |
| ------------------------- | ----- |
| Longueur                  | 18 cm |
| Largeur                   | 10 cm |
| Hauteur                   | 3 cm  |
| Poids                     | 230 g |